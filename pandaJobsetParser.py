# Library to scrape jobset information from the Pandamon website, and return
# in an easy-to-use container class

import HTMLParser

class PandaJobsetParser(HTMLParser.HTMLParser):
  def __init__(self, jobContainer):
    HTMLParser.HTMLParser.__init__(self)

    # Save reference to the job container object
    self._jobContainer = jobContainer

    # Initialise state variables
    self.inJobTable = False

    self._resetRowVars()

  def _resetRowVars(self):
    # There are up to three rows in each element, but we only care about two...
    self.inRowOne = False
    self.inRowTwo = False
    self.doneRowOne = False
    self.doneRowTwo = False

    self.inIDOwner = False
    self.inPandaID = False
    self.inOwner = False
    self.inJob = False
    self.inJobsetID = False
    self.inJobInfo = False
    self.inStatus = False
    self.inCreated = False
    self.inTime = False
    self.inDuration = False
    self.inEnded = False
    self.inCloudSiteType = False
    self.inCloud = False
    self.inSite = False
    self.inPriority = False
    self.inInOrOutTest = False
    self.inInput = False
    self.inOutput = False

    self.doneIDOwner = False
    self.donePandaID = False
    self.doneOwner = False
    self.doneJob = False
    self.doneJobsetID = False
    self.doneJobInfo = False
    self.doneStatus = False
    self.doneCreated = False
    self.doneTime = False
    self.doneDuration = False
    self.doneEnded = False
    self.doneCloudSiteType = False
    self.doneCloud = False
    self.doneSite = False
    self.donePriority = False
    self.doneInOrOutTest = False
    self.doneInput = False
    self.doneOutput = False

    self.pandaID = 0
    self.owner = ''
    self.jobsetID = 0
    self.jobInfo = ''
    self.status = ''
    self.created = ''
    self.time = ''
    self.duration = ''
    self.ended = ''
    self.cloud = ''
    self.site = ''
    self.type = ''
    self.priority = 0
    self.input = ''
    self.output = ''

  def handle_starttag(self, tag, attrs):
    if not self.inJobTable and tag == 'table' and ('border', '1') in attrs and ('cellspacing', '0') in attrs and ('cellpadding', '2') in attrs:
      self.inJobTable = True
    elif self.inJobTable:
      if not self.doneRowOne and tag == 'tr' and len(attrs) == 0:
        self.inRowOne = True
      elif self.inRowOne:
        if not self.doneIDOwner:
          if tag == 'td' and ('align', 'middle') in attrs:
            self.inIDOwner = True
          # Breakdown of elements within IDOwner container here
          if self.inIDOwner:
            if not self.donePandaID and tag == 'a':
              self.inPandaID = True
            elif not self.doneOwner and tag == 'a':
              self.inOwner = True
        elif not self.doneJob:
          if tag == 'td':
          #if tag == 'td' and ('bgcolor', '#ffffbb') in attrs:
            self.inJob = True
          if self.inJob:
            if not self.doneJobsetID and tag == 'a':
              self.inJobsetID = True
            elif not self.doneJobInfo and tag == 'a':
              self.inJobInfo = True
        elif not self.doneStatus and tag == 'td':
          self.inStatus = True
        elif not self.doneCreated and tag == 'td':
          self.inCreated = True
        elif not self.doneTime and tag == 'td':
          self.inTime = True
        elif not self.doneDuration and tag == 'td':
          self.inDuration = True
        elif not self.doneEnded and tag == 'td':
          self.inEnded = True
        elif not self.doneCloudSiteType:
          if tag == 'td':
            self.inCloudSiteType = True
          if self.inCloudSiteType:
            if not self.doneCloud and tag == 'a':
              self.inCloud = True
            elif not self.doneSite and tag == 'a':
              self.inSite = True
        elif not self.donePriority and tag == 'td':
          self.inPriority = True
      elif self.doneRowOne and not self.inRowTwo and tag == 'td' and ('colspan', '9') in attrs:
      #elif self.doneRowOne and not self.inRowTwo and tag == 'td' and ('colspan', '9') in attrs and ('bgcolor', '#ffffbb') in attrs:
        self.inRowTwo = True
      elif self.inRowTwo:
        if not self.doneInOrOutTest and tag == 'b':
          self.inInOrOutTest = True
        elif not self.doneInput and tag == 'a':
          self.inInput = True
        elif not self.doneOutput and tag == 'a':
          self.inOutput = True

  def handle_endtag(self, tag):
    if self.inJobTable:
      if tag == 'table':
        self.inJobTable = False
      if self.inRowOne:
        if tag == 'tr':
          self.inRowOne = False
          self.doneRowOne = True
        if self.inIDOwner:
          if tag == 'td':
            self.inIDOwner = False
            self.doneIDOwner = True
          if self.inPandaID and tag == 'a':
            self.inPandaID = False
            self.donePandaID = True
          elif self.inOwner and tag == 'a':
            self.inOwner = False
            self.doneOwner = True
        elif self.inJob:
          if tag == 'td':
            self.inJob = False
            self.doneJob = True
          if self.inJobsetID and tag == 'a':
            self.inJobsetID = False
            self.doneJobsetID = True
          elif self.inJobInfo and tag == 'a':
            self.inJobInfo = False
            self.doneJobInfo = True
        elif self.inStatus and tag == 'td':
          self.inStatus = False
          self.doneStatus = True
        elif self.inCreated and tag == 'td':
          self.inCreated = False
          self.doneCreated = True
        elif self.inTime and tag == 'td':
          self.inTime = False
          self.doneTime = True
        elif self.inDuration and tag == 'td':
          self.inDuration = False
          self.doneDuration = True
        elif self.inEnded and tag == 'td':
          self.inEnded = False
          self.doneEnded = True
        elif self.inCloudSiteType:
          if tag == 'td':
            self.inCloudSiteType = False
            self.doneCloudSiteType = True
          if self.inCloud and tag == 'a':
            self.inCloud = False
            self.doneCloud = True
          elif self.inSite and tag == 'a':
            self.inSite = False
            self.doneSite = True
        elif self.inPriority and tag == 'td':
          self.inPriority = False
          self.donePriority = True
      if self.inRowTwo:
        if self.inInOrOutTest and tag == 'b':
          self.inInOrOutTest = False
          self.doneInOrOutTest = True
        elif self.inInput and tag == 'a':
          self.inInput = False
          self.doneInput = True
        elif self.inOutput and tag == 'a':
          if self.output != 'Monitor':
            self.inOutput = False
            self.doneOutput = True
        if tag == 'tr':
          self.inRowTwo = False
          self.doneRowTwo = True

          # Skip build jobs
          if self.type.count('build') == 0:
            # Dump out grabbed data
            job = Job()
            job.owner = self.owner
            job.pandaID = self.pandaID
            job.status = self.status
            job.created = self.created
            job.timeToStart = self.time
            job.duration = self.duration
            job.ended = self.ended
            job.cloud = self.cloud
            job.site = self.site
            job.type = self.type
            job.priority = self.priority
            job.inputDataset = self.input
            job.outputDataset = self.output

            # Add job to the container
            self._jobContainer.append(job)

          # End of jobset, so reset
          self._resetRowVars()

  def handle_data(self, data):
    if self.inPandaID:
      self.pandaID = int(data.strip())
    if self.inOwner:
      self.owner = data.strip()
    if self.inJobsetID:
      self.jobsetID = int(data.strip())
    if self.inJobInfo:
      self.jobInfo = data.strip()
    if self.inStatus:
      self.status = data.strip()
    if self.inCreated:
      self.create = data.strip()
    if self.inTime:
      self.time = data.strip()
    if self.inDuration:
      self.duration = data.strip()
    if self.inEnded:
      self.ended = data.strip()
    if self.inCloud:
      self.cloud = data.strip()
    if self.inSite:
      self.site = data.strip()
    if self.inCloudSiteType and not self.inCloud and not self.inSite:
      raw = data.strip(' ,')
      if raw != '/':
        self.type = raw
    if self.inPriority:
      self.priority = int(data.strip())
    if self.inInOrOutTest:
      testval = data.strip()
      if testval.count('Out') > 0:
        self.doneInput = True
    if self.inInput:
      self.input = data.strip()
    if self.inOutput:
      self.output = data.strip()

class JobContainer(object):
  '''Object to hold and manipulate many Job objects'''
  def __init__(self):
    self._joblist = []

  def __str__(self):
    '''Summarise object'''
    s = ''
    for job in self._joblist:
      s += '{0}\n'.format(job)
    return s

  def __len__(self):
    return len(self._joblist)

  def __contains__(self, pandaID):
    return (self.byID(pandaID) is not None)

  def __getitem__(self, pandaID):
    if pandaID not in self:
      raise IndexError(pandaID)
    return self.byID(pandaID)

  def byID(self, pandaID):
    ret = None
    for job in self._joblist:
      if job.pandaID == pandaID:
        ret = job
    return ret

  def append(self, job):
    '''Add a job to the collection'''
    self._joblist.append(job)

  def all(self):
    '''Return list of all jobs'''
    return list(self._joblist)

  def finished(self):
    '''Return list of finished jobs'''
    ret = []
    for job in self._joblist:
      if job.isFinished():
        ret.append(job)
    return ret

  def failed(self):
    '''Return list of failed jobs'''
    ret = []
    for job in self._joblist:
      if job.isFailed():
        ret.append(job)
    return ret

  def notFinished(self):
    '''Return list of jobs that aren't finished'''
    ret = []
    for job in self._joblist:
      if not job.isFinished():
        ret.append(job)
    return ret

  def updateMetadata(self):
    '''Update the metadata of all child jobs'''
    for job in self._joblist:
      job.updateMetadata()

  def blocks(self):
    '''Return a list of the blocks (datsasets) used'''
    blocklist = []
    for job in self._joblist:
      if not job.outputBlock in blocklist:
        blocklist.append(job.outputBlock)
    return blocklist

class Job(object):
  '''Container class to hold information about a job scraped from the site'''

  def __init__(self):
    self.jobsetID = 0

    self.owner = ''
    self.pandaID = 0
    self.status = ''
    self.created = ''
    self.timeToStart = ''
    self.duration = ''
    self.ended = ''
    self.cloud = ''
    self.site = ''
    self.type = ''
    self.priority = ''
    self.inputDataset = ''
    self.outputDataset = ''

    # Information gleaned from second parsing about consituent files
    self.inputFiles = []
    self.outputBlock = ''
    self.outputFile = ''
    self.outputSize = 0

  def __str__(self):
    '''Summarise job'''
    return '{0}: {1} - {2} ({3}) \n    {4}\n'.format(self.pandaID, self.status, self.outputFile, self.outputSize, '\n    '.join(self.inputFiles))

  def isFinished(self):
    '''Return true iff status is 'finished' '''
    return self.status == 'finished'

  def isFailed(self):
    '''Return true iff status is 'failed' '''
    return self.status == 'failed'
  
  def updateMetadata(self):
    '''Assign the metadata object for this job'''
    import pandaJobParser
    metadata = pandaJobParser.getJobMetadata(self.pandaID)
    self.inputFiles = metadata.namesByInDS(self.inputDataset) 
    self.outputFile = metadata.getOutputFile()
    self.outputBlock = metadata.guessOutputDataset()
    self.outputSize = metadata.outputFileBytes

def _downloadPage(url):
  '''Small helper function to download the url as a string'''
  import urllib
  s = None
  while s is None:
    try:
      f = urllib.urlopen(url)
      s = f.read()
      f.close()
    except:
      # Try again in 30 seconds...
      import time
      print 'PandaJobsetParser: timeout, retrying in 30s'
      time.sleep(30)
  return s

def getJobContainer(names, jobsetID):
  '''Main entry point for the module. Will return a job container for the given user. Names
  should be a list of first (middle) last names, as it appears on the users page on Pandamon'''
  name = '%20'.join(names)
  url = 'http://panda.cern.ch/server/pandamon/query?job=*&jobsetID={0}&name={1}'.format(jobsetID, name)
  pandaSource = _downloadPage(url)

  # More workarounds...
  pandaSource = pandaSource.replace('>&nbsp;<', '> &nbsp; <')
  # Error extract workaround
  lindex = pandaSource.find('<div id="9" style="display: visible">')
  rindex = -1
  if lindex != -1:
    rindex = pandaSource.find('</div>', lindex)
  if lindex != -1 and rindex != -1:
    pandaSource = pandaSource[:lindex]+pandaSource[rindex+6:]

  lindex = pandaSource.find('<div id="9" style="display: none">')
  rindex = -1
  if lindex != -1:
    rindex = pandaSource.find('</div>', lindex)
  if lindex != -1 and rindex != -1:
    pandaSource = pandaSource[:lindex]+pandaSource[rindex+6:]

  pandaContainer = JobContainer()
  parser = PandaJobsetParser(pandaContainer)
  parser.feed(pandaSource)

  # Get metadata from panda
  pandaContainer.updateMetadata()

  return pandaContainer

# Some testing code
def main():
  container = getJobContainer(['thomas', 'gillam'], 55389)
  #print container 
  print len(container)
  for job in container._joblist:# finished():
    print job.pandaID
    print job.inputFiles
    print job.outputFile
    print job.outputBlock
    print job.outputSize
    print job.inputDataset
    print job.outputDataset
    print

if __name__ == '__main__':
  main()
