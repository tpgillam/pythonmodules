#!/usr/bin/env python

class CommandPoller(object):
  def __init__(self, commandsName):
    self._commandsName = commandsName
    import socket
    self._nodeName = socket.gethostname()
    self._waitingTime = 0.5

    self._commands = []
    self._processedIDs = set()
    self._numSubmitted = 0
  
  def runAndWait(self):
    while(1):
      self._updateCommandsFromFile()
      if self._numWaitingProcs() > 0:
        self._submitWatingProcs()
      self._cleanUpFinished()
      import time
      time.sleep(self._waitingTime)

  def _cursorCallback(self, callback):
    import sqlite3
    conn = sqlite3.connect(self._commandsName, timeout=60)
    cursor = conn.cursor()
    ret = callback(cursor)
    conn.commit()
    conn.close()
    return ret

  def _updateCommandsFromFile(self):
    class CommandAndProcess(object):
      def __init__(self, id, command):
        if not isinstance(command, str):
          print 'WARNING: creating command with not string, attempting to cast'
          print 'Type is:',type(command)
          print 'Command is:',command
        self.id = id
        self.command = str(command)
        self.process = None

      def procState(self):
        '''0 = not started, 1 = running, 2 = finished/failed, 3 = cleaned up'''
        if self.process == None:
          return 0
        elif self.process == 'done':
          return 3
        returncode = self.process.poll()
        if returncode is None:
          return 1
        else:
          self.returncode = returncode
          return 2

      def flushOutput(self):
        self.process.communicate()
        import sys
        sys.stdout.flush()


    def readCommandsCallback(cursor):
      return list(cursor.execute('SELECT * FROM commands WHERE status == 0'))
    rows = self._cursorCallback(readCommandsCallback)

    for row in rows:
      id = row[0]
      command = row[1]
      if not id in self._processedIDs:
        self._processedIDs.add(id)
        if isinstance(command, unicode):
          import unicodedata 
          command = unicodedata.normalize('NFKD', command).encode('ascii')
        self._commands.append(CommandAndProcess(id, command))
        if command == 'KILL_NOW':
          self._kill()

  def _kill(self):
    for command in self._commands:
      if command.process is not None and not isinstance(command.process, basestring):
        command.process.kill()
    import sys
    sys.exit()

  def _cleanUpFinished(self):
    for command in self._commands:
      # If a job has finished, but not been cleaned up...
      if command.procState() == 2:
        self._writeFinishedIndexWithExitCode(command.id, command.returncode)

        command.flushOutput() 
        #print 'CommandPoller ('+self._nodeName+'): Cleaning up '+str(command.process.pid)
        del command.process
        command.process = 'done'

  def _writeFinishedIndexWithExitCode(self, id, exitCode):
    def writeFinishedCallback(cursor):
      cursor.execute('UPDATE commands SET status = 1, exitCode = {1} WHERE id = {0}'.format(id, exitCode))
      #cursor.execute('DELETE FROM commands WHERE id = {0}'.format(id))
    self._cursorCallback(writeFinishedCallback)

  def _submitWatingProcs(self):
    for command in self._commands:
      # Submit a job if the process hasn't already been spawned.
      if command.procState() == 0:
        import subprocess
        try:
          process = subprocess.Popen(command.command, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT, shell=True, executable='/bin/bash')
        except TypeError, e:
          print 'Type error in job creation!'
          print 'Command being submitted:'
          print command.command
          print 'End command'
          print 'Type of command:',type(command)
          raise e
        self._numSubmitted += 1
        #print 'CommandPoller ('+self._nodeName+'): Spawned process '+str(process.pid)+', '+str(self._numSubmitted)+'/'+str(len(self._commands))
        command.process = process

  def _numWaitingProcs(self):
    '''Determine the number processes waiting to submit'''
    count = 0
    for command in self._commands:
      if command.procState() == 0:
        count += 1
    return count

  def _numActiveProcs(self):
    '''Determine the number of active processes'''
    count = 0
    for command in self._commands:
      if command.procState() == 1:
        count += 1
    return count

  def _numFinishedProcs(self):
    '''Determine the number of finished/failed processes'''
    count = 0
    for command in self._commands:
      if command.procState() == 2:
        count += 1
    return count

  def _numCleanedProcs(self):
    '''Determine the number of cleaned up processes'''
    count = 0
    for command in self._commands:
      if command.procState() == 3:
        count += 1
    return count

def main():
  import sys
  configDir = ''
  if len(sys.argv) > 1:
    configDir = sys.argv[1]

  import socket
  host = socket.gethostname()

  import os.path
  commandsName = os.path.join(configDir, 'commands.'+host)
  poller = CommandPoller(commandsName)
  poller.runAndWait()

if __name__ == '__main__':
  main()
