# TODO TODO TODO
# Not finished!!!
# TODO TODO TODO


# The finer-grained interation of the Sledgehammer. Performs more robust validation
# of downloaded files, and when a download fails will only resubmit those input
# files that aren't accounted for in the output that is present

import pandaSledgehammer

class Clawhammer(pandaSledgehammer.Sledgehammer):
  def __init__(self):
    super(Clawhammer, self).__init__()

    # Split up jobs by dataset
    self._dq2MaxJobs = 5

  # Overload to use _ClawJob
  def append(self, command, outputDir, pattern=''):
    '''Append a job to the list to run. Call AFTER setting other parameters!!!'''
    self._jobs.append(_ClawJob(command, outputDir, pattern, resubmitLevel=0, gridSetupScript=self._gridSetupScript, dq2SetupScript=self._dq2SetupScript, names = self._names, days = self._days))

  def run(self):
    '''Hold on tight!'''
    self._printWelcome()

    self._print('Searching for resubmits...')

    import pandaParser
    self._print('parsing Panda')
    pandasummary = pandaParser.getJobsetContainer(self._names, self._days)
    newjobs = []
    for job in self._jobs:
      # Update panda for each job
      job.pandaUpdate(pandasummary, self._pandaRetryLimit)
      
      prevjob = job
      finished = False
      count = 1
      while not finished:
        testdataset = '{0}_X{1}/'.format(job.outputDataset.strip('/'), count)
        self._print('Looking for {0}'.format(testdataset))

        if len(pandasummary.byOutDS(testdataset)) > 0:
          # This means that the _X dataset exists
          self._print('Found {0}'.format(testdataset))

          # 1. Validate the previous prevjob, and set to dq2 failed...
          prevjob.validate()
          prevjob.dq2NumFails = self._dq2RetryLimit
          prevjob.flaggedForDeletion = True

          # 2. Add a prevjob for the new dataset
          newjob = _ClawJob(prevjob.command, prevjob.dq2OutputDir, prevjob.dq2Pattern, prevjob.resubmitLevel+1, prevjob.gridSetupScript, prevjob.dq2SetupScript, self._names, self._days, inputFiles = prevjob.dq2UnrepresentedInput)
          newjob.pandaUpdate(pandasummary, self._pandaRetryLimit)

          newjobs.append(newjob)
          prevjob = newjob
        else:
          finished = True

        # Increment the counter...
        count += 1

    self._jobs += newjobs
    self._print('Finished pre-run prep')
    self._printCurrentStatus()

    self._print('Entering execution loop...')
    while self._dq2StillGoing():
      # Parse panda
      import pandaParser
      self._print('parsing Panda')
      pandasummary = pandaParser.getJobsetContainer(self._names, self._days)

      for job in self._jobs:
        # A little hacky, for performance reasons... Update job stats from panda
        # but only if it isn't completed/abandoned
        if not job.abandoned and not job.pandaCompleted:
          # Panda stuff
          job.pandaUpdate(pandasummary, self._pandaRetryLimit)
          # If not running and not stuck, resubmit
          if not job.pandaCompleted and not job.abandoned and not job.pandaRunning and not job.pandaStuck:
            job.pandaSubmit()
          elif job.pandaStuck:
            self._print('{0} is stuck (maybe transferring?).'.format(job.outputDataset))
        elif not job.abandoned or (job.abandoned and self._shouldDownloadAbandoned):
          # Now we should look at DQ2 stuff...

          # If already running, poll
          if job.dq2Process != None:
            job.dq2Poll(self._dq2Timeout)
            if job.dq2Process == None:
              self._dq2Count -= 1

          # If failed, abandon this job, and retry...
          if job.dq2NumFails >= self._dq2RetryLimit and not job.flaggedForDeletion:
            self._print('abandoning download for {0}'.format(job.outputDataset))
            if job.dq2Process != None:
              self._dq2Count -= 1
              job._dq2Terminate()

            # Validate the job to get unrepresented input files...
            self._print('Validating on fail, {0}'.format(job.outputDataset))
            self._validateCleanAndGo(job)
            job.flaggedForDeletion = True
            continue

          # If we've completed (successfully?!) but haven't validated...
          if job.dq2Completed and not job.dq2Validated:
            self._print('Validating on succeed, {0}'.format(job.outputDataset))
            self._validateCleanAndGo(job)

          # If we're not running...
          if not job.dq2Completed and job.dq2Process == None and not job.flaggedForDeletion and self._dq2Count < self._dq2MaxJobs:
            job.dq2Submit()
            self._dq2Count += 1

      # Print status summary after each pass
      self._printCurrentStatus()

      import time
      time.sleep(self._delay)

    # Output summary
    self._printSummary()

  ##### INTERNAL FUNCTIONS #####

  def _dq2StillGoing(self):
    '''Returns false if all jobs listed as completed. Take into account jobs that
    have been retried the maximum number of times'''
    import re

    # Dictionary of datasets -> (still going, iteration)
    stillGoingDict = {}
    for job in self._jobs:
      match = re.search('(\S+)_X(\d+)/', job.outputDataset)
      dataset = ''
      index = 0
      if match != None:
        dataset = match.group(1)+'/'
        index = int(match.group(2))
      else:
        dataset = job.outputDataset
        index = 0
      # Insert into dict if *either* key doesn't exist or current key is greater
      # than stored key
      stillGoing = False
      # (not completed) and (should have completed)
      # (should've completed) == (not abandoned) or (is abandoned and should download abandoned)
      # (completed) == is validated and no missing files
      if (not (job.dq2Validated and len(job.dq2MissingFiles) == 0)) and ((not job.abandoned) or (job.abandoned and self._shouldDownloadAbandoned)):
        stillGoing = True
      if dataset in stillGoingDict and index > stillGoingDict[dataset][1]:
        stillGoingDict[dataset] = (stillGoing, index)
      elif not dataset in stillGoingDict:
        stillGoingDict[dataset] = (stillGoing, index)

    # Return True if any datasets still going, else false
    for key in stillGoingDict:
      if stillGoingDict[key][0]:
        return True
    return False

  def _validateCleanAndGo(self, job):
    '''Validate the given job, and see if it passed. If not, resubmit with the failed input files'''
    job.validate()
    
    missingInput = job.dq2UnrepresentedInput

    # Test for whether this has failed...
    if len(missingInput) != 0:
      # Clean out any corrupted files
      if len(job.dq2CorruptedFiles) > 0:
        import os
        for name in job.dq2CorruptedFiles:
          try:
            self._print('Deleting corrupted file {0}'.format(os.path.join(job.dq2OutputDir, name)))
            os.remove(os.path.join(job.dq2OutputDir, name))
          except:
            self._print('Couldn\'t delete {0}'.format(os.path.join(job.dq2OutputDir, name)))

      # Create a new job using only the failed input files
      self._print('{0} missing input files for {1}, resubmitting'.format(len(missingInput), job.outputDataset))
      newjob = _ClawJob(job.command, job.dq2OutputDir, job.dq2Pattern, job.resubmitLevel+1, job.gridSetupScript, job.dq2SetupScript, self._names, self._days, inputFiles = missingInput)
      self._jobs.append(newjob)
    else:
      self._print('Successful validation for {0}'.format(job.outputDataset))

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Clawhammer:',msg

  def _printWelcome(self):
    '''Print welcome message'''
    print '''

     CCCCC  LL        AAA   WW      WW HH   HH   AAA   MM    MM MM    MM EEEEEEE RRRRRR  
    CC    C LL       AAAAA  WW      WW HH   HH  AAAAA  MMM  MMM MMM  MMM EE      RR   RR 
    CC      LL      AA   AA WW   W  WW HHHHHHH AA   AA MM MM MM MM MM MM EEEEE   RRRRRR  
    CC    C LL      AAAAAAA  WW WWW WW HH   HH AAAAAAA MM    MM MM    MM EE      RR  RR  
     CCCCC  LLLLLLL AA   AA   WW   WW  HH   HH AA   AA MM    MM MM    MM EEEEEEE RR   RR 

      ................................................................................
      ................ ~~:=  ,   .....................................................
      ...............:::7D~:::~. .....................................................
      .............~::~::::~~.........................................................
      ...........,~~~:~::~~= .........................................................
      ..........=~~===:~~=............................................................
      .........?+++=+~=++.............................................................
      ........IIIII7==++..............................................................
      .......??7??7=+?:.....................................................Z8888.....
      ......I?7I?I~+?7................................ .:ODD88D8DDDDDDDDDDDDNNNNNN....
      ......I7II7~=+?..........................M888DDDDDDNDNNNNNNNNNNNNNNNDNNMMMMMN...
      .....I77I7==+?IZM........ +I+:,..,,,,::~~MDNDDNNNNNMMMNNNNNNNMDNMNNNMNMNNMNMM...
      .....IIII+?ZI+IZZM++~I$MMMMMMM,,.,,.,,,,:NDDDNNNNNNMNMMNMNMMNMNMMMMMMMMMMMMMM...
      ....?IIII=+?I+7$$$?::~~~+$I7ZN,::..::~~~~NNNNMNNNMNMMMMMMMMMNMMMMMMMMMMMMMMNN ..
      ....IIII?====I$$$$$7OOOMMMMMMM::,:~~OOOONMMMMMMMMMMMMMMMMMNMMMMMMMMMMMMMMMMM ...
      ....7III++7$Z$$O+~~.......................,7DNNNMMNMMN8I,...........,~?7ZZ+.....
      ....$77I===+?$,..................................................,,,,,,,........
      ....D8ZD8==?I...................................................................
      .....NN=II$8 ...................................................................
      .....88~IIO:....................................................................
      .....OO:I?Z.....................................................................
      .....O=,Z+7.....................................................................
      ....ZZ::O=?: ...................................................................
      ...8$Z~~O:=O....................................................................
      ...D8N,~D+?D....................................................................
      ...D8N:?D??Z....................................................................
      ..=7NM77D8Z?....................................................................
      ...:=++??+~.....................................................................
      ................................................................................
      ................................................................................

    '''

  def _printCurrentStatus(self):
    '''Print a summary of the current jobs that are running, downloading etc'''
    print '--- Clawhammer Status ---'
    import datetime
    print 'Timestamp: {0}'.format(datetime.datetime.now())
    print 'DQ2 job count: {0}/{1}'.format(self._dq2Count, self._dq2MaxJobs)
    print 'Current jobs:'
    for job in self._jobs:
      print job
      status = '  '
      if job.pandaInputFiles != None:
        status += '({0} files) '.format(len(job.pandaInputFiles))
      if job.pandaRunning:
        status += 'Panda: running, ' 
      if job.pandaStuck:
        status += 'Panda: stuck, '
      if job.pandaCompleted:
        status += 'Panda: completed, '
      if job.abandoned:
        status += 'Panda: abandoned, '

      if job.dq2Process != None:
        import time
        status += 'DQ2: downloading, Runtime: {0}s, '.format(time.time()-job.dq2StartTime)
      if job.dq2Completed:
        status += 'DQ2: completed, '
      elif job.dq2Process == None and job.dq2NumFails >= self._dq2RetryLimit:
        status += 'DQ2: abandoned, '
      if job.dq2Validated:
        status += 'Validation: {0}/{1}, '.format(job.dq2NumPresentOutput, job.dq2NumActualOutput)
      status += 'Panda fails: {0}, DQ2 fails: {1}'.format(job.pandaNumFails, job.dq2NumFails)
      print status
    print '--------------------------\n'

  def _printSummary(self):
    '''Print a summary that we'd typically want to show at the end of the run'''
    print '\n\n=== CLAWHAMMER SUMMARY ==='
    print '==========================\n'
    for job in self._jobs:
      status = 'Completed'
      if job.abandoned:
        status = 'Abandoned'
      print '{0}: validated {1}/{2}, {3}'.format(status, job.dq2NumPresentOutput, job.dq2NumActualOutput, job.outputDataset)

class _ClawJob(pandaSledgehammer._Job):
  '''Internal class to represent a job'''
  def __init__(self, command, outputDir, pattern, resubmitLevel, gridSetupScript, dq2SetupScript, names, days, inputFiles = None):
    super(_ClawJob, self).__init__(command, outputDir, pattern, resubmitLevel, gridSetupScript, dq2SetupScript)

    # Files in input dataset to use when submitting
    # Use all if == ''
    self.pandaInputFiles = inputFiles

    # Days history to use for validator
    self.names = names
    self.days = days

    # Extra DQ2 info re validation
    self.dq2Validated = False
    self.dq2CorruptedFiles = []
    self.dq2MissingFiles = []
    self.dq2UnrepresentedInput = []
    self.dq2NumPresentOutput = 0
    self.dq2NumActualOutput = 0

    self.dq2OutputBlocks = []
    self.dq2Processes = [] # List of DQ2 processes...
    self.dq2StartTimes = [] # List of DQ2 start times

  def pandaSubmit(self):
    '''Submit this job to panda, noting the input file list. Blocks until done'''
    command = '. {0}; {1}'.format(self.gridSetupScript, self.command)

    tempFileName = ''
    if self.pandaInputFiles != None:
      tempFileName = self._getTempFilename()
      f = open(tempFileName, 'w')
      f.write('\n'.join(self.pandaInputFiles))
      f.close()

      command += ' --inputFileList={0}'.format(tempFileName)

    import subprocess
    self._print('Submitting {0}'.format(self.outputDataset))
    self._print('Executing: {0}'.format(command))
    subprocess.call(command, shell=True)

    import os
    if tempFileName != '':
      os.remove(tempFileName)

  def dq2Submit(self, nFreeSlots):
    '''Initial dq2 submit of the job. nFreeSlots represents how many jobs we
    are allowed to spawn on the first pass'''
  def dq2Poll(self, timeout):
    '''Poll the running dq2 process(es)
  def dq2Cleanse(self):

  def validate(self):
    '''Perform panda-based validation, and populate validation member variables'''
    self._print('Validating {0}'.format(self.outputDataset))
    import pandaValidator
    validator = pandaValidator.PandaValidator(self.names, self.days, self.outputDataset, self.dq2OutputDir)

    self.dq2CorruptedFiles = validator.corruptedOutputFiles()
    self.dq2MissingFiles = validator.missingOutputFiles()
    self.dq2UnrepresentedInput = validator.unrepresentedInputFiles()

    # Counts for status message printing
    self.dq2NumPresentOutput = len(validator.presentOutputFiles())
    self.dq2NumActualOutput = validator.numExpectedOutputFiles()

    self.dq2Validated = True

  ##### INTERNAL FUNCTIONS #####

  def _getComponentDatasets(self):
    '''Get the individual datasets that need to be downloaded as a part of 
    this dataset container, and store in memeber variable'''
    import pandaJobsetParser
    jobsetContainer = pandaParser.getJobsetContainer(self.names, self.days)
    self.dq2OutputBlocks = jobsetContainer.blocks()

  def _getTempFilename(self):
    '''Get a temporary filename in the present directory'''
    name = 'tmpinnames'
    import os.path
    while (os.path.exists(name)):
      name += '1'
    return name

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Clawhammer:',msg

