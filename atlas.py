def atlasStyle():
  import ROOT
  import os.path
  path = os.path.dirname(os.path.realpath(__file__))
  ROOT.gROOT.LoadMacro(os.path.join(path, 'AtlasStyle.C'))
  ROOT.gROOT.LoadMacro(os.path.join(path, 'AtlasLabels.C'))
  ROOT.SetAtlasStyle()
  # Allow us to set histogram background colours..!
  ROOT.gROOT.ForceStyle(0)

