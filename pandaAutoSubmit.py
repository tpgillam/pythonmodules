# Library to handle auto-submitting jobs to panda, until they've all succeeded.
# Inputs are the list of submission commands

class AutoSubmitter(object):
  '''Auto submit jobs to panda until all are completed'''

  def __init__(self):
    self._jobs = []
    self._delay = 600
    self._names = ['thomas', 'gillam']
    self._days = 3
    self._resubmitlimit = 5

  def append(self, command):
    '''Append a submission command to the list'''
    import re
    # TODO do we need to add more allowed characters into dataset name?
    outputDataset = re.search('--outDS[\'"=\s]*([-_./\w]+)', command).group(1)
    # Ensure it ends with a forward-slash
    if outputDataset[-1] != '/':
      outputDataset += '/'

    # Use the --disableAutoRetry option to prevent the servers screwing us over
    if command.count('--disableAutoRetry') == 0:
      command += ' --disableAutoRetry'

    # Build submit object
    obj = _SubmitObject()
    obj.command = command
    obj.outputDataset = outputDataset

    self._jobs.append(obj)

  def setDelay(self, delay):
    '''Set the delay (in seconds) between polling the panda website'''
    self._delay = delay

  def setNames(self, names):
    '''Set your names! Important if you want this to work... ;-)'''
    self._names = names

  def setDaysHistory(self, days):
    '''Set number of days history to use in panda. Default is 3'''
    self._days = days

  def setResubmitLimit(self, limit):
    '''Set the maximum number of times we should attempt to resubmit
    any given command'''
    self._resubmitlimit = limit

  def run(self):
    '''Run all the commands, return when everything is finished'''
    # Update jobs first, just in case... 
    import pandaParser
    pandasummary = pandaParser.getJobsetContainer(self._names, self._days)
    for job in self._jobs:
      self._updateJob(pandasummary, job)

    # Continue until all jobs are finished
    while self._stillGoing():
      # Query the panda website
      pandasummary = pandaParser.getJobsetContainer(self._names)

      # Iterate over all jobs we're working with
      for job in self._jobs:
        # Update job status from panda
        self._updateJob(pandasummary, job)

        # Giving up with this job...?
        if job.numFails == self._resubmitlimit:
          print 'AutoSubmitter:',job.outputDataset,'has failed',job.numFails,'times, skipping'
          continue
        
        # Jobs that aren't completed, and haven't all succeeded should
        # be resubmitted
        if not job.completed and not job.running:
          self._submitJob(job)
        elif job.running:
          pass
          #print 'AutoSubmitter:',job.outputDataset,'is running.'
        elif job.completed:
          pass
          #print 'AutoSubmitter:',job.outputDataset,'is completed.'
        if job.stuck:
          print 'AutoSubmitter:',job.outputDataset,'is stuck (maybe transferring?).'

      # Sleep for the delay time
      import time
      time.sleep(self._delay)

  ##### INTERNAL FUNCTIONS #####

  def _submitJob(self, job):
    '''Submit the command for the given job. Blocks until done'''
    import subprocess
    print 'AutoSubmitter: Submitting', job
    print 'AutoSubmitter: Executing:', job.command
    subprocess.call(job.command, shell=True)

  def _updateJob(self, pandasummary, job):
    '''Given the panda summary from pandaParser, and a job, update
    its status flags'''
    pjobs = pandasummary.byOutDS(job.outputDataset)
    running = False
    completed = False
    stuck = False
    numFails = 0
    for pjob in pjobs:
      if not pjob.isFinished():
        running = True
      # FIXME: Breaks if more than one jobset running simultaneously (e.g.
      # if site decides to auto-resubmit itself
      elif pjob.isFinished():
        if pjob.isFinishedSuccessfully():
          completed = True
        else:
          # This is a failure...
          numFails += 1
      if pjob.isStuck():
        stuck = True

    # Take into account the fact that if jobs are still running,
    # cannot be completed
    if running:
      completed = False

    # Update the job container
    job.completed = completed
    job.running = running
    job.stuck = stuck
    job.numFails = numFails

  def _stillGoing(self):
    '''Find out whether any jobs are listed as incomplete...'''
    for job in self._jobs:
      if not job.completed and not job.numFails == self._resubmitlimit:
      #if job.running or (not job.completed and not self.numFails == self._resubmitlimit):
        return True
    return False

class _SubmitObject(object):
  '''Internal class to summarise the content and state of a job'''
  def __init__(self):
    self.command = ''
    self.outputDataset = ''
    self.numFails = 0
    self.completed = False
    self.running = False
    self.stuck = False

  def __str__(self):
    return self.outputDataset
