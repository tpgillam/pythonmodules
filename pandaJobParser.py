# Library to scrape jobt information from the Pandamon website, and return
# in an easy-to-use container class.

import HTMLParser

class PandaJobParser(HTMLParser.HTMLParser):
  def __init__(self, jobMetadata):
    HTMLParser.HTMLParser.__init__(self)

    # Save reference to the job container object
    self._jobMetadata = jobMetadata

    # Initialise state variables
    self.inFileTable = False
    self.inFileTableBody = False

    self._resetRowVars()

  def _resetRowVars(self):
    self.inRow = False
    self.doneRow = False

    self.inFilenameBox = False
    self.inFilename = False
    self.inType = False
    self.inStatus = False
    self.inDatasetBox = False
    self.inDataset = False
    self.inBlock = False

    self.doneFilenameBox = False
    self.doneFilename = False
    self.doneType = False
    self.doneStatus = False
    self.doneDatasetBox = False
    self.doneDataset = False
    self.doneBlock = False

    self.filename = ''
    self.type = ''
    self.status = ''
    self.dataset = ''
    self.block = ''

  def handle_starttag(self, tag, attrs):
    if not self.inFileTable and tag == 'table' and ('id', 'fileNameTableId0') in attrs:
      self.inFileTable = True
    elif self.inFileTable:
      if not self.inFileTableBody and tag == 'tbody':
        self.inFileTableBody = True
      if self.inFileTableBody:
        if not self.doneRow and tag == 'tr' and len(attrs) == 0:
          self.inRow = True
        if self.inRow:
          # Handle row tags
          if not self.doneFilenameBox:
            if tag == 'td':
              self.inFilenameBox = True
            if self.inFilenameBox:
              if not self.doneFilename and tag == 'a':
                self.inFilename = True
          elif not self.doneType and tag == 'td':
            self.inType = True
          elif not self.doneStatus and tag == 'td':
            self.inStatus = True
          elif not self.doneDatasetBox:
            if tag == 'td':
              self.inDatasetBox = True
            if self.inDatasetBox:
              if not self.doneDataset and tag == 'a':
                self.inDataset = True
              elif not self.doneBlock and tag == 'a':
                self.inBlock = True

  def handle_endtag(self, tag):
    if self.inFileTable:
      if tag == 'table':
        self.inFileTable = False
        self.doneFileTable = True
      if self.inFileTableBody:
        if tag == 'tbody':
          self.inFileTableBody = False
          self.doneFileTableBody = True
        if self.inRow:
          if self.inFilenameBox:
            if tag == 'td':
              self.inFilenameBox = False
              self.doneFilenameBox = True
            if self.inFilename and tag == 'a':
              self.inFilename = False
              self.doneFilename = True
          elif self.inType and tag == 'td':
            self.inType = False
            self.doneType = True
          elif self.inStatus and tag == 'td':
            self.inStatus = False
            self.doneStatus = True
          elif self.inDatasetBox:
            if tag == 'td':
              self.inDatasetBox = False
              self.doneDatasetBox = True
            if self.inDataset and tag == 'a':
              self.inDataset = False
              self.doneDataset = True
            elif self.inBlock and tag == 'a':
              self.inBlock = False
              self.doneBlock = True
          if tag == 'tr':
            if self.type == 'input':
              self._jobMetadata.inputFiles.append((self.filename, self.status, self.dataset, self.block))
            if self.type == 'log':
              self._jobMetadata.logFiles.append((self.filename, self.status, self.dataset, self.block))
            if self.type == 'output':
              self._jobMetadata.outputFiles.append((self.filename, self.status, self.dataset, self.block))
            
            self._resetRowVars()

  def handle_data(self, data):
    if self.inFilename: self.filename = data.strip()
    if self.inType: self.type = data.strip()
    if self.inStatus: self.status = data.strip()
    if self.inDataset: self.dataset = data.strip()
    if self.inBlock: self.block = data.strip()

class JobMetadata(object):
  def __init__(self):
    # Each element should be a tuple of strings (name, status, dataset, block)
    self.inputFiles = []
    # TODO We expect these to be length 1, so throw up a warning if not...
    self.logFiles = []
    self.outputFiles = []

    self.computingSite = ''
    self.destinationSE = ''
    self.nEvents = 0
    self.processingType = ''
    self.outputFileBytes = 0

  def namesByInDS(self, name):
    '''Return a list of input file names given the input dataset (container).
    Remember the forward slash...'''
    ret = []
    for tup in self.inputFiles:
      if tup[2] == name:
        ret.append(tup[0])
    return ret

  def getOutputFile(self):
    '''Return the output file. print a warning if there's more than one...'''
    if len(self.outputFiles) > 1:
      print 'PandaJobParser: WARNING!!! More than one output file!'
    elif len(self.outputFiles) == 0:
      return None
    return self.outputFiles[0][0]

  def guessOutputDataset(self):
    '''Return the best guess for the output *dataset* (not container) in which the
    output file has been stored. *Doesn't end in a forward slash*'''
    if len(self.outputFiles) > 1:
      print 'PandaJobParser: WARNING!!! More than one output file!'
    elif len(self.outputFiles) == 0:
      return None
    tokens = self.outputFiles[0][3].split('.') 
    tokens[-1] = tokens[-1].split('_')[0]
    return '.'.join(tokens)

def _downloadPage(url):
  '''Small helper function to download the url as a string'''
  import urllib
  s = None
  while s == None:
    try:
      f = urllib.urlopen(url)
      s = f.read()
      f.close()
    except:
      # Try again in 30 seconds...
      import time
      print 'PandaJobParser: timeout, retrying in 30s'
      time.sleep(30)
  return s

def _getParameter(source, param):
  '''Regexp bash to get the parameter we're interested in'''
  import re
  x = ''
  try:
    x = re.search('<tr><td>'+param+'</td><td>(\S+)</td></tr>', source).group(1)
  except:
    pass
  return x

def getJobMetadata(pandaID):
  '''Main entry point for the module. Returns a class with information about the job,
  given its panda ID'''
  url = 'http://panda.cern.ch/server/pandamon/query?job={0}'.format(pandaID)

  pandaSource = _downloadPage(url)

  # More workarounds...
  pandaSource = pandaSource.replace('>&nbsp;<', '> &nbsp; <')
  # Error extract workaround
  pandaSource = removeLogExtractsFromSource(pandaSource)

  pandaContainer = JobMetadata()
  parser = PandaJobParser(pandaContainer)
  try:
    parser.feed(pandaSource)
  except Exception as moo:
    print url
    print pandaSource
    raise moo

  # Now use regular expressions to pull out other parameters
  pandaContainer.computingSite = _getParameter(pandaSource, 'computingSite')
  pandaContainer.destinationSE = _getParameter(pandaSource, 'destinationSE')
  nEvents = _getParameter(pandaSource, 'nEvents')
  if nEvents == '--' or not nEvents.isdigit():
    pandaContainer.nEvents = 0
  else:
    pandaContainer.nEvents = int(nEvents)
  pandaContainer.processingType = _getParameter(pandaSource, 'processingType')
  outputFileBytes = _getParameter(pandaSource, 'outputFileBytes')
  if outputFileBytes == '--':
    pandaContainer.outputFileBytes = 0
  else:
    pandaContainer.outputFileBytes = int(outputFileBytes)

  return pandaContainer

def removeLogExtractsFromSource(pandaSource):
  beginTag = '<div id="9"'
  endTag = '</div>'

  lindex = pandaSource.find(beginTag)
  rindex = -1
  if lindex != -1:
    rindex = pandaSource.find(endTag, lindex)
  if lindex != -1 and rindex != -1:
    pandaSource = pandaSource[:lindex]+pandaSource[rindex+len(endTag):]
  return pandaSource

# Some testing code
def main():
  metadata = getJobMetadata(1568519657)
  #print metadata.namesByInDS('group10.phys-susy.data11_7TeV.periodG.physics_JetTauEtmiss.PhysCont.NTUP_SUSYSKIM.pro10_v01_p832/')
  print 'Output file:',metadata.getOutputFile()
  print 'Guessed output dataset:',metadata.guessOutputDataset()
  #print 'Input files:',metadata.namesByInDS('therearenoinputs') 
  print metadata.inputFiles
  print metadata.computingSite
  print metadata.destinationSE
  print metadata.nEvents
  print metadata.processingType
  print metadata.outputFileBytes

if __name__ == '__main__':
  main()
