import multiprocessing
import multiprocessing.managers
import Queue

AUTHKEY = 'moomin'

def factorizer_worker(job_q, result_q, context):
  while True:
    try:
      job = job_q.get_nowait()
      id = job[0]
      functionName = job[1]
      functionObject = getattr(context, functionName)
      arg = job[2]
      outdict = {}
      import cPickle
      outdict[id] = cPickle.dumps(functionObject(arg), protocol=2)
      result_q.put(outdict)
    except Queue.Empty:
      return


def mp_factorizer(shared_job_q, shared_result_q, nprocs, context):
  """ Split the work with jobs in shared_job_q and results in
      shared_result_q into several processes. Launch each process with
      factorizer_worker as the worker function, and wait until all are
      finished.
  """
  procs = []
  for i in range(nprocs):
    p = multiprocessing.Process(
            target=factorizer_worker,
            args=(shared_job_q, shared_result_q, context))
    procs.append(p)
    p.start()

  for p in procs:
    p.join()


class Worker(object):
  def __init__(self, hostname, numProcesses, callingModulePath, serverPort, workerPort, extraSetup=''):
    self.hostname = hostname
    self.numProcesses = numProcesses
    self.callingModulePath = callingModulePath
    self.serverPort = serverPort
    self.workerPort = workerPort
    self.extraSetup = extraSetup
    self._connectedOnce = False
    self.connect()

  def connect(self):
    self.proc = runClientOnRemote(self.hostname, self.numProcesses, self.callingModulePath, self.serverPort, self.workerPort, self.extraSetup)
    self._connectedOnce = True

  def retryIfNecessary(self):
    if not self._connectedOnce:
      self.connect()
      return
    returncode = self.proc.poll()
    if returncode is not None and returncode != 0:
      self.workerPort += 1
      self.connect()

  def isFinished(self):
    if not self._connectedOnce:
      self.connect()
      return
    returncode = self.proc.poll()
    if returncode is not None and returncode == 0:
      return True
    return False



def runserver(function, arglist, machineconfig=None):
  # Start a shared manager server and access its queues
  serverPort = 15380
  workerPort = 15381
  succeeded = False
  while not succeeded:
    try:
      manager = make_server_manager(serverPort, AUTHKEY)
      succeeded = True
    except EOFError:
      serverPort += 2
      workerPort += 2
  shared_job_q = manager.get_job_q()
  shared_result_q = manager.get_result_q()

  import inspect, os.path
  callingModulePath = os.path.abspath(inspect.getouterframes(inspect.currentframe())[1][1])

  id = 0
  for arg in arglist:
    shared_job_q.put((id, function.__name__, arg))
    id += 1

  numJobs = shared_job_q.qsize()

  import sys
  sys.path.append('/usera/gillam/PythonModules/')
  import progressbar
  widgets = ['Completion: ', progressbar.Percentage(), ' ', progressbar.Bar(marker='=',left='[',right=']'), ' ', progressbar.ETA()]
  pbar = progressbar.ProgressBar(widgets=widgets, maxval=numJobs)

  if machineconfig is None:
    machineconfig = [
        ('pckg', 8),
        ]
  workers = []
  for machine in machineconfig:
    if len(machine) == 2:
      workers.append(Worker(machine[0], machine[1], callingModulePath, serverPort, workerPort))
    elif len(machine) == 3:
      workers.append(Worker(machine[0], machine[1], callingModulePath, serverPort, workerPort, machine[2]))

  import time
  time.sleep(1)

  pbar.start()
  total = len(workers)
  while True:
    numResults = shared_result_q.qsize()
    pbar.update(numResults)
    if numResults == numJobs:
      break
    for worker in workers:
      worker.retryIfNecessary()
    time.sleep(1)
  pbar.finish()

  for worker in workers:
    if worker.proc.poll() is None:
      worker.proc.terminate()
  
  # Wait until all results are ready in shared_result_q
  numresults = 0
  resultdict = {}
  while numresults < numJobs:
    outdict = shared_result_q.get()
    resultdict.update(outdict)
    numresults += len(outdict)

  # Sleep a bit before shutting down the server - to give clients time to
  # realize the job queue is empty and exit in an orderly way.
  # import time
  # time.sleep(2)
  manager.shutdown()

  resultList = []
  import cPickle
  for i in xrange(len(resultdict)):
    resultList.append(cPickle.loads(resultdict[i]))

  return resultList



def make_server_manager(port, authkey):
    """ Create a manager for the server, listening on the given port.
        Return a manager object with get_job_q and get_result_q methods.
    """
    job_q = Queue.Queue()
    result_q = Queue.Queue()

    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class JobQueueManager(multiprocessing.managers.SyncManager):
        pass

    JobQueueManager.register('get_job_q', callable=lambda: job_q)
    JobQueueManager.register('get_result_q', callable=lambda: result_q)

    manager = JobQueueManager(address=('', port), authkey=authkey)
    manager.start()
    print 'Server started at port %s' % port
    return manager



def runclient():
    import sys
    workerPort = int(sys.argv[1])
    numProcesses = int(sys.argv[2])
    callingModulePath = sys.argv[3]
    manager = make_client_manager('localhost', workerPort, AUTHKEY, numProcesses)
    job_q = manager.get_job_q()
    result_q = manager.get_result_q()
    import imp
    import os, sys
    sys.path.append(os.path.dirname(callingModulePath))
    moduleName = os.path.basename(callingModulePath).replace('.py','')
    context = imp.load_source(moduleName, callingModulePath)
    mp_factorizer(job_q, result_q, numProcesses, context)


def make_client_manager(ip, port, authkey, numProcesses):
    """ Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_job_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
    """
    class ServerQueueManager(multiprocessing.managers.SyncManager):
        pass

    ServerQueueManager.register('get_job_q')
    ServerQueueManager.register('get_result_q')

    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    manager.connect()

    import socket
    print 'Worker created on {0}:{1} with {2} slots'.format(socket.gethostname(), port, numProcesses)
    return manager

def runClientOnRemote(hostname, numProcesses, callingModulePath, serverPort, workerPort, extraSetup=''):
  import subprocess
  extraSetup = extraSetup.strip()
  if extraSetup != '':
    if not extraSetup.endswith(';'):
      extraSetup += ';'
  startClient = '{0} python /usera/gillam/PythonModules/gillam/triffid.py'.format(extraSetup)
  command = 'ssh -q -o "ExitOnForwardFailure yes" -R localhost:{1}:localhost:{0} {2} "{3} {1} {4} {5}"'.format(serverPort, workerPort, hostname, startClient, numProcesses, callingModulePath)
  proc = subprocess.Popen(command, shell=True)
  return proc


if __name__ == '__main__':
  runclient()
