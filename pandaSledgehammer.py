# Library to handle auto-submission of jobs to panda, and auto downloading
# to a directory. Also, it will (hopefully) be smart enough to:
#
# 1) Be downloading a completed dataset & resubmitting failed datasets in
#    parallel, to maximise chance of successful download.
#
# 1.1) Only submit 1 dataset at a time, and download 1 dataset. For single datasets,
#      assume that submission is potentially much faster than download.
#
# 1.2) Run submission in a blocking call. Run download using Popen, then poll
#      for completion, and continue resubmitting if there's a download job
#      still going
#
# 1.3) This can be constructed by a monolithic while loop with sufficient
#      variables to store the state of the various aspects
#
# 2) If download fails/times out after specified number of retries, clean
#    up the downloaded files. Then, remove the job from the list, and add
#    a new job with a modified datset name. (e.g. append -SH0, then -SH1 etc.)
#
# 2.1) It will be assumed that the user's merging script is smart enough to
#      ignore this postfix, so it will not be trimmed.
#
# 3) We still need a mechanism for ignoring jobs that fail in panda for a
#    good reason (e.g. some period B Muons having no events). After a certain
#    number of panda resubmissions, give up, but flag this to the user
#
# 4) We should assume a passphraseless certificate. Since there will be a lot of
#    panda resubmission (potentially), experiment with calling dq2-setup.sh
#    before pathena / prun to get the proxy initialised *without* having to
#    press enter to dismiss the prompt
#
# 4.1) This is probably messy - but voms-proxy isn't on the path from panda
#      init. If it works....
#
# 4.2) Better is to use grid_setup.sh :-) dq2 does indeed interfere with athena,
#      but grid_setup seems to be ok, and lets us call voms-proxy-init
#
# 5) Grapeshot mode: break down dataset containers into smallest possible
#    constituents before submitting & downloading
#
# 6) This method will probably eat grid priority for breakfast, lunch and dinner
#
# END UNREALISTIC WISHLIST

class Sledgehammer(object):
  '''Resubmit and download. Repeat, until it works. Show Don Quixote and the Panda who's boss...'''
  def __init__(self):
    self._jobs = []
    self._names = ['thomas', 'gillam']
    self._days = 3
    self._pandaRetryLimit = 100
    import os.path
    self._gridSetupScript = os.path.expanduser('~gillam/scripts/grid_setup.sh')
    self._dq2RetryLimit = 3
    self._dq2Timeout = 60*60 # Timeout in seconds
    self._dq2SetupScript = os.path.expanduser('~gillam/scripts/dq2_setup.sh')
    self._delay = 30 # Chillaxing time, in seconds
    self._dq2Count = 0 
    self._dq2MaxJobs = 3 # Let's only have one or two DQ2 process at a time...
    self._shouldDownloadAbandoned = False # Should we attempt to download abandoned datasets?

  def setNames(self, names):
    '''Set your names! Important if you want this to work... ;-)'''
    self._names = names

  def setDaysHistory(self, days):
    '''Set number of days history to use in panda. Default is 3'''
    self._days = days

  def setPandaRetryLimit(self, limit):
    '''Set the maximum number of times we should attempt to retry
    any given command'''
    self._pandaRetryLimit = limit

  def setDQ2RetryLimit(self, limit):
    '''Set the maximum number of times we should attempt to download
    any given dataset'''
    self._dq2RetryLimit = limit

  def setDQ2Timeout(self, timeout):
    '''Maximum amount of time (in s) to let a download job run before
    killing it and declaring it as failed'''
    self._dq2Timeout = timeout

  def setMaxDQ2Threads(self, numThreads):
    '''Maximum number of DQ2 threads to run concurrently'''
    self._dq2MaxJobs = numThreads

  def setDQ2Setup(self, dq2SetupScript):
    '''Set the location of the DQ2 setup script'''
    self._dq2SetupScript = dq2SetupScript

  def setGridSetup(self, gridSetupScript):
    '''Set the location of the Grid setup script'''
    self._gridSetupScript = gridSetupScript

  def setDelay(self, delay):
    '''Set a short delay to insert into the while loop'''
    self._delay = delay

  def setShouldDownloadAbandoned(self, flag):
    '''Should we attempt to download abandoned datasets?'''
    self._shouldDownloadAbandoned = flag

  def append(self, command, outputDir, pattern=''):
    '''Append a job to the list to run. Call AFTER setting other parameters!!!'''
    self._jobs.append(_Job(command, outputDir, pattern, resubmitLevel=0, gridSetupScript=self._gridSetupScript, dq2SetupScript=self._dq2SetupScript))

  def run(self):
    '''Hold on tight!'''
    while self._dq2StillGoing():
      # Only scrape the panda website if we still have jobs that are running
      pandasummary = None
      #if self._pandaStillGoing():
      self._print('parsing Panda')
      import pandaJsonParser
      pandasummary = pandaJsonParser.getJobsetContainer(self._names, self._days)
      #import pandaParser
      #pandasummary = pandaParser.getJobsetContainer(self._names, self._days)

      # Thin out defunct jobs
      self._jobs = [x for x in self._jobs if not x.flaggedForDeletion]

      # Print status summary after each pass
      self._printCurrentStatus()

      for job in self._jobs:
        # A little hacky, for performance reasons... Update job stats from panda
        # but only if it isn't completed/abandoned
        if not job.abandoned and not job.pandaCompleted:
          # Panda stuff
          job.pandaUpdate(pandasummary, self._pandaRetryLimit)
          # If not running and not stuck, resubmit
          if not job.pandaCompleted and not job.abandoned and not job.pandaRunning and not job.pandaStuck:
            job.pandaSubmit()
          elif job.pandaStuck:
            self._print('{0} is stuck (maybe transferring?).'.format(job.outputDataset))
        elif not job.abandoned or (job.abandoned and self._shouldDownloadAbandoned):
          # Now we should look at DQ2 stuff...
          #print 'Sledgehammer: DQ2 stuff;',job.outputDataset

          # If already running, poll
          if job.dq2Process != None:
            job.dq2Poll(self._dq2Timeout)
            if job.dq2Process == None:
              self._dq2Count -= 1

          # If failed, abandon this job, and retry...
          if job.dq2NumFails >= self._dq2RetryLimit and not job.flaggedForDeletion:
            self._print('flagging {0} for deletion.'.format(job.outputDataset))
            if job.dq2Process != None:
              self._dq2Count -= 1
            job.dq2Cleanse()
            newjob = _Job(job.command, job.dq2OutputDir, job.dq2Pattern, job.resubmitLevel+1, job.gridSetupScript, job.dq2SetupScript)
            self._jobs.append(newjob)
            job.flaggedForDeletion = True
            continue

          # If we're not running...
          if not job.dq2Completed and job.dq2Process == None and not job.flaggedForDeletion and self._dq2Count < self._dq2MaxJobs:
            job.dq2Submit()
            self._dq2Count += 1

      import time
      time.sleep(self._delay)

    # Output summary
    self._printSummary()

  ##### INTERNAL FUNCTIONS #####

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Sledgehammer:',msg

  def _dq2StillGoing(self):
    '''Returns false if all jobs listed as completed. Take into account jobs that
    have been retried the maximum number of times'''
    for job in self._jobs:
      # (not completed) and (should have completed)
      # (should've completed) == (not abandoned) or (is abandoned and should download abandoned)
      if not job.dq2Completed and (not job.abandoned or (job.abandoned and self._shouldDownloadAbandoned)):
        return True
    return False

  def _pandaStillGoing(self):
    '''Returns true if the current set of jobs have all finished being processed
    with panda.'''
    for job in self._jobs:
      if not job.pandaCompleted and not job.abandoned:
        return True
    return False

  def _printCurrentStatus(self):
    '''Print a summary of the current jobs that are running, downloading etc'''
    print '-- Sledgehammer Status --'
    import datetime
    print 'Timestamp: {0}'.format(datetime.datetime.now())
    print 'DQ2 job count: {0}'.format(self._dq2Count)
    print 'Current jobs:'
    for job in self._jobs:
      print job
      status = '    '
      if job.pandaRunning:
        status += 'Panda: running, ' 
      if job.pandaStuck:
        status += 'Panda: stuck, '
      if job.pandaCompleted:
        status += 'Panda: completed, '
      if job.abandoned:
        status += 'Panda: abandoned, '

      if job.dq2Process != None:
        import time
        status += 'DQ2: downloading, Runtime: {0}s, '.format(time.time()-job.dq2StartTime)
      if job.dq2Completed:
        status += 'DQ2: completed, '
      status += 'Panda fails: {0}, DQ2 fails: {1}'.format(job.pandaNumFails, job.dq2NumFails)
      print status
    print '--------------------------\n'

  def _printSummary(self):
    '''Print a summary that we'd typically want to show at the end of the run'''
    print '\n\n== SLEDGEHAMMER SUMMARY =='
    print '==========================\n'
    for job in self._jobs:
      status = 'Completed'
      if job.abandoned:
        status = 'Abandoned'
      print '{0}: {1} resubmits, {2}'.format(status, job.resubmitLevel, job.outputDataset)


class _Job(object):
  '''Internal class to represent a job'''
  def __init__(self, command, outputDir, pattern, resubmitLevel, gridSetupScript, dq2SetupScript):
    '''Initialise a new job object'''
    # Use the --disableAutoRetry option to prevent the servers screwing us over
    if command.count('--disableAutoRetry') == 0:
      command += ' --disableAutoRetry'

    # Take into account resubmit level...
    outputDataset = self._figureDataset(command)
    if resubmitLevel > 0:
      outputDataset = outputDataset.strip('/')
      oldDataset = outputDataset
      if resubmitLevel > 1:
        # Strip off any previous appendix
        import re
        match = re.search('(\S+)(_X\d+)', outputDataset)
        if match != None:
          outputDataset = match.group(1)
      outputDataset += '_X{0}'.format(resubmitLevel)
      command = command.replace(oldDataset, outputDataset)
      outputDataset += '/'

    # Common info
    self.flaggedForDeletion = False
    self.abandoned = False
    self.command = command
    self.outputDataset = outputDataset
    # How many times this has failed to download and been resubmitted to Panda
    self.resubmitLevel = resubmitLevel 

    # Panda info
    self.pandaStartDateTime = None
    self.pandaNumPreRun = 0
    self.pandaNumFails = 0
    self.pandaCompleted = False
    self.pandaRunning = False
    self.pandaStuck = False
    self.pandaJobsetID = 0
    self.pandaIDs = None
    self.gridSetupScript = gridSetupScript
    
    # DQ2 info
    self.dq2OutputDir = outputDir
    self.dq2Pattern = pattern # '' => no pattern
    self.dq2Completed = False
    self.dq2NumFails = 0
    self.dq2SetupScript = dq2SetupScript

    self.dq2Process = None # Store the Popen object
    self.dq2StartTime = 0.0
  
  def __str__(self):
    return self.outputDataset

  def pandaUpdate(self, pandasummary, retryLimit):
    '''Given the panda summary from pandaParser, update this job's
    its status flags'''
    pjobs = pandasummary.byOutDS(self.outputDataset)
    # print 'Sledgehammer: found',len(pjobs),'panda entries for',self.outputDataset
    running = False
    completed = False
    stuck = False
    numFails = 0
    for pjob in pjobs:
      if not pjob.isFinished():
        #print 'Sledgehammer: dataset running in panda;',self.outputDataset
        running = True
        self.pandaJobsetID = pjob.jobID
        self.pandaNumPreRun = pjob.prerun 
        self.pandaStartDateTime = pjob.startDateTime()
        self.pandaIDs = pjob.pandaIDs

      # FIXME: Breaks if more than one jobset running simultaneously (e.g.
      # if site decides to auto-resubmit itself
      elif pjob.isFinished():
        if pjob.isFinishedSuccessfully():
          #print 'Sledgehammer: dataset completed in panda;',self.outputDataset
          completed = True
        else:
          # This is a failure...
          numFails += 1
      if pjob.isStuck():
        stuck = True

    # Take into account the fact that if jobs are still running,
    # cannot be completed
    if running:
      completed = False

    # Update the job container
    self.pandaCompleted = completed
    self.pandaRunning = running
    self.pandaStuck = stuck
    self.pandaNumFails = numFails

    # Should this dataset be abandoned completely??
    if numFails >= retryLimit:
      self._print('{0} has failed running {1} times, abandoning'.format(self.outputDataset, self.pandaNumFails))
      self.abandoned = True

  def pandaSubmit(self):
    '''Submit this job to panda. Blocks until done'''
    command = '. {0}; {1}'.format(self.gridSetupScript, self.command)
    import subprocess
    self._print('Submitting {0}'.format(self.outputDataset))
    self._print('Executing: {0}'.format(self.command))
    subprocess.call(command, shell=True)

  def dq2Submit(self):
    '''Submit the job, setting up DQ2 in the process. The process will run in
    the background, and the process object is stored'''
    if self.dq2Process != None:
      self._print('WARNING, DQ2 process already exists! Aborting...')
      return

    # Create directory if it doesn't already exit
    import os
    if not os.path.isdir(self.dq2OutputDir):
      os.makedirs(self.dq2OutputDir)

    command = '. {0}; dq2-get '.format(self.dq2SetupScript)
    if self.dq2Pattern != '':
      command += '-f "{0}" '.format(self.dq2Pattern)
    command += self.outputDataset 

    import subprocess, os
    self._print('downloading {0}'.format(self.outputDataset))
    self._print('executing {0}'.format(command))
    process = subprocess.Popen(command, cwd=self.dq2OutputDir, \
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, \
        preexec_fn=os.setsid)
    self.dq2Process = process
    import time
    self.dq2StartTime = time.time()

  def dq2Poll(self, timeout):
    '''Poll the dq2 process, presuming one exists'''
    # If there isn't a download running, don't do anything
    if self.dq2Process == None:
      return

    import time
    returncode = self.dq2Process.poll()
    if returncode == None:
      if time.time() - self.dq2StartTime > timeout:
        # Kill the job, and chalk up as error
        self._print('download timed out ({0}s)'.format(timeout))
        self._print('killing process...')
        self.dq2NumFails += 1
        self._dq2Terminate()
        self._print('download failed, {0} failures for {1}'.format(self.dq2NumFails, self.outputDataset))
      return

    # If we're here, the process finished!! Parse output, then reset to None...
    output = self.dq2Process.communicate()[0]
    #print output
    if self._successfulDQ2Output(output):
      self._print('download succeeded! {0}'.format(self.outputDataset))
      self.dq2Completed = True
    else:
      self.dq2NumFails += 1
      self._print('download failed, {0} failures for {1}'.format(self.dq2NumFails, self.outputDataset))
      #print output

    self.dq2Process = None
    self.dq2StartTime = 0.0

  def dq2Cleanse(self):
    '''Terminate process if running. Purge the output directory of any remnants 
    of the download of this dataset'''
    self._dq2Terminate()
    import os, shutil
    if not os.path.isdir(self.dq2OutputDir):
      return
    raw = os.listdir(self.dq2OutputDir)
    for line in raw:
      if line.count(self.outputDataset.strip('/')+'.') > 0:
        self._print('deleting {0}'.format(os.path.join(self.dq2OutputDir, line)))
        shutil.rmtree(os.path.join(self.dq2OutputDir, line))

  ##### INTERNAL FUNCTIONS #####

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Sledgehammer:',msg

  def _figureDataset(self, command):
    '''Figure out the dataset name given a command'''
    import re
    # TODO do we need to add more allowed characters into dataset name?
    outputDataset = re.search('--outDS[\'"=\s]*([-_./\w]+)', command).group(1)
    # Ensure it ends with a forward-slash
    if outputDataset[-1] != '/':
      outputDataset += '/'
    return outputDataset

  def _successfulDQ2Output(self, output):
    '''Determine whether the output is indicative of a failure or not.
    Returns True if the output was unequivocally a success.'''
    # # Test 1: Square brackets => it's failed... (not strictly true, but they're annoying)
    # # Remember to get rid of dq2 initialisation, which contains square brackets...
    # import re
    # moddedoutput = re.sub(r'Setting up dq2[\s\S]+Your proxy is valid until','',output)
    # moddedoutput = moddedoutput.replace('[/DC=ch/DC=cern/OU=computers/CN=lcg-voms.cern.ch]','')
    # if moddedoutput.count('[') > 0 or moddedoutput.count (']') > 0:
    #   print moddedoutput
    #   self._print('Failed Test #1')
    #   return False
    
    # Test 2: Look for the word 'FAILED'
    if output.count('FAILED') > 0:
      self._print('Failed Test #2')
      return False

    # Let's hope it was all fine ;-)
    return True

  def _dq2Terminate(self):
    '''Terminate a running dq2 process, if existent'''
    import os, signal
    if self.dq2Process != None:
      os.killpg(self.dq2Process.pid, signal.SIGTERM)
      #self.dq2Process.kill()
    self.dq2Process = None
    self.dq2StartTime = 0.0

