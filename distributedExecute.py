# Very light-weight parallel processing library. Give a set of commands, a
# directory to run in, and this will run all of them, running as many in
# parallel as possible.
# This is the version that executes over multiple machines :-)

class RemoteCommand(object):
  def __init__(self, command):
    self.command = command
    self.remoteHost = None


class Node(object):
  def __init__(self, host, maxThreads, configDir, quiet):
    self.host = host
    self.maxThreads = maxThreads
    self.configDir = configDir
    self.quiet = quiet
    import os.path
    self.commandsName = os.path.join(configDir, 'commands.'+host)
    self.active = False

    self.startRemotePoller()
    self._numSubmittedCommands = 0

  def _cursorCallback(self, callback):
    import sqlite3
    conn = sqlite3.connect(self.commandsName, timeout=60)
    cursor = conn.cursor()
    ret = callback(cursor)
    conn.commit()
    conn.close()
    return ret

  def _execSQL(self, sql):
    def runSQL(cursor):
      cursor.execute(sql)
    self._cursorCallback(runSQL)

  def initialiseCommunicationChannels(self):
    self._execSQL('CREATE TABLE commands (id INTEGER PRIMARY KEY AUTOINCREMENT, command TEXT, status INTEGER, exitCode INTEGER)')
  
  def finaliseCommunicationChannels(self):
    pass

  def startRemotePoller(self):
    self.initialiseCommunicationChannels()
    import subprocess, os
    command = 'ssh '+self.host+' \'. ~gillam/scripts/python_setup.sh; ~gillam/PythonModules/gillam/commandPoller.py '+self.configDir+'\''
    self._process = subprocess.Popen(command, shell=True, preexec_fn=os.setsid)

    self.active = True

  def ensureActive(self):
    returncode = self._process.poll()
    if returncode is not None:
      print 'DistributedExecute ('+self.host+'): Node failure'
      self.active = False
    else:
      if not self.quiet:
        print 'DistributedExecute ('+self.host+'): Node activated!'

  def stopRemotePoller(self):
    if not self.active:
      return
    import os, signal
    self.submitCommands([RemoteCommand('KILL_NOW')])
    os.killpg(self._process.pid, signal.SIGKILL)
    del self._process
    self._process = None
    self.finaliseCommunicationChannels()
    if not self.quiet:
      print 'DistributedExecute ('+self.host+'): Node stopped!'

  def submitCommands(self, commands):
    commandList = []
    for command in commands:
      commandList.append((command.command, 0))

    def submitCallback(cursor):
      cursor.executemany('INSERT INTO commands (id, command, status) VALUES (NULL, ?, ?)', commandList)
    self._cursorCallback(submitCallback)
    self._numSubmittedCommands += len(commands)

  def numFreeSlots(self):
    return self.maxThreads - self.numRunningJobs()

  def numRunningJobs(self):
    def countRunningCallback(cursor):
      for row in cursor.execute('SELECT COUNT(id) FROM commands WHERE status == 0'):
        return row[0]
    return self._cursorCallback(countRunningCallback)

  def unreportedFailedJobs(self):
    def failedCallback(cursor):
      result = []
      for row in cursor.execute('SELECT id FROM commands WHERE status != 0 AND exitCode != 0'):
        result.append(row[0])
      return result
    return self._cursorCallback(failedCallback)

  def commandForID(self, id):
    def commandForIDCallback(cursor):
      for row in cursor.execute('SELECT command FROM commands WHERE id = {0}'.format(id)):
        return row[0]
    return self._cursorCallback(commandForIDCallback)

  def clearFailedJobs(self, ids):
    def clearFailedJobCallbackGenerator(id):
      def callback(cursor):
        cursor.execute('UPDATE commands SET exitCode = 0 WHERE id = {0}'.format(id))
      return callback
    for id in ids:
      self._cursorCallback(clearFailedJobCallbackGenerator(id))
  
  def cleanupDatabase(self):
    def cleanupCallback(cursor):
      query = 'DELETE FROM commands WHERE status != 0'
      cursor.execute(query)
    return self._cursorCallback(cleanupCallback)


class DistributedExecute(object):
  # TODO Change me, but tell Thibaut
  def __init__(self, commandList, defconLevel=3, safeMode=True, quiet=False):
    self._defconLevel = defconLevel
    self._safeMode = safeMode
    self._registerCleanup()
    self._generateConfigDir()
    self._generateCommandList(commandList)
    self._waitingTime = 0.5
    self._failedJobIDs = {}
    self._quiet = quiet

  def _startup(self):
    if not self._quiet:
      if self._defconLevel == 5:
        self._printNukeLoadingScreen()
      elif self._defconLevel > 2 :
        self._printKrakenLoadingScreen()
      else:
        self._printKrakenLoadingScreen()
    self._generateNodeList()

  def _registerCleanup(self):
    import signal, sys
    def signalHandler(signal, frame):
      print 'DistributedExecute: Aborting!'
      self._stopNodes()
      sys.exit(0)
    signal.signal(signal.SIGINT, signalHandler)

  def _generateConfigDir(self):
    import time
    configPath = '~/.distributedExecute/'+str(time.time())
    import os
    self._configDir= os.path.expanduser(configPath)
    os.makedirs(self._configDir)

  def _printKrakenLoadingScreen(self):
    print \
r'''

                  ___ _  _ ____    _  _ ____ ____ _  _ ____ _  _ 
                   |  |__| |___    |_/  |__/ |__| |_/  |___ |\ | 
                   |  |  | |___    | \_ |  \ |  | | \_ |___ | \| 
                                                                   
                                                                                
                                                                                
                 .D     Z,                                                      
                 .D.$    O                                                      
            D~ . 8D8OOZ .$77                                                    
            O=$? Z7I$..$?8~ +       NDMM,                                       
            $:,  :ON. ~ +O8. .=         DD                                      
            7MI:887?Z+O~+$ZN7: ~~       $D                                      
         +$ $O  +O$$7?=+Z~=,,Z~  ~+     $7                                      
         ?$M88DODN= +8+?DZNZIIN~.. ==   DM                                      
          NMM8M.O $.,= I7~==:?ON7I.~ D7,M                                       
         NNMMDN+O.NDND$8=Z8DDDN~,8,=   M                                        
 =....... +MMNNDNMNNNNNNNNNNDDNN.. I$+N.   ...   .....$:,::~~~==++??II77$$ZZODMD
. .......:N8MMMNNNNNNNNNNDNNNNNN.Z..NMM ..~DIO ...,N ?:,D MDOONM................
..........,:  ::: .::::::::..:::. ..ZM$ DD= ..... I, ..~DZ=~?==8M...............
...........     ::,.::::.~:::.. .. ~MNDD,I?.  ...N 7. M+8:DM.,??$......,$MZM8N..
..............  .,::::::::.~........M,:.I.MZDI+D+,..=NID,O$  NI8O... MI8M$$ZZOD8
.................................... = M.MM?D7~,.. M NN.IM .N+ND?..:IM$NMM??  8M
...................................I.M~M MN?... ..~8M7,?N .DIOMN. DMIDM.........
...................................M+MI=... ND8Z~8MM.++D  O:7D .MI$M8. .  ..   .
............. ... ................ DOD8MND,OI8.. $:ODZ8O77:?MMINDD7O............
...................................M.ODNN8,.ID ..~~Z$Z=I?:?MO+=I$I..............
........................ , ...... .OMO:8N7OZ.MN ...,,~$O,$N:........ .. ........
...................... M~:+,...IID~++=: M.ZNZ,7$8ZZOOZM$D$7ZM .... DM$8MN ......
.....................~M$.. O,D:N$. ....$O8M?7?,+=:=??$O:$MZ?OMM...8OMO$$NM......
......... .......... MMO....Z$I~.,....,M$NI$DM,ZI:  .=++8D8O=NND.?MI...DOM .....
....................,D+.....M+8M,,,,?MDDDOIZONZDMM,O8M+7D8M7Z78M8,IDM.788M .... 
......   ...........+DO .... D:NOMDN7+Z::IO+~7+OZ8NNNM7=O8DDMOODDI$MMMONMD......
...... .............=DZ ....  ~ZO$Z+:IZDNN8ODM$~:+78MNM++NDMZO$8NMM NDNMOD8 ....
...  ............... OOO........ . ...MMDMM+OM .$I~?$7MM?8OMD$ODMMMMZM7$MMM.....
.....................$DZ.............$N88 D?Z8~..N8=?MNNMNZDMZ$NMMNOMMMMM MMMMMM
..................... 8ZD+..........MMN8..,D$NNDM8MD77O8MMMDMNMNNMN8MMMMMMM 8NMM
...................... IZM?. ... +NDNN+  :DMMMM$MDMMMM7ONMMNNMMDNMMMM =MM .MMMMM
........ ............... MI?M?7DMM$MI ..MZ87?8OMMMNMMNMMMMMMNMNNZMMMMM .  .MMMMM

                            _ _ _ ____ _  _ ____ ____ 
                            | | | |__| |_/  |___ [__  
                            |_|_| |  | | \_ |___ ___] 
                                                     
'''


  def _printNukeLoadingScreen(self):
    print \
r'''
8888888888888888O8O88O88888O88O8O8888OOO888O888OOOO88O8OOOOOOOOOOOO8O8O8888888888888888888888888888888888888888888888888
88888888888888888888OOOOOOOO8O8OO88888O8O88O88O8O8O8OO8OOOOOOOOOOOOO88OO8O8888888O88888888888888888888888888888888888888
88888888888888888OOOO88OOO8888888888O88OOOO8OO8OOOOOOOO8OOOOOOOOOOOOOOOOOOOOO8O8O8OOOO8OOOO888O8888888888888888888888888
888888888888888888888OO888888888O8888OO8OOOO8O8OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO8OOO8OO8OO88888888888888888888888
888888888888888O888O88O8OO888888OOOOOOOOOOOOOOO8OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO888O888888888888888888888
88888888888888888OOO8OOOOO888O88OOOOOOOOOO8OO8OOOOOOOCCoooCoCCCCCOCOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO8O88O88888888888888888
88888888888888OO8OOOOOOOOO8O88O8OOOOOOOOOOOOOO8OCCCoooccccoCococCOOooCOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO8OO888888888888888
88888888888888O88OOO8OOOOO8OOO88OOOOOOOOOOOOOOCooCCCoc:::cccccccooCCoCOCCCOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO8O88888888888888
888888888888888888O8OOOOOO8O88O8888O8OCoocccocc:coo:..:c::::cooo::cc:....::CCOCCCOOOOOOOOOOOOOOOOOOOOOOOO888888888888888
888888888888888888OOOO8OOOOO8O8888CCoc::........ ..c.cocc:..ccoc:.:..........::::::cCOCOOOOOOOOOOOOO8OOOOO888O8888888888
888888888888888O8O8OOOOOOOOOO8O888C::.. .      .::.::cccc::::::ccc:c:::.:.........:::oCOOOOOOOOOOO8OOOO8OOO8O88888888888
888888888888888888OOOOOOOOOO8O8OCo:...        ..:...::::.  . .   .:.:c:c:c:. . .. ...::cCOOOCOOOOOOOOOOOOOOOO88O88888888
888888888888O8OO8OOOOOOOOOOOO8Ooc:..              ...... ....:..   ..::cc:. .       ...:oCCCOCCCOCCOOOOOCOOOOOOO888O8888
888888888888O8O8OOOOOOOOOOOOOOCo:.                .  . . .......:.. ...::::c.:...     ..::cCCCCoCCCCCCCCOCOOOOO8O8888888
8888888888888O8OOOOOOOOOOOOOOo::....      ..... .  . . .  :..............:cc::...      ..::cCCCCCCoCCCCOCOOOOOOO8OO88888
88888888888O8OOOOOOOOOOOOOO8o..:.....:....:::::::::::::...............  ..:::cc.:..  .  ..::oCCCCCCCCCCCOOOOOO88O8888888
88888888888OOOOOOOOOOO8OOO8::.::ccooc:coooooCOCCCCOoCCoccoc:c::...... ... ..:c:cccc........::CCCCCCCCCCOCOOOOOOO8O888888
88888888888O8OO8OOOOO8OOO8o:.:coCC88888888@@@@@@@@8@@@@888OOOOOOOOOOo:c:.:c:coCocco:.  .....:CCCCCCCCCOCCCOOOOO8O8888888
88888888888888888OOO8O8OO8C.::cC@@@@@@@@@@@@@@@@@8@88@@@88OO8888888OCCCCCoCooCCCCooc:...  ...:CCCCCCCCCCCCOOCOOOO8888888
88888888888888O8888O8OO8O8O::.cC8@@@@@@88OOOOOCooOCCooocooc:::coooCCoc:...:::c:::coooo:.  ...:oCCCCCCCCCCCCOOOOOOO888888
8888888888888O8OO8O8OOOOOOO:..ccoCOOOCoccooCccc:::::::::::... . ..  . .  ...::ccc:::coo:. ...coCCCCCCCCCCCCCOOOOOOOO8888
888888888888888O8O8OOOOOOOO:....:c::::....:c::::::::..........          . .... .::c:ccoc:...:oCCOCCCCCCCOCCCCOOOOOOOO8O8
8@888888888888OO8O8OO8OOOO8C... .. ..   . ..:::..............              .        .:::. ..:OOCCOOOOOOOOOOOOOOOOO8O8888
8@888888888O88O8OOOOOOOOOOOCo:.           . ........... . ..                .             .:COCOOOOO8OOOOOOO8O8OOOO8O888
8@8888888888OO8O8OOCOOOOCOOCOC:..          ..... .....  ....              :..:          ...:oOCOOOOOOO8OOOOOOOOO8OO8O888
8@88888888888O8OO8OCOOOCOOCCOOCC:.        ...:....... .   . ...          ..:::..        ..:oCCCCCCCOOCOOOOOOO8O8OO88O888
88888888888O8OOOOOOOOOOOOCCCCOOoc:....::::::::::........ .....           ...:cc.  .  ...oCCoCooCoCoCCCCCCCOCOCOOOO888O88
8888888888OO8O8OOOCOCCOCCCCCOCCc:...coCcc::::::::::.........::..     .......:coc... :ccocoCoCoCoCoCoCoCoCCCCCOCOOO8O8O88
8888888888O8OO8OOOOOCCCCCCCCCCoc:.:co8Cc:::::::::::.... ....::::..  ..::::::cooOoo:.:.cccoCooCoCCoCCCCoCCCCCCCCOOOOO8O88
888888888OO8OOOOOOCCCCCCCCCCCCCooooc@@8o::::::::::.........:::c:::::.:::::ccooo888Ooc:::cooCoCoCCCoCCCCCCCCCCOCCOOOOO888
8888888888OOO8OOCOCCCCCCCCCCCCCCCoc@@@8:::::::::::::..:::::::cco:::::::cc:ccCooO88@OoooooooooCCoCoCoCCCCCCCCCCOCOOOOO8O8
8888888888OOOOOOOCOOCCCCCCCCCCCCCc8@@Cc:c:ccc:::..::::::::::coooooc::::ccccoCoooOO88@CoooooCoooCoCCCCCCCCCCCOCOOOOOOO8O8
88@8888888OOO8COOCCOCCoCCCCCCCCCC:c88OccCCoco::::::c:.:::::cooCCo8@@c:::::cocCOOOOO888CoooCoooCooCoCoCCCCCCCCCOOCOOOO8O8
8888888888O8OOOOCOCCCCCCCCCCCCCCc::o8OcoCOCCcc:.:::::.:::ccooCoocC8@@@OCoocooooCCO8O88OCCCooCCoCooCCCCCCCCCCCOOOOOO88888
88888888888OOOOOOCCOOCCCCCCCCOCooC8@8CooOOCoc::::::co:cccoCCCOCCOOO8@@@@8O8CoooC8OCo88@COCCoCooCoCCCCCCCCCCOCOCOOO8O8888
88888888888OOOOOCOCOCCCCCCCCCCCc8@@88OO8CoCCoc:::::CCcccoCO8@@CCCC888@@@@OC888O8@@OOO88@OCCCoCoCoCCCCCCCCCCCOCCOOOO88888
@88@88888888O8OOOOCOCCOCCCCCCCc8@@8OoO888occcc::::coo.cc888@@@OOocC8@@8@Oo:ooCOCOOCCOOO8@8CCooCoCoCCCCCCCCCCOOCOOOO8O888
@8@888888888888O8888O88O8OOOoc:8@@@oC8O8Oc:c:c::::cc::@@@@@@@888@@8@8o88:ocoCococooCOO8@CoCooCoCCCCCCCOCCOOOOOOO8O888888
888888888888O88OOOOOOOOOOOOOOccO@@88@@8@o:::::....  .8@@@@@@@@@OCoc:cccO@@oc:oCco::::ccO88CoCCooCoCCCCCCCCCOCOOOOO8O8888
88888888O8O8OOOOOOCCCCCOOOOOCcC@@@@@@@::co::.... .  .8888@@@@Ooo:::::o:coo.cOCCOOo::cC88ooCCooCCCCCCCCCCCOOOOOOO8O888888
888@8888888888OOOOCOOOOOOOOOOO@@@@@@o::C8::::.     cCoco8O@@@co:::C:..coc:oCOCC8@CcoOoCCCCCCCCCCCCCCCCOOOOOOOOO888888888
8888888888O8OOOOOOOOOOOCCCCOCCO@@@@@@8COo::::....      .coO@@@@@OcccOc...:::.:oOOooOC:OCCCCCCCCCOCCCCCCCCOOO8O8888888888
8@8888888O88OOOOCCCOCCCCCCCCCCCoc@@@@coc..... ...        ::cccC8@Co8Cc:::...CO:CCOCCOOoooCoCCCCCCOOOOOOOOCCOOOO88O888888
888888888OOO8OOOOCOCOCCCCCCCCCCcco888O:....... ..       ..::  .....::.     .c8CcoCCOcooooooCCoCCCCCOOOOOOCOOOOOOO8888888
88888888OOOOOOOOOCOCCOCCCCCoCCoooocc::::...:....        ..::  . . .::. . ......::::cocoooooooCCCCCOCOOOCOCOOOOOOO8OCC888
88888OOCoooCooooCCOCCCCCooCCooooooccc::::::...:.         ..:     ..::........::::c:ccoccoooooCooCoCCCCCCCCCCOCo::::cooC8
88OCoCooCoooCoocoooccooooooooCocococcc:::........          .      ..:........:::::ccococcooooooCCo::cooCCCCCo:c:..::cCO8
:::..:ccc..::co:ooooooo::::....... .::.     .....          .      ..:.......:.:::c:ccccoococoooocc:..:coCCCooo::::::::Oo
:::::......:::::::::............ . .           .           .     .::::........::::ccccc:ccoooooc::.  .:oCCCCoCo::.:.:::c
c:::::::::::cc:::::::::::....... . .           .          .      .cc.:........:::::cccc:......:...   ..:oooCCo:::.:::c::
:ooc:::.c:oc::::::::c:c::::::.... . .                     .      :coc................:... .              .:C::..........
c:c::cc:::::::cc::::..:..:......... . .  .                .     .cc.:o        .  ......                         .  .. ..
ooc:::::::c:c:::c::.::..:....... .....  .    .            .     ::cc.o.             ...                           .. .. 
oooo:::::::::::c::.:................  .. .               :.     .::c:o:              .                            .  ...
::::.:.:.:::::.............. ...  .                            .:. co.c:              . . .                 o:cC:.   ...
::.::.:........       .....                                .        c::c.              .:...                :8@C.::8@8C:
::::..:::::.................   .   .                             ..oc..:c.     :. ....:ccc..      .      ..O88@8888@Oo..
oOOcOOoOo.:............. . .:O8:... .. .ooO                   .     .c..::.     :..:cc .co.:oC888O8oCocooO8COCC:.:CCC...
...:8o.cc8.::.......  ...  .:.:@c         .CO:                ..    ccc..ooc   .... ...::c:..oC888O88OCOOO888@@@@OOCo...
..:Oc ...:8:c.ccc........   ..Cc            88C                     cc.:: :o    ....c@oc....::.cccOcooooooCO8@@@O:oC88@o
........o:o:COOcc::.... ...               ..OOc                     ....ccc:co:..:o.o..c::....::.oOo:O8o:.:OC:@@8.:oO8O8
.CcCOCc888C. .              . .cO.oO         :c cOcc.  .C@o:O.oo8o:...O8o:  ... . . .. .o:c.  ...:O@ocC::o8
.             :: .. .   .                   :O.o8: .O8O: .c. .cco:oO .c .   .o::::OO.COO      ..C.   ooc ::....C@@C  ..:
.oco:.o:co:..O:o..o8c:.             . CoO.oc       .cOOOCO.c:.Oo@ooC8@o    ..
:cc          .CO:.                    :o       . .COCcc:co   ...C.              Oc .:8O.C.      ...C   .c8C.cO8O8C::...:
.                            :..:::.C .:   .cooO:oCO.Co .  .:coooOc c: .. .  cO:oC. .:...OOo      .    .c:c:c.:.. o88Coo
.  ...     .           .         .. : o        :.c:8   :.      .c:oCo        cc..   ..o   ooO       .CCc.c   ...:..Oc:O@
@@@@@8oooco::::c:::c.c.   .: :   .    . .   :: c..CCO:.        ccCOoCo88cc:c:oOOooO:.CO:.: :OC .Oo: .o.Cc:     ..ooC::CO
@@@@@@@@@@@@8@8o.                               .ooO88888O88888O888888888888O8O8O88OOO8O8888888O8O8888888@8@888@88@8@@@@
@8@@8Oo:                                                     ...   .                                                    
'''


  def _generateNodeList(self):
    incrementalDefconCases = {
        0: {'pckg': 8},
        1: {'cl011': 24, 'cl012': 24},
        2: {'cl013': 24, 'cl014': 24},
        3: {'cl001': 8, 'cl002': 8, 'cl003': 8, 'cl004': 8, 'cl005': 8,
            'cl015': 16, 'cl016': 16, 'cl017': 16, 'cl018': 16, 
            },
        4: {'pckg': 8, 'pckh': 8, 'pcil': 2,
            'pcka': 1,
            'pcke': 2,
            'pckf': 3,
            'pcki': 8,
            'pckj': 8,
            'pckl': 3,
            'pckp': 1,
            'pckq': 2,
            'pcla': 8,
            'pclb': 8,
            'pclc': 8,
            'pckv': 8,
            'pckw': 3,
            'pckx': 3,
            'pcky': 8,
            'pckz': 8,
            },
        5: {
          'pcht': 4,
          'pcil': 4,
          'pcgs': 4,
          'pcgw': 4,
          'pcgx': 4,
          'pche': 4,
          'pchf': 4,
          'pchg': 4,
          'pchh': 4,
          'pchi': 4,
          'pchr': 4,
          'pchv': 4,
          'pchw': 4,
          'pchx': 4,
          'pchy': 4,
          'pchz': 4,
          'pcia': 4,
          'pcie': 4,
          'pcif': 4,
          'pcih': 4,
          'pcii': 4,
          'pcij': 4,
          'pcik': 4, # DEAD?
          'pcim': 4,
          'pciy': 4,
          'pciz': 4,
          'pcjb': 4,
          'pcjc': 4,
          'pcjd': 4,
          'pcjg': 4,
          'pcjj': 4,
          'pcjn': 4,
          'pcjo': 4,
          'pcjp': 4},
        }
    # 'pcjm': 16, 
    if self._safeMode:
      for case in incrementalDefconCases:
        for machine in incrementalDefconCases[case]:
          incrementalDefconCases[case][machine] = incrementalDefconCases[case][machine] / 4

    defconCases = {}
    defconCases[-1] = {'pcjm': 16}
    defconCases[0] = incrementalDefconCases[0]
    defconCases[1] = incrementalDefconCases[1]
    defconCases[2] = dict(incrementalDefconCases[1].items() + incrementalDefconCases[2].items())
    defconCases[3] = dict(incrementalDefconCases[1].items() + incrementalDefconCases[2].items() + incrementalDefconCases[3].items())
    defconCases[4] = dict(incrementalDefconCases[1].items() + incrementalDefconCases[2].items() + incrementalDefconCases[3].items() + incrementalDefconCases[4].items())
    defconCases[5] = dict(incrementalDefconCases[1].items() + incrementalDefconCases[2].items() + incrementalDefconCases[3].items() + incrementalDefconCases[4].items() + incrementalDefconCases[5].items())


    computers = defconCases[self._defconLevel]
    self._nodeList = []
    for node in computers:
      self._nodeList.append(Node(node, computers[node], self._configDir, self._quiet))
    
    import time
    time.sleep(15)
    for node in self._nodeList:
      node.ensureActive()

  def _generateCommandList(self, commandList):
    self._commandList = []
    for command in commandList:
      self._commandList.append(RemoteCommand(command))

  def runAndWait(self):
    self._startup()
    while(1):
      if self._isFailedJob():
        self._printFailedJobs()
        self._clearFailedJobs()
      elif self._numUnsubmittedCommands() > 0:
        self._submitToFreeNodes()
      elif self._allNodesFinished():
        if not self._quiet:
          self._updateCurrentStatus()
          print
        break
      else:
        if not self._quiet:
          self._updateCurrentStatus()
      import time
      time.sleep(self._waitingTime)
    self._stopNodes()

  def _numSubmittedCommands(self):
    return len(self._commandList) - self._numUnsubmittedCommands()

  def _numUnsubmittedCommands(self):
    return len(list(self._unsubmittedCommands()))

  def _submitToFreeNodes(self):
    for node in self._freeNodes():
      node.cleanupDatabase()
      numFreeSlots = node.numFreeSlots()
      commandsToSubmit = list(self._unsubmittedCommands())[:numFreeSlots]
      node.submitCommands(commandsToSubmit)
      for command in commandsToSubmit:
        if not self._quiet:
          print 'DistributedExecute ('+node.host+'): Spawned process '+str(self._numSubmittedCommands()+1)+'/'+str(len(self._commandList))
        command.remoteHost = node.host

  def _freeNodes(self):
    for node in self._nodeList:
      if not node.active:
        continue
      if node.numFreeSlots() == 0:
        continue
      yield node

  def _unsubmittedCommands(self):
    for command in self._commandList:
      if command.remoteHost == None:
        yield command

  def _isFailedJob(self):
    for node in self._nodeList:
      if not node.active:
        continue
      self._failedJobIDs[node] = node.unreportedFailedJobs()
      if len(self._failedJobIDs[node]) > 0:
        return True
    return False

  def _printFailedJobs(self):
    print '\nERROR! jobs failed :'
    for node in self._failedJobIDs:
      ids = self._failedJobIDs[node]
      for id in ids:
        print '  {0}: {1}'.format(node.host, node.commandForID(id))
    print

  def _clearFailedJobs(self):
    for node in self._nodeList:
      if not node.active:
        continue
      if len(self._failedJobIDs[node]) > 0:
        node.clearFailedJobs(self._failedJobIDs[node])

  def _allNodesFinished(self):
    for node in self._nodeList:
      if not node.active:
        continue
      if node.numRunningJobs() > 0:
        return False
    return True

  def _updateCurrentStatus(self):
    import sys
    sys.stdout.write('\r\033[K'+self._jobsRunningStatus())
    sys.stdout.flush()

  def _numJobsRunning(self):
    numRunning = 0
    for node in self._nodeList:
      if not node.active:
        continue
      numRunning += node.numRunningJobs()
    return numRunning

  def _jobsRunningStatus(self):
    return '  '.join(self._nodeStatuses())

  def _nodeStatuses(self):
    for node in self._nodeList:
      if not node.active:
        continue
      yield node.host+': '+str(node.numRunningJobs())

  def _stopNodes(self):
    for node in self._nodeList:
      node.stopRemotePoller()


# Testing
def test():
  commands = []
  for i in range(10):
    #commands.append('moooo; echo {0}; echo "scale = 1000; sqrt(1.0/3.0)" | bc'.format(i))
    commands.append('moooo{0}'.format(i))
  par = DistributedExecute(commands, 0)
  par.runAndWait()

if __name__ == "__main__":
  test()
