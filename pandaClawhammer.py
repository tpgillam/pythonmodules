# The finer-grained interation of the Sledgehammer. Performs more robust validation
# of downloaded files, and when a download fails will only resubmit those input
# files that aren't accounted for in the output that is present

import pandaSledgehammer

class ValidatedDatasetStorage(object):
  def __init__(self, databaseName="~/.clawhammer/validated.db"):
    import os.path
    self._databaseName = os.path.expanduser(databaseName)
    self._createDBIfNecessary()

  def isDatasetValidated(self, datasetName):
    if not datasetName.endswith('/'):
      datasetName += '/'
    def countValidatedCallback(cursor):
      for row in cursor.execute('SELECT COUNT(dataset) FROM validated WHERE dataset == "{0}"'.format(datasetName)):
        return row[0]
    return (self._cursorCallback(countValidatedCallback) > 0)

  def validationStatus(self, datasetName):
    if not datasetName.endswith('/'):
      datasetName += '/'
    def validationStatusCallback(cursor):
      for row in cursor.execute('SELECT status FROM validated WHERE dataset == "{0}"'.format(datasetName)):
        return row[0]
    return (self._cursorCallback(validationStatusCallback) > 0)

  def recordDatasetAsValidated(self, datasetName, successful):
    if not datasetName.endswith('/'):
      datasetName += '/'
    if not self.isDatasetValidated(datasetName):
      inputTuple = (datasetName, successful)
      def recordCallback(cursor):
        cursor.execute('INSERT INTO validated VALUES (?, ?)', inputTuple)
      self._cursorCallback(recordCallback)
    else:
      def updateCallback(cursor):
        cursor.execute('UPDATE validated SET status={1} WHERE dataset == "{0}"'.format(datasetName, int(successful)))
      self._cursorCallback(updateCallback)

  def _createDBIfNecessary(self):
    import os
    if os.path.isfile(self._databaseName):
      return
    print 'Creating validation database',self._databaseName
    parentDir = os.path.dirname(self._databaseName)
    if not os.path.exists(parentDir):
      os.makedirs(parentDir)
    self._execSQL('CREATE TABLE validated (dataset TEXT, status INTEGER)')

  def _cursorCallback(self, callback):
    import sqlite3
    conn = sqlite3.connect(self._databaseName)
    cursor = conn.cursor()
    ret = callback(cursor)
    conn.commit()
    conn.close()
    return ret

  def _execSQL(self, sql):
    def runSQL(cursor):
      cursor.execute(sql)
    self._cursorCallback(runSQL)

validatedDatasetStorage = ValidatedDatasetStorage()

class Clawhammer(pandaSledgehammer.Sledgehammer):
  def __init__(self):
    super(Clawhammer, self).__init__()
    self.numberOfTweetsPerStatus = 0
    self.noDownloading = False
    self.krakeniseSubmission = False
    self.krakenOnceOnly = True
    self.krakenSetupCommand = ''
    from datetime import timedelta
    self.pandaJobTimeout = timedelta(seconds=3*3600)

    import tweeter
    self._tweeter = tweeter.Tweeter()
    self._pandasummary = None
    self._excludedSites = set()
    self._pbookJobKiller = None

  # Overload to use _ClawJob
  def append(self, command, outputDir, pattern=''):
    '''Append a job to the list to run. Call AFTER setting other parameters!!!'''
    self._jobs.append(_ClawJob(command, outputDir, pattern, resubmitLevel=0, gridSetupScript=self._gridSetupScript, dq2SetupScript=self._dq2SetupScript, names = self._names, days = self._days))

  def addExcludedSite(self, site):
    self._excludedSites.add(site)

  def run(self):
    '''Hold on tight!'''
    self._printWelcome()
    self._print('Searching for resubmits...')
    self._getPandaSummary()

    newjobs = []
    for job in self._jobs:
      # Update panda for each job
      job.pandaUpdate(self._pandasummary, self._pandaRetryLimit)
      foundExtraDataset = self._findXStyleDatasetsForJob(job, newjobs)

      # Attempt to validate current download
      if not foundExtraDataset:
        import os
        if not os.path.isdir(job.dq2OutputDir):
          continue
        foundThisJob = False
        testName = job.outputDataset.strip('/')+'.' 
        for dir in os.listdir(job.dq2OutputDir):
          if testName in dir:
            foundThisJob = True
        if not foundThisJob:
          continue

        job.validate(self._pandasummary)
        # If the job hasn't successfully validated, undo the validation...
        if (len(job.dq2UnrepresentedInput) != 0 or job.dq2NumPresentOutput == 0) and not job.dq2FastValidateSuccessful:
          self._print('Validation failed - perhaps download unfinished?')
          job.dq2Completed = False
          job.dq2Validated = False
        else:
          self._print('Validated successfully!')
          job.dq2Completed = True
          job.dq2Validated = True
          # If DQ2 finished, then so did Panda...
          job.pandaCompleted = True

    self._jobs += newjobs
    self._print('Finished pre-run prep')
    self._printCurrentStatus()

    self._print('Entering execution loop...')
    if self.noDownloading:
      self._pandaRunLoop()
    else:
      self._pandaRunAndDownloadLoop()

    # Make sure we're up-to-date
    self._getPandaSummary()
    for job in self._jobs:
      job.pandaUpdate(self._pandasummary, self._pandaRetryLimit)

    # Output summary
    self._printSummary()

  ##### INTERNAL FUNCTIONS #####

  def _findXStyleDatasetsForJob(self, job, newjobs):
    # Search for extra datasets
    prevjob = job
    foundExtraDataset = False
    count = 1
    while True:
      testdataset = '{0}_X{1}/'.format(job.outputDataset.strip('/'), count)
      self._print('Looking for {0}'.format(testdataset))

      if len(self._pandasummary.byOutDS(testdataset)) > 0:
        foundExtraDataset = True
        # This means that the _X dataset exists
        self._print('Found {0}'.format(testdataset))

        # 1. Validate the previous prevjob, and set to dq2 failed...
        prevjob.validate(self._pandasummary)
        prevjob.dq2NumFails = self._dq2RetryLimit
        prevjob.flaggedForDeletion = True

        # 2. Add a job for the new dataset
        newjob = _ClawJob(prevjob.command, prevjob.dq2OutputDir, prevjob.dq2Pattern, prevjob.resubmitLevel+1, prevjob.gridSetupScript, prevjob.dq2SetupScript, self._names, self._days, inputFiles = prevjob.dq2UnrepresentedInput)
        newjob.pandaUpdate(self._pandasummary, self._pandaRetryLimit)

        newjobs.append(newjob)
        prevjob = newjob
      else:
        break
      # Increment the counter...
      count += 1
    return foundExtraDataset

  def _pandaRunLoop(self):
    numUnfinishedJobs = len(self._jobs)
    while numUnfinishedJobs > 0:
      # Jobs that should be processed at the end of the loop
      self._jobsToKill = []
      self._jobsToSubmit = []

      self._getPandaSummary()
      numUnfinishedJobs = 0
      for job in self._unfinishedJobs():
        self._processUnfinishedJob(job)
        numUnfinishedJobs += 1
      self._submitWaitingJobs()
      self._killJobsInPanda(self._jobsToKill)

  def _unfinishedJobs(self):
    for job in self._jobs():
      if not job.abandoned and not job.pandaCompleted:
        yield job

  def _processUnfinishedJob(self, job):
    job.pandaUpdate(self._pandasummary, self._pandaRetryLimit)
    # If not running and not stuck, resubmit
    from datetime import datetime
    if not (job.pandaCompleted or job.abandoned or job.pandaRunning or job.pandaStuck):
      if self.krakeniseSubmission:
        self._jobsToSubmit.append(job)
      else:
        job.pandaSubmit(self._excludedSites)
    elif job.pandaStuck:
      self._print('{0} is stuck (maybe transferring?).'.format(job.outputDataset))
    elif job.pandaNumPreRun > 0:
      if (datetime.utcnow() - job.pandaStartDateTime) > self.pandaJobTimeout:
        self._jobsToKill.append(job)

  def _submitWaitingJobs(self):
    if not self.krakeniseSubmission:
      return
    commands = []
    for job in self._jobsToSubmit:
      command = '{0}; '.format(self.krakenSetupCommand)+job.pandaSubmit(self._excludedSites, justReturnCommand=True)
      commands.append(command)
      print command
    from distributedExecute import DistributedExecute
    dist = DistributedExecute(commands, 2)
    dist.runAndWait()
    if self.krakenOnceOnly:
      self.krakeniseSubmission = False

  def _pandaRunAndDownloadLoop(self):
    while self._dq2StillGoing():
      self._getPandaSummary()

      # Jobs that should be processed at the end of the loop
      self._jobsToKill = []
      self._jobsToSubmit = []

      import time
      startLoopTime = time.time()
      skipDelay = False
      for job in self._jobs:
        if (time.time() - startLoopTime) > 15*60:
          skipDelay = True
          break

        # A little hacky, for performance reasons... Update job stats from panda
        # but only if it isn't completed/abandoned
        if not job.abandoned and not job.pandaCompleted:
          self._processUnfinishedJob(job)
        elif not job.abandoned or (job.abandoned and self._shouldDownloadAbandoned):
          # Now we should look at DQ2 stuff...

          # If already running, poll
          if job.dq2Process != None:
            job.dq2Poll(self._dq2Timeout)
            if job.dq2Process == None:
              self._dq2Count -= 1

          # If failed, abandon this job, and retry...
          if job.dq2NumFails >= self._dq2RetryLimit and not job.flaggedForDeletion:
            self._print('abandoning download for {0}'.format(job.outputDataset))
            if job.dq2Process != None:
              self._dq2Count -= 1
              job._dq2Terminate()

            # Validate the job to get unrepresented input files...
            self._print('Validating on fail, {0}'.format(job.outputDataset))
            self._validateCleanAndGo(job)
            job.flaggedForDeletion = True
            continue

          # If we've completed (successfully?!) but haven't validated...
          if job.dq2Completed and not job.dq2Validated:
            self._print('Validating on succeed, {0}'.format(job.outputDataset))
            self._validateCleanAndGo(job)

          # If we're not running...
          if not job.dq2Completed and job.dq2Process == None and not job.flaggedForDeletion and self._dq2Count < self._dq2MaxJobs:
            job.dq2Submit()
            self._dq2Count += 1

      self._submitWaitingJobs()
      self._killJobsInPanda(self._jobsToKill)

      # Print status summary after each pass
      self._printCurrentStatus()

      if skipDelay:
        skipDelay = False
      else:
        time.sleep(self._delay)

  def _getPandaSummary(self):
    self._print('parsing Panda')
    import pandaJsonParser
    self._pandasummary = pandaJsonParser.getJobsetContainer(self._names, self._days)
    #import pandaParser
    #self._pandasummary = pandaParser.getJobsetContainer(self._names, self._days)

  def _killJobsInPanda(self, jobs):
    self._fastKillJobsInPanda(jobs)
    return
    if self._pbookJobKiller is not None:
      self._pbookJobKiller.poll()
      if self._pbookJobKiller.returncode is None:
        self._print('PBook Job killer already running...')    
        return
      else:
        self._print('PBook Job killer finished!')    
        self._pbookJobKiller = None
    if len(jobs) == 0:
      return
    jobsetIds = [job.pandaJobsetID for job in jobs]
    command = '. {0}; pbook -c "sync()"; '.format(self._gridSetupScript)
    for jobsetId in jobsetIds:
      command +='pbook -c "kill({0})"; '.format(jobsetId)
    import subprocess
    #self._pbookJobKiller = subprocess.Popen(command, shell=True)
    #TODO see if the parallel method can be made more reliable...
    subprocess.call(command, shell=True)

  def _fastKillJobsInPanda(self, jobs):
    for job in jobs:
      if job.pandaIDs is not None:
        from pandatools import Client
        Client.killJobs(job.pandaIDs, verbose=True)

  def _killJobInPanda(self, job):
    command = '. {0}; pbook -c "sync()"; pbook -c "kill({1})"'.format(self._gridSetupScript, job.pandaJobsetID)
    import subprocess
    subprocess.call(command, shell=True)
    #jobsToKill = self._getJobIDsWithSetID(job.pandaJobsetID)
    #TODO
    #from pandatools import Client
    #Client.killJobs(jobsToKill, False)

  # get JobIDs with JobsetID
  #def _getJobIDsWithSetID(self,jobsetID):
  #  idMap = PdbUtils.getMapJobsetIDJobIDs(self.verbose)
  #  if idMap.has_key(jobsetID):
  #      return idMap[jobsetID]
  #  return None


  def _dq2StillGoing(self):
    '''Returns false if all jobs listed as completed. Take into account jobs that
    have been retried the maximum number of times'''
    import re

    # Dictionary of datasets -> (still going, iteration)
    stillGoingDict = {}
    for job in self._jobs:
      match = re.search('(\S+)_X(\d+)/', job.outputDataset)
      dataset = ''
      index = 0
      if match != None:
        dataset = match.group(1)+'/'
        index = int(match.group(2))
      else:
        dataset = job.outputDataset
        index = 0
      # Insert into dict if *either* key doesn't exist or current key is greater
      # than stored key
      stillGoing = False
      # (not completed) and (should have completed)
      # (should've completed) == (not abandoned) or (is abandoned and should download abandoned)
      # (completed) == is validated and no missing files
      if (not (job.dq2Validated and len(job.dq2MissingFiles) == 0)) and ((not job.abandoned) or (job.abandoned and self._shouldDownloadAbandoned)):
        stillGoing = True
      if dataset in stillGoingDict and index > stillGoingDict[dataset][1]:
        stillGoingDict[dataset] = (stillGoing, index)
      elif not dataset in stillGoingDict:
        stillGoingDict[dataset] = (stillGoing, index)

    # Return True if any datasets still going, else false
    for key in stillGoingDict:
      if stillGoingDict[key][0]:
        return True
    return False

  def _validateCleanAndGo(self, job):
    '''Validate the given job, and see if it passed. If not, resubmit with the failed input files'''
    job.validate(self._pandasummary)
    
    missingInput = job.dq2UnrepresentedInput

          #TODO
          #self._print('Validation failed - perhaps download unfinished?')
          #job.dq2Completed = False
          #job.dq2Validated = False

    # Test for whether this has failed...
    if len(missingInput) != 0:
      # If we still have repeats left, try again...
      if job.dq2NumFails < self._dq2RetryLimit:
        self._print('Validation failed - retrying')
        job.dq2NumFails += 1
        job.dq2Completed = False
        job.dq2Validated = False
        return

      # Clean out any corrupted files
      if len(job.dq2CorruptedFiles) > 0:
        import os
        for name in job.dq2CorruptedFiles:
          try:
            self._print('Deleting corrupted file {0}'.format(os.path.join(job.dq2OutputDir, name)))
            os.remove(os.path.join(job.dq2OutputDir, name))
          except:
            self._print('Couldn\'t delete {0}'.format(os.path.join(job.dq2OutputDir, name)))

      # Create a new job using only the failed input files
      self._print('{0} missing input files for {1}, resubmitting'.format(len(missingInput), job.outputDataset))
      newjob = _ClawJob(job.command, job.dq2OutputDir, job.dq2Pattern, job.resubmitLevel+1, job.gridSetupScript, job.dq2SetupScript, self._names, self._days, inputFiles = missingInput)
      self._jobs.append(newjob)
    else:
      self._print('Successful validation for {0}'.format(job.outputDataset))

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Clawhammer:',msg

  def _printWelcome(self):
    '''Print welcome message'''
    print '''

     CCCCC  LL        AAA   WW      WW HH   HH   AAA   MM    MM MM    MM EEEEEEE RRRRRR  
    CC    C LL       AAAAA  WW      WW HH   HH  AAAAA  MMM  MMM MMM  MMM EE      RR   RR 
    CC      LL      AA   AA WW   W  WW HHHHHHH AA   AA MM MM MM MM MM MM EEEEE   RRRRRR  
    CC    C LL      AAAAAAA  WW WWW WW HH   HH AAAAAAA MM    MM MM    MM EE      RR  RR  
     CCCCC  LLLLLLL AA   AA   WW   WW  HH   HH AA   AA MM    MM MM    MM EEEEEEE RR   RR 

      ................................................................................
      ................ ~~:=  ,   .....................................................
      ...............:::7D~:::~. .....................................................
      .............~::~::::~~.........................................................
      ...........,~~~:~::~~= .........................................................
      ..........=~~===:~~=............................................................
      .........?+++=+~=++.............................................................
      ........IIIII7==++..............................................................
      .......??7??7=+?:.....................................................Z8888.....
      ......I?7I?I~+?7................................ .:ODD88D8DDDDDDDDDDDDNNNNNN....
      ......I7II7~=+?..........................M888DDDDDDNDNNNNNNNNNNNNNNNDNNMMMMMN...
      .....I77I7==+?IZM........ +I+:,..,,,,::~~MDNDDNNNNNMMMNNNNNNNMDNMNNNMNMNNMNMM...
      .....IIII+?ZI+IZZM++~I$MMMMMMM,,.,,.,,,,:NDDDNNNNNNMNMMNMNMMNMNMMMMMMMMMMMMMM...
      ....?IIII=+?I+7$$$?::~~~+$I7ZN,::..::~~~~NNNNMNNNMNMMMMMMMMMNMMMMMMMMMMMMMMNN ..
      ....IIII?====I$$$$$7OOOMMMMMMM::,:~~OOOONMMMMMMMMMMMMMMMMMNMMMMMMMMMMMMMMMMM ...
      ....7III++7$Z$$O+~~.......................,7DNNNMMNMMN8I,...........,~?7ZZ+.....
      ....$77I===+?$,..................................................,,,,,,,........
      ....D8ZD8==?I...................................................................
      .....NN=II$8 ...................................................................
      .....88~IIO:....................................................................
      .....OO:I?Z.....................................................................
      .....O=,Z+7.....................................................................
      ....ZZ::O=?: ...................................................................
      ...8$Z~~O:=O....................................................................
      ...D8N,~D+?D....................................................................
      ...D8N:?D??Z....................................................................
      ..=7NM77D8Z?....................................................................
      ...:=++??+~.....................................................................
      ................................................................................
      ................................................................................

    '''

  def _printCurrentStatus(self):
    '''Print a summary of the current jobs that are running, downloading etc'''
    print '--- Clawhammer Status ---'
    import datetime
    print 'Timestamp: {0}'.format(datetime.datetime.now())
    print 'DQ2 job count: {0}/{1}'.format(self._dq2Count, self._dq2MaxJobs)
    print 'Current jobs:'
    for job in self._jobs:
      status = '  '
      color = None
      shouldPrintJob = True
      attributes = []
      if job.pandaInputFiles != None:
        status += '({0} files) '.format(len(job.pandaInputFiles))
      if job.pandaRunning:
        status += 'Panda: running, ' 
      if job.pandaStuck:
        status += 'Panda: stuck, '
      if job.pandaCompleted:
        status += 'Panda: completed, '
        color = 'blue'
      if job.abandoned:
        status += 'Panda: abandoned, '
        color = 'red'

      if job.dq2Process != None:
        import time
        status += 'DQ2: downloading, Runtime: {0}s, '.format(time.time()-job.dq2StartTime)
      if job.dq2Completed:
        status += 'DQ2: completed, '
        color = 'cyan'
      elif job.dq2Process == None and job.dq2NumFails >= self._dq2RetryLimit:
        status += 'DQ2: abandoned, '
      if job.dq2Validated:
        status += 'Validation: {0}/{1}, '.format(job.dq2NumPresentOutput, job.dq2NumActualOutput)
        if job.dq2NumPresentOutput == job.dq2NumActualOutput:
          shouldPrintJob = False
          color = 'green'
          if job.abandoned:
            shouldPrintJob = True
            color = 'red'
        else:
          color = 'magenta'
      status += 'Panda fails: {0}, DQ2 fails: {1}'.format(job.pandaNumFails, job.dq2NumFails)
      if shouldPrintJob:
        from termcolor import cprint
        cprint(str(job), color, None, attributes)
        cprint(status, color, None, attributes)
    print '--------------------------'
    for i in range(self.numberOfTweetsPerStatus):
      self._tweeter.printRandomTweet()
    print '--------------------------\n'

  def _printSummary(self):
    '''Print a summary that we'd typically want to show at the end of the run'''
    print '\n\n=== CLAWHAMMER SUMMARY ==='
    print '==========================\n'
    total = 0
    fullyCompleted = 0
    for job in self._jobs:
      status = 'Completed'
      if job.abandoned:
        status = 'Abandoned'
      print '{0}: validated {1}/{2}, {3}'.format(status, job.dq2NumPresentOutput, job.dq2NumActualOutput, job.outputDataset)
      if job.resubmitLevel == 0:
        total += 1
      if (not job.abandoned) and job.dq2NumPresentOutput == job.dq2NumActualOutput:
        fullyCompleted += 1
    print
    print '=================================='
    print '{0}/{1} jobs fully completed'.format(fullyCompleted, total)
    print '=================================='
    print
    if fullyCompleted != total:
      print '=================================='
      print 'Dodgy jobs:'
      for job in self._jobs:
        if not ((not job.abandoned) and job.dq2NumPresentOutput == job.dq2NumActualOutput):
          print job
      print '=================================='
      print

class _ClawJob(pandaSledgehammer._Job):
  '''Internal class to represent a job'''
  def __init__(self, command, outputDir, pattern, resubmitLevel, gridSetupScript, dq2SetupScript, names, days, inputFiles = None):
    super(_ClawJob, self).__init__(command, outputDir, pattern, resubmitLevel, gridSetupScript, dq2SetupScript)

    # Files in input dataset to use when submitting
    # Use all if == ''
    self.pandaInputFiles = inputFiles

    # Days history to use for validator
    self.names = names
    self.days = days

    # Extra DQ2 info re validation
    self.dq2Validated = False
    self.dq2FastValidateSuccessful = False
    self.dq2CorruptedFiles = []
    self.dq2MissingFiles = []
    self.dq2UnrepresentedInput = []
    self.dq2NumPresentOutput = 0
    self.dq2NumActualOutput = 0

  def pandaSubmit(self, excludedSites=set(), justReturnCommand=False):
    '''Submit this job to panda, noting the input file list. Blocks until done'''
    command = '/usera/gillam/scripts/timeout3 -t 900 {0}'.format(self.command)

    tempFileName = ''
    if self.pandaInputFiles != None:
      tempFileName = self._getTempFilename()
      f = open(tempFileName, 'w')
      f.write('\n'.join(self.pandaInputFiles))
      f.close()
      command += ' --inputFileList={0}'.format(tempFileName)

    if len(excludedSites) > 0:
      command += ' --excludedSite='+(','.join(excludedSites))

    self._print('Submitting {0}'.format(self.outputDataset))
    if justReturnCommand:
      return command
    command = '. {0}; {1}'.format(self.gridSetupScript, command)
    self._print('Executing: {0}'.format(command))
    self._runCommandAdInfinitumWithTimeout(command, 15*60)

    import os
    if tempFileName != '':
      os.remove(tempFileName)

  def _runCommandAdInfinitumWithTimeout(self, command, timeout):
    import subprocess, time, os, signal
    done = False
    running = False
    while not done:
      if not running:
        process = subprocess.Popen(command, shell=True, preexec_fn=os.setsid)
        starttime = time.time()
        running = True
      else:
        returncode = process.poll()
        if returncode == None:
          if time.time() - starttime > timeout:
            self._print('submit timed out ({0}s)'.format(timeout))
            self._print('killing process...')
            os.killpg(process.pid, signal.SIGKILL)
            running = False
            continue
          time.sleep(30)
        else:
          done = True

  def validate(self, jobsetContainer):
    '''Perform panda-based validation, and populate validation member variables'''
    if validatedDatasetStorage.isDatasetValidated(self.outputDataset):
      successful = validatedDatasetStorage.validationStatus(self.outputDataset)
      if successful:
        self._print('Fast validation of {0} successful!'.format(self.outputDataset))
        self.dq2Validated = True
        self.dq2FastValidateSuccessful = True
        return

    self._print('Validating {0}'.format(self.outputDataset))
    import pandaValidator
    validator = pandaValidator.PandaValidator(self.names, self.days, self.outputDataset, self.dq2OutputDir, jobsetContainer=jobsetContainer)

    self.dq2CorruptedFiles = validator.corruptedOutputFiles()
    self.dq2MissingFiles = validator.missingOutputFiles()
    self.dq2UnrepresentedInput = validator.unrepresentedInputFiles()

    # Counts for status message printing
    self.dq2NumPresentOutput = len(validator.presentOutputFiles())
    self.dq2NumActualOutput = validator.numExpectedOutputFiles()

    self.dq2Validated = True

    validatedSuccessfully = (len(self.dq2UnrepresentedInput) == 0)
    validatedDatasetStorage.recordDatasetAsValidated(self.outputDataset, validatedSuccessfully)

  ##### INTERNAL FUNCTIONS #####

  def _getTempFilename(self):
    '''Get a temporary filename in the present directory'''
    name = 'tmpinnames'
    import os.path
    while (os.path.exists(name)):
      name += '1'
    return name

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Clawhammer:',msg

