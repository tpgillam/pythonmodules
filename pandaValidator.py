# Module to perform validation of files in a given directory against information retrieved from panda
# Subsequently, information about the missing/otherwise files can be retrieved

class PandaValidator(object):
  def __init__(self, names, days, outDS, testDir, jobsetContainer=None):
    '''Initialise with a given output dataset name. Names is passed
    to PandaParser. Validate the given testDir'''
    #import pandaJsonParser
    if jobsetContainer is None:
      #jobsetContainer = pandaJsonParser.getJobsetContainer(names, days)
      import pandaParser
      jobsetContainer = pandaParser.getJobsetContainer(names, days)
    jobsets = jobsetContainer.byOutDS(outDS)

    # Get a list of all successful jobs that would contribute to this outDS
    jobs = []
    for jobset in jobsets:
      notGotJobs = True
      thesejobs = None
      retriesLeft = 20
      # Hack to suppress bug
      while notGotJobs and retriesLeft > 0:
        #try:
          #thesejobs = pandaJsonParser.getJobContainer(names, days, jobset.jobID)
          import pandaJobsetParser
          thesejobs = pandaJobsetParser.getJobContainer(names, jobset.jobID)
          if len(thesejobs) > 0:
            notGotJobs = False
          retriesLeft -= 1
        #except:
        #  print 'PandaValidator: ERROR - could not get jobs container, retrying'

      # We're only interested in successful jobs!
      for job in thesejobs.finished():
        jobs.append(job)

    if len(jobs) == 0:
      print 'PandaValidator: Warning - seem to have no jobs?!'
        
    # Generate: block -> file -> (size, [inputfiles])
    self.jobDict = {}
    for job in jobs:
      # Skip jobs with no known output file
      if job.outputFile is None:
        continue
      if not job.outputBlock in self.jobDict:
        self.jobDict[job.outputBlock] = {}
      self.jobDict[job.outputBlock][job.outputFile] = (job.outputSize, job.inputFiles)

    # actualDir takes into account any _rev123 suffix
    # Generate: block -> file -> (size, actualDir)
    self.dirDict = {}
    for key in self.jobDict:
      self.dirDict[key] = {}
    import os, re
    if not os.path.isdir(testDir):
      return
    for dir in [x for x in os.listdir(testDir) if os.path.isdir(os.path.join(testDir,x))]:
      effDir = '.'.join(dir.split('.')[:-1])+'.'+dir.split('.')[-1].split('_')[0]
      if outDS.strip('/') not in effDir:
        continue

      if not effDir in self.dirDict:
        print 'PandaValidator: Warning, unknown dataset found, omitting',dir
        continue

      for fileName in [x for x in os.listdir(os.path.join(testDir,dir)) if os.path.isfile(os.path.join(os.path.join(testDir,dir),x))]:
        self.dirDict[effDir][fileName] = (os.path.getsize(os.path.join(os.path.join(testDir,dir),fileName)), dir)
   
  def numExpectedOutputFiles(self):
    '''Return the number of output files we *should* have'''
    tot = 0
    for dir in self.jobDict:
      tot += len(self.jobDict[dir])
    return tot

  def presentOutputFiles(self):
    '''Return a list of correctly downloaded output files'''
    ret = []
    for dir in self.jobDict:
      for name in self.jobDict[dir]:
        if name in self.dirDict[dir]:
          if self.jobDict[dir][name][0] == self.dirDict[dir][name][0]:
            import os.path
            ret.append(os.path.join(dir,name))
    return ret      

  def missingOutputFiles(self):
    '''Return a list of the missing output files'''
    ret = []
    for dir in self.jobDict:
      for name in self.jobDict[dir]:
        if not name in self.dirDict[dir]:
          import os.path
          ret.append(os.path.join(dir,name))
    return ret      

  def corruptedOutputFiles(self):
    '''Return a list of the output files that are present, but don't match the panda
    page that denote what their size should be'''
    ret = []
    for dir in self.jobDict:
      for name in self.jobDict[dir]:
        if name in self.dirDict[dir]:
          if self.jobDict[dir][name][0] != self.dirDict[dir][name][0]:
            import os.path
            ret.append(os.path.join(dir,name))
    return ret      

  def unrepresentedInputFiles(self):
    '''Return a list of input files that aren't represented by the (valid) output files
    that are in the download directory'''
    ret = []
    for dir in self.jobDict:
      for name in self.jobDict[dir]:
        if name in self.dirDict[dir]:
          if self.jobDict[dir][name][0] != self.dirDict[dir][name][0]:
            ret += self.jobDict[dir][name][1]
        else:
          ret += self.jobDict[dir][name][1]
    return ret      

  def __str__(self):
    present = self.presentOutputFiles()
    corrupted = self.corruptedOutputFiles()
    missing = self.missingOutputFiles()
    numExpected = self.numExpectedOutputFiles()
    input = self.unrepresentedInputFiles()

    s = 'We have {0}/{1} output files\n'.format(len(present), numExpected)
    s += '\nCorrupted files ({0}):\n'.format(len(corrupted))
    for x in corrupted: s += '  '+x+'\n'
    s += '\nMissing files ({0}):\n'.format(len(missing))
    for x in missing: s += '  '+x+'\n'
    s += '\nCorresponding input files ({0}):\n'.format(len(input))
    for x in input: s += '  '+x+'\n'
    return s


# Test code
def main():
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.group10.phys-susy.data11_7TeV.periodI.physics_JetTauEtmiss.PhysCont.NTUP_SUSYSKIM.pro10_v01_p832.PARITYVARS.35/','/r02/atlas/gillam/parity_output/version_35')
  #validator = PandaValidator(['thibaut','mueller'],3,'user.tmueller.0427.allData.periodL.physics_Muons/','/r02/atlas/thibaut/SledgeDL')
  #validator = PandaValidator(['thibaut','mueller'],10,'user.tmueller.0419.dataAll2011.periodB.physics_Muons/','/r02/atlas/thibaut/SledgeDL')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.group10.phys-susy.data11_7TeV.periodI.physics_JetTauEtmiss.PhysCont.NTUP_SUSYSKIM.pro10_v01_p832.PARITYVARS.37/','/r02/atlas/gillam/parity_output/version_37')
  #validator = PandaValidator(['thibaut','mueller'],3,'user.tmueller.0429.allData.periodB.physics_Muons/','/r02/atlas/thibaut/ClawhammerDL')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.0806RPV-defaultslim.107653.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp3/','/r02/atlas/gillam/rpv/slims-0806RPV/defaultslim/MC/')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.0816RPV-defaultslim.periodB3.physics_Egamma/','/r02/atlas/gillam/rpv/slims-0816RPV/defaultslim/Data/')
  #validator = PandaValidator(['thibaut','mueller'],12,'user.tmueller.1009-defaultslim.periodB.physics_Muons_X1/','/r02/atlas/thibaut/tevg/slims-1009/defaultslim/Data/')
  #print validator
  #validator = PandaValidator(['thibaut','mueller'],12,'user.tmueller.1009-defaultslim.periodB.physics_Muons_X2/','/r02/atlas/thibaut/tevg/slims-1009/defaultslim/Data/')
  #print validator
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.1119RPV-defaultslim.Herwigpp_UEEE3_CTEQ6L1_Gtt_G900_T2500_L1.merge_X1/','/r02/atlas/gillam/rpv/slims-1119RPV/defaultslim/Signal/')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.1119RPV-defaultslim.Herwigpp_UEEE3_CTEQ6L1_Gtt_G1100_T2500_L700.merge/','/r02/atlas/gillam/rpv/slims-1119RPV/defaultslim/Signal/')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.0125RPV_p1328-defaultslim.periodI3.physics_Muons/','/r02/atlas/gillam/rpv/slims-0125RPV_p1328/defaultslim/Data/')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.0123RPV_p1328-defaultslim.156510.Herwigpp_UEEE3_CTEQ6L1_Gtt_G800_T2500_L400/','/r02/atlas/gillam/rpv/slims-0123RPV_p1328/defaultslim/Signal/')
  #validator = PandaValidator(['thomas','gillam'],3,'user.tgillam.0130RPV_p1328-defaultslim.146438.AlpgenJimmy_AUET2CTEQ6L1_WgammaNp2_LeptonPhotonFilter/','/r02/atlas/gillam/rpv/slims-0130RPV_p1328/defaultslim/MC/')
  #validator = PandaValidator(['thomas','gillam'],70,'user.tgillam.20130604RPV-defaultslim.00204853.physics_Muons.130605031356/','/r02/atlas/gillam/rpv/slims-20130604RPV/defaultslim/Data/')
  validator = PandaValidator(['thomas','gillam'],2,'user.tgillam.20130812RPV-defaultslim.173834.MadGraphPythia_AUET2BCTEQ6L1_SMGG2CNsl_1065_785_645_505/', '/r02/atlas/gillam/rpv/slims-20130812RPV/defaultslim/Signal/')
  print validator
  #print validator.jobDict
  #print validator.dirDict
  #print validator.missingOutputFiles()

if __name__ == '__main__':
  main()
