# Library to scrape jobset summary from the Pandamon website, and return
# in an easy-to-use container class

import HTMLParser

class PandaParser(HTMLParser.HTMLParser):
  def __init__(self, jobsetContainer):
    HTMLParser.HTMLParser.__init__(self)
    
    # Save reference to the jobset container object
    self._jobsetContainer = jobsetContainer

    # Initialise variables that denote what part of the page we're in
    self.inDivThree = False

    # Parsing to get the number of jobsets
    self.inSummaryTable = False
    self.inSummaryBTag = False
    self.doneSummary = False

    # The main jobset summary table
    self.inJobsetTable = False

    self.doneOneRow = False

    self._resetRowVars()

  def _resetRowVars(self):
    '''Reset row state vars'''
    # There are two rows in each element
    self.inRowOne = False
    self.inRowTwo = False
    self.doneRowOne = False
    self.doneRowTwo = False

    self.inUserJobId = False
    self.doneUserJobId = False
    self.inCreated = False
    self.doneCreated = False
    self.inLatest = False
    self.doneLatest = False
    self.inJobs = False
    self.doneJobs = False
    self.inPrerun = False
    self.donePrerun = False
    self.inRunning = False
    self.doneRunning = False
    self.inHolding = False
    self.doneHolding = False
    self.inFinished = False
    self.doneFinished = False
    self.inFailed = False
    self.doneFailed = False
    self.inCancelled = False
    self.doneCancelled = False
    self.inMerging = False
    self.doneMerging = False
    self.inBuildJob = False
    self.doneBuildJob = False
    self.inSite = False
    self.doneSite = False

    self.inInput = False
    self.doneInput = False
    self.inInOrOutTest = False
    self.doneInOrOutTest = False
    self.inOutput = False
    self.doneOutput = False

    self._user = ''
    self._jobID = 0
    self._created = ''
    self._latest = ''
    self._jobs = 0
    self._prerun = 0
    self._running = 0
    self._holding = 0
    self._finished = 0
    self._failed = 0
    self._cancelled = 0
    self._merging = 0
    self._site = ''

    self._input = ''
    self._output = ''

  def handle_starttag(self, tag, attrs):
    if not self.inDivThree and tag == 'div' and ('id', 'three') in attrs:
      self.inDivThree = True
    elif self.inDivThree:
      if not self.doneSummary and tag == 'table' and len(attrs) == 0:
        self.inSummaryTable = True
      elif self.inSummaryTable:
        if not self.inSummaryBTag and tag == 'b':
          self.inSummaryBTag = True
        elif self.inSummaryBTag and tag == 'b':
          # Stupid bug in the panda source code... they have two opening <b>s,
          # and no closing. Also no closing table, so just quit the table now...
          self.inSummaryBTag = False
          self.inSummaryTable = False
          self.doneSummary = True
      elif not self.inJobsetTable and tag == 'table' and ('border', '1') in attrs and ('cellspacing', '0') in attrs and ('cellpadding', '3') in attrs:
        self.inJobsetTable = True
      elif self.inJobsetTable:
        #if not self.doneOneRow and not self.doneRowOne and tag == 'tr' and len(attrs) == 0:
        if not self.doneRowOne and tag == 'tr' and len(attrs) == 0:
          self.inRowOne = True
        elif self.inRowOne:
          # Deal with all tags we care about that occur in row one
          if not self.doneUserJobId and tag == 'a':
            self.inUserJobId = True
          elif self.doneUserJobId and tag == 'td':
            if not self.doneCreated:
              self.inCreated = True
            elif not self.doneLatest:
              self.inLatest = True
            elif not self.doneJobs:
              self.inJobs = True
            elif not self.donePrerun:
              self.inPrerun = True
            elif not self.doneRunning:
              self.inRunning = True
            elif not self.doneHolding:
              self.inHolding = True
            elif not self.doneFinished:
              self.inFinished = True
            elif not self.doneFailed:
              self.inFailed = True
            elif not self.doneCancelled:
              self.inCancelled = True
            elif not self.doneMerging:
              self.inMerging = True
            elif not self.doneBuildJob:
              self.inBuildJob = True
            elif not self.doneSite:
              self.inSite = True
        elif self.doneRowOne and not self.inRowTwo and tag == 'tr' and len(attrs) == 0:
          self.inRowTwo = True
        elif self.inRowTwo:
          # Deal with all tags that occur in row two
          if not self.doneInOrOutTest and tag == 'b':
            self.inInOrOutTest = True
          elif not self.doneInput and tag == 'a':
            self.inInput = True
          elif not self.doneOutput and tag == 'a':
            self.inOutput = True

  def handle_endtag(self, tag):
    if self.inDivThree:
      if tag == 'div':
        self.inDivThree = False
      if self.inJobsetTable:
        if tag == 'table':
          self.inJobsetTable = False
        if self.inRowOne:
          if tag == 'tr':
            self.inRowOne = False
            self.doneRowOne = True
          if self.inUserJobId and tag == 'a':
            self.inUserJobId = False
            self.doneUserJobId = True
          elif self.inCreated and tag == 'td':
            self.inCreated = False
            self.doneCreated = True
          elif self.inLatest and tag == 'td':
            self.inLatest = False
            self.doneLatest = True
          elif self.inJobs and tag == 'td':
            self.inJobs = False
            self.doneJobs = True
          elif self.inPrerun and tag == 'td':
            self.inPrerun = False
            self.donePrerun = True
          elif self.inRunning and tag == 'td':
            self.inRunning = False
            self.doneRunning = True
          elif self.inHolding and tag == 'td':
            self.inHolding = False
            self.doneHolding = True
          elif self.inFinished and tag == 'td':
            self.inFinished = False
            self.doneFinished = True
          elif self.inFailed and tag == 'td':
            self.inFailed = False
            self.doneFailed = True
          elif self.inCancelled and tag == 'td':
            self.inCancelled = False
            self.doneCancelled = True
          elif self.inMerging and tag == 'td':
            self.inMerging = False
            self.doneMerging = True
          elif self.inBuildJob and tag == 'td':
            self.inBuildJob = False
            self.doneBuildJob = True
          elif self.inSite and tag == 'td':
            self.inSite = False
            self.doneSite = True
        if self.inRowTwo:
          if self.inInOrOutTest and tag == 'b':
            self.inInOrOutTest = False
            self.doneInOrOutTest = True
          elif self.inInput and tag == 'a':
            self.inInput = False
            self.doneInput = True
          elif self.inOutput and tag == 'a':
            if self._output != 'Monitor':
              self.inOutput = False
              self.doneOutput = True
          if tag == 'tr':
            self.inRowTwo = False
            self.doneRowTwo = True

            # Dump out grabbed data
            job = Jobset()
            job.user = self._user
            job.jobID = self._jobID
            job.created = self._created
            job.latest = self._latest
            job.jobs = self._jobs
            job.prerun = self._prerun
            job.running = self._running
            job.holding = self._holding
            job.finished = self._finished
            job.failed = self._failed
            job.cancelled = self._cancelled
            job.merging = self._merging
            job.site = self._site
            job.inputDataset = self._input
            job.outputDataset = self._output

            # Add jobset to the container
            self._jobsetContainer.append(job)

            # End of jobset, so reset
            self._resetRowVars()
            self.doneOneRow = True

  def handle_data(self, data):
    # If everything works, these should all be mutually exclusive...!!
    if self.inSummaryBTag:
      pass
      #print 'Summary: ', data
    elif self.inUserJobId:
      try:
        self._user = data.split(':')[0]
        self._jobID = int(data.split(':')[1])
      except Exception, e:
        print 'Failed in handle_data for inUserJobId'
        print 'Data:'
        print data
        raise e
    elif self.inCreated:
      self._created = data.strip()
    elif self.inLatest:
      self._latest = data.strip()
    elif self.inJobs:
      raw = data.strip()
      if raw != '' and self._jobs == 0:
        self._jobs = int(raw)
    elif self.inPrerun:
      raw = data.strip()
      if raw != '' and self._prerun == 0:
        self._prerun = int(raw)
    elif self.inRunning:
      raw = data.strip()
      if raw != '' and raw != 'end' and self._running == 0:
        self._running = int(raw)
    elif self.inHolding:
      raw = data.strip()
      if raw != '' and self._holding == 0:
        self._holding = int(raw)
    elif self.inFinished:
      raw = data.strip()
      if raw != '' and self._finished == 0:
        self._finished = int(raw)
    elif self.inFailed:
      raw = data.strip()
      if raw != '' and self._failed == 0:
        self._failed = int(raw)
    elif self.inCancelled:
      raw = data.strip()
      if raw != '' and self._cancelled == 0:
        self._cancelled = int(raw)
    elif self.inMerging:
      raw = data.strip()
      if raw != '' and self._merging == 0:
        self._merging = int(raw)
    elif self.inBuildJob:
      pass
    elif self.inSite:
      self._site = data.strip()
    elif self.inInOrOutTest:
      testval = data.strip()
      if testval.count('Out') > 0:
        self.doneInput = True
    elif self.inInput:
      self._input = data.strip()
    elif self.inOutput:
      self._output = data.strip()

class JobsetContainer(object):
  '''Object to hold and manipulate many Jobset objects'''
  def __init__(self):
    self._jobsetlist = []

  def __str__(self):
    '''Summarise object'''
    s = ''
    for job in self._jobsetlist:
      s += '{0}\n'.format(job)
    return s

  def __len__(self):
    return len(self._jobsetlist)

  def __contains__(self, jobsetID):
    return (self.byID(jobsetID) is not None)

  def __getitem__(self, jobsetID):
    if jobsetID not in self:
      raise IndexError(jobsetID)
    return self.byID(jobsetID)

  def append(self, job):
    '''Add a jobset to the collection'''
    self._jobsetlist.append(job)

  def all(self):
    '''Return list of all jobs'''
    return list(self._jobsetlist)

  def finished(self):
    '''Return list of finished jobs'''
    ret = []
    for job in self._jobsetlist:
      if job.isFinished():
        ret.append(job)
    return ret

  def running(self):
    '''Return list of running jobs'''
    ret = []
    for job in self._jobsetlist:
      if not job.isFinished():
        ret.append(job)
    return ret
  
  def byOutDS(self, outDS):
    '''Return list of jobs associated with given output dataset'''
    ret = []
    for job in self._jobsetlist:
      if job.outputDataset == outDS:
        ret.append(job)
    return ret

  def byID(self, jobID):
    '''Return the job associated with the given jobset ID. If not 
    in the list, return None'''
    ret = None
    for job in self._jobsetlist:
      if job.jobID == jobID:
        ret = job
    return ret

class Jobset(object):
  '''Container class to hold information about a jobset scraped from the site'''

  def __init__(self, jobsetID=0):
    self.user = ''
    self.jobID = jobsetID
    self.created = ''
    self.latest = ''
    self.jobs = 0
    self.prerun = 0
    self.running = 0
    self.holding = 0
    self.finished = 0
    self.failed = 0
    self.cancelled = 0
    self.merging = 0
    self.site = ''
    self.inputDataset = ''
    self.outputDataset = ''
    self.pandaIDs = None

  def __str__(self):
    '''Summarise jobset'''
    return str(self.__dict__)
    return '{0}: {1}/{2}, {3} @ {4} ({5})'.format(self.jobID, self.finished, self.jobs, self.outputDataset, self.site, self.inputDataset)

  def isFinished(self):
    '''Return True if the jobset is finished, False otherwise'''
    return self.finished + self.failed + self.cancelled == self.jobs

  def isFinishedSuccessfully(self):
    '''Return True iff all jobs finished successfully'''
    return self.finished == self.jobs

  def isStuck(self):
    '''**GUESS** whether job is stuck in the 'transferring' state. The main indicator of
    this is when no jobs are running/holding, but the total number of finished jobs is
    less than the total number of jobs'''
    return self.prerun == 0 and self.running == 0 and self.holding == 0 and not self.isFinished()

  def startDateTime(self):
    from datetime import datetime
    if self.created == '':
      return datetime.utcnow()
    dateString = self.created.split()[0]
    timeString = self.created.split()[1]
    tokD = [int(x) for x in dateString.split('-')]
    tokT = [int(x) for x in timeString.split(':')]
    return datetime(tokD[0], tokD[1], tokD[2], tokT[0], tokT[1])


def _downloadPage(url):
  '''Small helper function to download the url as a string'''
  import urllib
  s = None
  while s == None:
    try:
      f = urllib.urlopen(url)
      s = f.read()
      f.close()
    except:
      # Try again in 30 seconds...
      import time
      print 'PandaParser: timeout, retrying in 30s'
      time.sleep(30)
  return s

def getJobsetContainer(names, days):
  '''Main entry point for the module. Will return a jobset container for the given user. Names
  should be a list of first (middle) last names, as it appears on the users page on Pandamon'''
  name = '%20'.join(names)
  url = 'http://panda.cern.ch/server/pandamon/query?ui=user&name={0}&nojobs=yes&days={1}&limit=500000'.format(name, days)
  pandaSource = _downloadPage(url)

  # More workarounds...
  pandaSource = pandaSource.replace('>&nbsp;<', '> &nbsp; <')

  pandaContainer = JobsetContainer()
  parser = PandaParser(pandaContainer)
  parser.feed(pandaSource)
  return pandaContainer


# Some nice example code :)
def main():
  container = getJobsetContainer(['thomas', 'gillam'], 3)
  print container 

  print 'Fully successful jobs:'
  for job in container.finished():
    if job.isFinishedSuccessfully():
      print job

  print '\nAll jobs:'
  for job in container._jobsetlist:
    print job

if __name__ == "__main__":
  main()
