# Specialised version of the *hammer series that does not download, but facilitates "production" style jobs,
# where there are a set of chains of commands. In each chain, any one command cannot be run until the prior is completed.

class JobChain(object):
  def __init__(self):
    self.jobList = []
    self.isDefunct = False

  def append(self, job):
    self.jobList.append(job)

  def site(self):
    try:
      return self.jobList[0].pandaSite
    except IndexError:
      return ''
  
  def duplicateWithNewDatasets(self):
    newJobChain = JobChain()
    for job in self.jobList:
      newCommand = job.command
      inputDataset = job.inputDataset
      outputDataset = job.outputDataset
      if inputDataset is not None:
        newCommand = newCommand.replace(inputDataset, self.getNewerDatasetName(inputDataset))
      newCommand = newCommand.replace(outputDataset, self.getNewerDatasetName(outputDataset))

      newJob = _Job(newCommand, job.gridSetupScript)
      newJobChain.append(newJob)
    return newJobChain

  def getNewerDatasetName(self, currentDatasetName):
    if currentDatasetName[-1] != '/':
      currentDatasetName.append('/')
    import re
    match = re.search('(\S+)_X(\d+)/', currentDatasetName)
    newDatasetName = ''
    index = 0
    if match is not None:
      datasetStem = match.group(1)
      newIndex = int(match.group(2)) + 1
      newDatasetName = '{0}_X{1}/'.format(datasetStem, newIndex)
    else:
      newDatasetName = currentDatasetName[:-1]+'_X1/'
    return newDatasetName

  def pandaUpdate(self, pandaSummary, pandaRetryLimit):
    for job in self.jobList:
      job.pandaUpdate(pandaSummary, pandaRetryLimit)

  def evgenDataset(self):
    return self.jobList[0].outputDataset

  def __iter__(self):
    return self.jobList.__iter__()

  def __len__(self):
    return len(self.jobList)

  def __contains__(self, v):
    return v in self.jobList

  def __getitem__(self, v):
    return self.jobList[v]


class Steamhammer(object):
  '''Resubmit and download. Repeat, until it works. Show Don Quixote and the Panda who's boss...'''
  def __init__(self):
    self._jobChains = []
    self._names = ['thomas', 'gillam']
    self._days = 15
    self._pandaRetryLimit = 7
    import os.path
    self._gridSetupScript = os.path.expanduser('~gillam/scripts/grid_setup.sh')
    self._delay = 30 # Chillaxing time, in seconds
    self._pandaSummary = None
    # List of sites known to be annoying
    #self._excludedSites = set(['ANALY_SCINET','ANALY_MPPMU','ANALY_IL-TAU-HEP-CREAM','ANALY_IL-TAU-HEP','ANALY_IllinoisHEP','ANALY_INFN-GENOVA','ANALY_UCL','ANALY_CAM','ANALY_AUSTRALIA','ANALY_NET2'])
    #self._excludedSites = set(['ANALY_IL-TAU-HEP', 'ANALY_CYF', 'ANALY_CAM', 'ANALY_LRZ', 'ANALY_HU_ATLAS_Tier2', 'ANALY_IllinoisHEP', 'ANALY_SCINET', 'ANALY_INFN-GENOVA', 'ANALY_GOEGRID', 'ANALY_SHEF', 'ANALY_NIKHEF-ELPROD', 'ANALY_FREIBURG', 'ANALY_MPPMU', 'ANALY_UCL', 'ANALY_OU_OCHEP_SWT2', 'ANALY_AUSTRALIA', 'ANALY_NET2', 'ANALY_GRIF-LAL', 'ANALY_IL-TAU-HEP-CREAM'])
    # From Mike Flowerdew's grid
    #self._excludedSites = set(['ANALY_IL-TAU-HEP', 'ANALY_INFN-GENOVA', 'ANALY_OU_OCHEP_SWT2', 'ANALY_ROMANIA07', 'ANALY_IL-TAU-HEP-CREAM', 'ANALY_CYF', 'ANALY_SCINET', 'ANALY_GOEGRID', 'ANALY_SHEF', 'ANALY_NIKHEF-ELPROD', 'ANALY_FREIBURG', 'ANALY_UCL', 'ANALY_SFU_bugaboo', 'ANALY_GRIF-LAL', 'ANALY_IllinoisHEP', 'ANALY_INFN-FRASCATI', 'ANALY_MPPMU', 'ANALY_LRZ', 'ANALY_CAM', 'ANALY_HU_ATLAS_Tier2', 'ANALY_AUSTRALIA', 'ANALY_NET2'])
    self._excludedSites = set(['ANALY_CSCS', 'ANALY_IL-TAU-HEP', 'ANALY_INFN-GENOVA', 'ANALY_OU_OCHEP_SWT2', 'ANALY_ROMANIA07', 'ANALY_IL-TAU-HEP-CREAM', 'ANALY_CYF', 'ANALY_SCINET', 'ANALY_GOEGRID', 'ANALY_SHEF', 'ANALY_NIKHEF-ELPROD', 'ANALY_FREIBURG', 'ANALY_UCL', 'ANALY_SFU_bugaboo', 'ANALY_GRIF-LAL', 'ANALY_IllinoisHEP', 'ANALY_PSNC', 'ANALY_INFN-FRASCATI', 'ANALY_JINR', 'ANALY_MPPMU', 'ANALY_LRZ', 'ANALY_CAM', 'ANALY_INFN-MILANO-ATLASC', 'ANALY_HU_ATLAS_Tier2', 'ANALY_LIP-Lisbon', 'ANALY_MWT2', 'ANALY_AUSTRALIA', 'ANALY_NET2'])

    import tweeter
    self._tweeter = tweeter.Tweeter()
    self.numberOfTweetsPerStatus = 3

  def setNames(self, names):
    '''Set your names! Important if you want this to work... ;-)'''
    self._names = names

  def setDaysHistory(self, days):
    '''Set number of days history to use in panda. Default is 3'''
    self._days = days

  def setPandaRetryLimit(self, limit):
    '''Set the maximum number of times we should attempt to retry
    any given command'''
    self._pandaRetryLimit = limit

  def setGridSetup(self, gridSetupScript):
    '''Set the location of the Grid setup script'''
    self._gridSetupScript = gridSetupScript

  def setDelay(self, delay):
    '''Set a short delay to insert into the while loop'''
    self._delay = delay

  def appendJobChain(self, chainOfCommands):
    '''Append a list of jobs to the list to run. Call AFTER setting other parameters!!!'''
    jobChain = JobChain()
    for command in chainOfCommands:
      jobChain.append(_Job(command, gridSetupScript=self._gridSetupScript))
    self._jobChains.append(jobChain)

  def run(self):
    '''Hold on tight!'''
    self._preRunSetup()
    while self._jobsStillGoing():
      self._getPandaSummary()
      self._printCurrentStatus()
      for jobChain in self._jobChains:
        if jobChain.isDefunct:
          continue
        for job in jobChain:
          if job.abandoned:
            jobChain.isDefunct = True
            self._excludedSites.add(jobChain.site())
            print 'Current site blacklist:',self._excludedSites
            self._jobChains.append(jobChain.duplicateWithNewDatasets())
            break
          if job.pandaCompleted:
            continue
          self._processIncompleteJob(job)
          break
      import time
      time.sleep(self._delay)
    self._printSummary()

  def _preRunSetup(self):
    self._getPandaSummary()
    listOfNewJobChains = []
    for jobChain in self._jobChains:
      jobChain.pandaUpdate(self._pandasummary, self._pandaRetryLimit)
      currJobChain = jobChain 
      finished = False
      while finished == False:
        newerEvgenDataset = currJobChain.getNewerDatasetName(currJobChain.evgenDataset())
        print 'Searching for evgen job',newerEvgenDataset
        if len(self._pandasummary.byOutDS(newerEvgenDataset)) > 0:
          currJobChain.isDefunct = True
          self._excludedSites.add(currJobChain.site())
          newJobChain = currJobChain.duplicateWithNewDatasets()
          newJobChain.pandaUpdate(self._pandasummary, self._pandaRetryLimit)
          listOfNewJobChains.append(newJobChain)
          currJobChain = newJobChain
        else:
          finished = True
    for jobChain in listOfNewJobChains:
      self._jobChains.append(jobChain)
    print 'Current site blacklist:',self._excludedSites

  def _jobsStillGoing(self):
    for jobChain in self._jobChains:
      if jobChain.isDefunct:
        continue
      for job in jobChain:
        # If a job is abandoned, so are all subsequent jobs in the chain
        if job.abandoned:
          break
        if not job.completedOrAbandoned():
          return True
    return False

  def _getPandaSummary(self):
    import pandaParser
    self._print('parsing Panda')
    self._pandasummary = pandaParser.getJobsetContainer(self._names, self._days)

  def _processIncompleteJob(self, job):
    job.pandaUpdate(self._pandasummary, self._pandaRetryLimit)
    # If not running and not stuck, resubmit
    if not job.pandaCompleted and not job.abandoned and not job.pandaRunning and not job.pandaStuck:
      if job.pandaNumFails  == 0:
        job.pandaSubmit(self._excludedSites)
      else:
        job.pandaRetry(ignoreDuplication=True)
    elif job.pandaStuck:
      self._print('{0} is stuck (maybe transferring?).'.format(job.outputDataset))

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Steamhammer:',msg

  def _printCurrentStatus(self):
    '''Print a summary of the current jobs that are running, downloading etc'''
    print '-- Steamhammer Status --'
    import datetime
    print 'Timestamp: {0}'.format(datetime.datetime.now())
    print 'Current jobs:'

    for jobChain in self._jobChains:
      if jobChain.isDefunct or jobChain[-1].pandaCompleted:
        continue
      print 'Job chain start:'
      for job in jobChain:
        status = '    '
        color = None
        if job.pandaRunning:
          status += 'Panda: running, ' 
        if job.pandaStuck:
          status += 'Panda: stuck, '
        if job.pandaCompleted:
          status += 'Panda: completed, '
          color = 'green'
        if job.abandoned:
          status += 'Panda: abandoned, '
          color = 'red'
        status += 'Panda fails: {0}'.format(job.pandaNumFails)
        from termcolor import cprint
        cprint(str('  '+str(job)), color)
        cprint(status, color)
    print '--------------------------'
    print '- Completed final stages -'
    print '--------------------------'
    listOfFinalStages = []
    for jobChain in self._jobChains:
      if jobChain.isDefunct:
        continue
      if jobChain[-1].pandaCompleted:
        listOfFinalStages.append(str(jobChain[-1]))
    listOfFinalStages.sort()
    for d3pd in listOfFinalStages:
      print d3pd
    print '--------------------------'
    for i in range(self.numberOfTweetsPerStatus):
      self._tweeter.printRandomTweet()
    print '--------------------------\n'

  def _printSummary(self):
    '''Print a summary that we'd typically want to show at the end of the run'''
    print '\n\n== STEAMHAMMER SUMMARY =='
    print '==========================\n'
    for jobChain in self._jobChains:
      print 'Job chain start:'
      for job in jobChain:
        status = 'Completed'
        if job.abandoned:
          status = 'Abandoned'
        print '  {0}: {1}'.format(status, job.outputDataset)
        if job.abandoned:
          break
    print '\n--------------------------'
    print 'Usable evgen datasets:'
    evgenList = []
    for jobChain in self._jobChains:
      if not jobChain.isDefunct:
        evgenList.append(jobChain.evgenDataset())
    evgenList.sort()
    for evgen in evgenList:
      print evgen

    print 'Excluded sites (by end of run):'
    print self._excludedSites


class _Job(object):
  '''Internal class to represent a job'''
  def __init__(self, command, gridSetupScript):
    '''Initialise a new job object'''
    # Use the --disableAutoRetry option to prevent the servers screwing us over
    if command.count('--disableAutoRetry') == 0:
      command += ' --disableAutoRetry'

    # Common info
    self.abandoned = False
    self.command = command
    self.inputDataset = self._figureInputDataset()
    self.outputDataset = self._figureOutputDataset()

    # Panda info
    self.pandaNumFails = 0
    self.pandaCompleted = False
    self.pandaRunning = False
    self.pandaStuck = False
    self.pandaJobsetID = 0
    self.pandaSite = ''
    self.gridSetupScript = gridSetupScript
    self.prevPandaSubmitTime = None
    self.lastPandaUpdatedTime = None
    self.pandaMinDelayBetweenSubmits = 600
    self.pandaSubmitAttemptCount = 0
  
  def __str__(self):
    return self.outputDataset

  def isEvgenStyleCommand(self):
    return command.count('--inDS') == 0

  def pandaUpdate(self, pandasummary, retryLimit):
    '''Given the panda summary from pandaParser, update this job's
    its status flags'''
    pjobs = pandasummary.byOutDS(self.outputDataset)
    # print 'Steamhammer: found',len(pjobs),'panda entries for',self.outputDataset
    running = False
    completed = False
    stuck = False
    numFails = 0
    foundJobID = False
    for pjob in pjobs:
      if not pjob.isFinished():
        #print 'Steamhammer: dataset running in panda;',self.outputDataset
        running = True
        self.pandaJobsetID = pjob.jobID
        self.pandaSite = pjob.site
        foundJobID = True
      elif pjob.isFinished():
        if pjob.isFinishedSuccessfully():
          #print 'Steamhammer: dataset completed in panda;',self.outputDataset
          completed = True
        else:
          # This is a failure...
          numFails += 1
      if pjob.isStuck():
        stuck = True

    # If no active jobs, pick most recent jobsetid
    if not foundJobID:
      self.pandaJobsetID = 0
      for pjob in pjobs:
        if pjob.jobID > self.pandaJobsetID:
          self.pandaJobsetID = pjob.jobID
          self.pandaSite = pjob.site

    # Take into account the fact that if jobs are still running,
    # cannot be completed
    if running:
      completed = False

    # Update the job container
    self.pandaCompleted = completed
    self.pandaRunning = running
    self.pandaStuck = stuck
    self.pandaNumFails = numFails

    # Should this dataset be abandoned completely??
    if numFails >= retryLimit:
      self._print('{0} has failed running {1} times, abandoning'.format(self.outputDataset, self.pandaNumFails))
      self.abandoned = True

    import time
    self.lastPandaUpdatedTime = time.time()

  def completedOrAbandoned(self):
    return self.pandaCompleted or self.abandoned

  def pandaSubmit(self, excludedSites):
    '''Submit this job to panda. Blocks until done'''
    if self.pandaSubmitAttemptCount > 2:
      print 'Attempted to submit 3 times, giving up'
      self.abandoned = True
    if (self.prevPandaSubmitTime is not None) and (self.lastPandaUpdatedTime - self.prevPandaSubmitTime) < self.pandaMinDelayBetweenSubmits:
      self._print('Skipping submit, delay {0} less than minimum delay {1}.'.format((self.lastPandaUpdatedTime - self.prevPandaSubmitTime), self.pandaMinDelayBetweenSubmits))
      return
    excludedSitesOption = '--excludedSite='+(','.join(excludedSites))
    command = '. {0}; {1} {2}'.format(self.gridSetupScript, self.command, excludedSitesOption)
    self._print('Submitting {0}'.format(self.outputDataset))
    self._print('Executing: {0}'.format(self.command))
    import subprocess
    subprocess.call(command, shell=True)
    import time
    self.prevPandaSubmitTime = time.time()
    if not (self.pandaCompleted or self.abandoned or self.pandaRunning):
      self.pandaSubmitAttemptCount += 1
      print 'Submit attempt count:',self.pandaSubmitAttemptCount

  def pandaRetry(self, ignoreDuplication):
    if (self.prevPandaSubmitTime is not None) and (self.lastPandaUpdatedTime - self.prevPandaSubmitTime) < self.pandaMinDelayBetweenSubmits:
      self._print('Skipping resubmit of {0}, delay {1} less than {2}.'.format(self.pandaJobsetID, (self.lastPandaUpdatedTime - self.prevPandaSubmitTime), self.pandaMinDelayBetweenSubmits))
      return
    command = '. {0}; pbook -c "sync()"; pbook -c "retry({1}, ignoreDuplication={2}, retryBuild=True)"'.format(self.gridSetupScript, self.pandaJobsetID, ignoreDuplication)
    import subprocess
    subprocess.call(command, shell=True)
    import time
    self.prevPandaSubmitTime = time.time()

  ##### INTERNAL FUNCTIONS #####

  def _print(self, msg):
    '''Print, prepending with name'''
    print 'Steamhammer:',msg

  def _figureOutputDataset(self):
    '''Figure out the dataset name given a command'''
    import re
    # TODO do we need to add more allowed characters into dataset name?
    outputDataset = re.search('--outDS[\'"=\s]*([-_./\w]+)', self.command).group(1)
    # Ensure it ends with a forward-slash
    if outputDataset[-1] != '/':
      outputDataset += '/'
    return outputDataset

  def _figureInputDataset(self):
    '''Figure out the dataset name given a command'''
    import re
    # TODO do we need to add more allowed characters into dataset name?
    try:
      inputDataset = re.search('--inDS[\'"=\s]*([-_./\w]+)', self.command).group(1)
      # Ensure it ends with a forward-slash
      if inputDataset[-1] != '/':
        inputDataset += '/'
      return inputDataset
    except:
      return None

