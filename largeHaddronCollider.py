def collide(outputFile, inputFiles, fastMode=True, safeMode=False):
  collider = LargeHaddronCollider(outputFile, inputFiles, fastMode=fastMode, safeMode=safeMode)
  collider.run()


class LargeHaddronCollider(object):
  def __init__(self, outputFile, inputFiles, fastMode=True, defcon=2, safeMode=False):
    self._defcon = defcon
    self._safeMode = safeMode
    self._fastMode = fastMode
    self._outputFile = outputFile
    self._inputFiles = inputFiles
    import tempfile
    self._scratchDir = tempfile.mkdtemp(dir='/r02/atlas/gillam/')
    self._tempFiles = []

  def __del__(self):
    import shutil
    shutil.rmtree(self._scratchDir)

  def run(self):
    if self._fastMode:
      commands = list(self._fastCommands())
    else:
      commands = self._commands()
    if self._fastMode:
      # Distribute
      commandsToDistribute = ['. /usera/gillam/scripts/root_setup.sh; '+x for x in commands[:-1]]
      #print 'Distributing:',commandsToDistribute
      if len(commandsToDistribute) > 1:
        from distributedExecute import DistributedExecute
        dist = DistributedExecute(commandsToDistribute, self._defcon, self._safeMode)
        dist.runAndWait()
      # Run last command
      import subprocess
      print 'Running:',commands[-1]
      subprocess.call(commands[-1], shell=True)
    else:
      for command in commands:
        import subprocess
        subprocess.call(command, shell=True)
        self._deleteOldTempFile()
    print 'Moving output to',self._outputFile
    import shutil
    shutil.move(self._tempFiles[-1], self._outputFile)

  def _deleteOldTempFile():
    if len(self._tempFiles) > 1:
      import os
      os.remove(self._tempFiles[-2])

  def _commands(self):
    count = 0
    command = ''
    for inputFile in self._existingInputFiles():
      if count == 0:
        command = 'hadd {0} {1}'.format(self._nextOutputFile(), self._previousOutputFile())
        self._storeNextOutputFile()
      command += ' '+inputFile
      count += 1
      if count == 100:
        yield command
        count = 0
    if count > 0:
      yield command

  def _fastCommands(self):
    count = 0
    commandCount = 0
    command = ''
    for inputFile in self._existingInputFiles():
      if count == 0:
        command = 'hadd {0}'.format(self._nextOutputFile())
        self._storeNextOutputFile()
        commandCount += 1
      command += ' '+inputFile
      count += 1
      if count == 100:
        yield command
        count = 0
    if count > 0:
      yield command
    if commandCount > 1:
      finalMerge = 'hadd {0} '.format(self._nextOutputFile())
      finalMerge += ' '.join(self._tempFiles)
      self._storeNextOutputFile()
      yield finalMerge
  
  def _existingInputFiles(self):
    import os
    for inputFile in self._inputFiles:
      if os.path.isfile(inputFile):
        if os.path.getsize(inputFile) == 0:
          continue
        yield inputFile

  def _nextOutputFile(self):
    import os
    outputFile = os.path.join(self._scratchDir, '{0}.root'.format(len(self._tempFiles)))
    return outputFile

  def _storeNextOutputFile(self):
    self._tempFiles.append(self._nextOutputFile())

  def _previousOutputFile(self):
    if len(self._tempFiles) == 0:
      return ''
    else:
      return self._tempFiles[-1]


def test():
  infiles = ['{0}.root'.format(x) for x in range(1001)]
  outfile = 'output.root'
  collide(outfile, infiles)

if __name__ == '__main__':
  test()
