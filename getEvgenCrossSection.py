#!/usr/bin/env python

def main():
  #destinationBlock = 'user.tgillam.990026.stgl1000_391.evgen.pool.v6/'
  #destinationBlock = 'user.tgillam.990120.stgl900_913.evgen.pool.v7/'
  destinationBlock = 'user.tgillam.144796.gs00_st240_c140_n080_LF_hwppsusy.evgen.pool.v3/'
  print getCrossSectionForDataset(destinationBlock)

def getCrossSectionForDataset(dataset):
  if not dataset.endswith('/'):
    dataset = dataset+'/'
  evgenCrossSection = EvgenCrossSection(evgenDataset=dataset)
  return evgenCrossSection.crossSectionInPicoBarn()
  
class EvgenCrossSection(object):
  def __init__(self, **kwargs):
    self.evgenDataset = None
    for key in kwargs:
      setattr(self, key, kwargs[key])

  def crossSectionInPicoBarn(self):
    totalCrossSection = 0.0
    foundSomething = False
    for logFileURL in self._athenaStdoutURLs():
      foundSomething = True
      totalCrossSection += self._crossSectionInPicoBarnForLogFile(logFileURL)
    if foundSomething:
      return totalCrossSection
    else:
      raise Exception('Coudln\'t find cross-section!')

  def _crossSectionInPicoBarnForLogFile(self, logFileURL):
    stdoutFile = self._downloadPage(logFileURL)
    import re
    for line in stdoutFile.split('\n'):
      match = re.match('.*MetaData: cross-section \(nb\) = (.*)', line)
      if match is not None:
        print 'Found x-sec: ',1000*float(match.group(1))
        return 1000*float(match.group(1))
    raise Exception('Couldn\'t find the cross-section!')

  def _athenaStdoutURLs(self):
    for findAndViewLogFilesURL in self._findAndViewLogFilesURLs():
      searchPage = self._downloadPage(findAndViewLogFilesURL)
      import re
      found = False
      for line in searchPage.split():
        match = re.match('.*href="(.*athena_stdout.txt)".*', line)
        if match is not None:
          found = True
          yield match.group(1)
      if not found:
        raise Exception('Couldn\'t find the athena stdout URL!')

  def _findAndViewLogFilesURLs(self):
    for jobPageURL in self._findJobPageURLs():
      jobPage = self._downloadPage(jobPageURL)
      import re
      for line in jobPage.split('\n'):
        match = re.match(".*href='(.*)'.*Find", line)
        if match is not None:
          url = match.group(1)
          pandaID = jobPageURL.split('=')[-1]
          if self._didPandaIDSucceed(pandaID):
            yield url 

  def _didPandaIDSucceed(self, pandaID):
    for line in self._searchPage.split('\n'):
      if pandaID in line and 'finished' in line:
        return True
    return False

  def _findJobPageURLs(self):
    self._searchPage = self._downloadPage(self._searchPageURL())
    import re
    for line in self._searchPage.split('\n'):
      if 'Associated build job' in line:
        continue
      match = re.search("a href='(http://panda.cern.ch/server/pandamon/query\?job=\d*)'", line)
      if match is not None:
        yield match.group(1)
      else:
        match = re.search("a href='#(\d*)'", line)
        if match is not None:
          yield 'http://panda.cern.ch/server/pandamon/query?job='+match.group(1)

  def _searchPageURL(self):
    return 'http://panda.cern.ch/server/pandamon/query?job=*&days=30&destinationDBlock={0}'.format(self.evgenDataset)

  def _downloadPage(self, url):
    import urllib
    s = None
    while s is None:
      f = urllib.urlopen(url)
      s = f.read()
      f.close()
    return s

if __name__ == '__main__':
  main()
