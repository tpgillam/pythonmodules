class ProductionCommandGenerator(object):
  def __init__(self):
    self.userName = 'tgillam'
    self.runNumber = None
    self.runName = None
    self.mcJobConfigFileName = None
    self.slhaFileName = None
    self.versionPostfix = None
    self.evgenExtFiles = ''
    self.numEvents = 0

  def evgenCommand(self):
    # Evgen e1381
    numSplits = int(int(self.numEvents) / 10000)
    if type(self.evgenExtFiles) == list:
      self.evgenExtFiles = ','.join(self.evgenExtFiles)
      self.evgenExtFiles = ','+self.evgenExtFiles
    evgenCommand = r'pathena -c "EvtMax=10000" --trf "Generate_trf.py ecmEnergy=8000. runNumber={1} randomSeed=%RNDM:12345 firstEvent=1 maxEvents=10000 jobConfig=\"{4}\" outputEVNTFile=\"%OUT.EVNT.pool.root\" " --athenaTag=17.2.4.1 --split={7} --extFile={3}{6} --highMinDS=mc12_8TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1119_s1469_s1471/ --lowMinDS=mc12_8TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1119_s1469_s1471/ --outDS=user.{0}.{1}.{2}.evgen.pool.{5}/'.format(self.userName, self.runNumber, self.runName, self.slhaFileName, self.mcJobConfigFileName, self.versionPostfix, self.evgenExtFiles, numSplits)
    return evgenCommand

  def simCommand(self):
    # Simulation a175
    simCommand = r'pathena --trf "AtlasG4_trf.py conditionsTag=\"OFLCOND-MC12-SIM-00\" geometryVersion=\"ATLAS-GEO-20-00-01_VALIDATION\" physicsList=QGSP_BERT preInclude=\"SimulationJobOptions/preInclude.MC12_AtlfastII_Hits.py\" maxEvents=500 inputEvgenFile=\"%IN\" outputHitsFile=\"%OUT.HITS.pool.root\" randomSeed=%RNDM:54321 skipEvents=%SKIPEVENTS" --athenaTag=17.2.4.1 --nFilesPerJob=1 --nEventsPerFile=10000 --nEventsPerJob=500 --highMinDS=mc12_8TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1119_s1469_s1471/ --lowMinDS=mc12_8TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1119_s1469_s1471/ --inDS=user.{0}.{1}.{2}.evgen.pool.{3}/ --outDS=user.{0}.{1}.{2}.HITS.pool.{3}/'.format(self.userName, self.runNumber, self.runName, self.versionPostfix)
    return simCommand

  def mergeCommand(self):
    # Merging_trf step, a177
    mergeCommand = r'''pathena --trf "Merging_trf.py maxEvents='1000' inputHitsFile=%IN outputHitsFile=%OUT.merge.HITS.pool.root inputLogsFile=NONE skipEvents=0 RunNumber={1} geometryVersion='ATLAS-GEO-20-00-01' conditionsTag='OFLCOND-MC12-AFII-07' preInclude='FastSimulationJobTransforms/jobConfig.v14_Parametrisation.py,FastCaloSimHit/preInclude.AF2Hit.py' postInclude='FastCaloSimHit/postInclude.AF2FilterHitItems.py,FastSimulationJobTransforms/jobConfig.FastCaloSim_ID_cuts.py,FastSimulationJobTransforms/jobConfig.egamma_lateral_shape_tuning.config20.py' " --nFilesPerJob=1 --athenaTag=17.2.4.1 --highMinDS=mc12_8TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1119_s1469_s1471/ --lowMinDS=mc12_8TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1119_s1469_s1471/ --inDS=user.{0}.{1}.{2}.HITS.pool.{3}/ --outDS=user.{0}.{1}.{2}.merge.HITS.pool.{3}/'''.format(self.userName, self.runNumber, self.runName, self.versionPostfix)
    return mergeCommand

  def digiCommand(self):
    # Digi + Reco a177
    digiCommand = r'''pathena --trf "DigiMReco_trf.py maxEvents=1000 digiSeedOffset1=%RNDM:50000 digiSeedOffset2=%RNDM:50022 skipEvents=0 DataRunNumber=-1 HighPtMinbiasHitsFile=%HIMBIN LowPtMinbiasHitsFile=%LOMBIN autoConfiguration=everything conditionsTag=\"OFLCOND-MC12-AFII-07\" digiRndmSvc=AtDSFMTGenSvc extraParameter=outputRDOFile=tmpRDO.pool.root geometryVersion=\"ATLAS-GEO-20-00-01\" jobNumber=0 numberOfHighPtMinBias=0.045336 numberOfLowPtMinBias=39.954664 postInclude_h2r='FastSimulationJobTransforms/postInclude.AF2DigiSetContainerNames.py' preExec_e2a='TriggerFlags.AODEDMSet=\"AODSLIM\";rec.Commissioning.set_Value_and_Lock(True);jobproperties.Beam.energy.set_Value_and_Lock(4000*Units.GeV);muonRecFlags.writeSDOs=True;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);jobproperties.Beam.bunchSpacing.set_Value_and_Lock(50)' preExec_h2r='from LArROD.LArRODFlags import larRODFlags;larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.UseDelta.set_Value_and_Lock(3);from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.overrideMetadata+=[\"SimLayout\",\"PhysicsList\"]' preExec_r2e='from LArROD.LArRODFlags import larRODFlags;larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.UseDelta.set_Value_and_Lock(3);rec.Commissioning.set_Value_and_Lock(True);jobproperties.Beam.energy.set_Value_and_Lock(4000*Units.GeV);muonRecFlags.writeSDOs=True;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);jobproperties.Beam.bunchSpacing.set_Value_and_Lock(50);from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' preInclude_h2r='Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrains2011Config8_DigitConfig.py,RunDependentSimData/configLumi_mc12_v1.py' triggerConfigByRun='{{195847:\"MCRECO:DB:TRIGGERDBMC:325,142,266\"}}' inputHitsFile=%IN outputAODFile=%OUT.AOD.pool.root" --nFilesPerJob=1 --athenaTag=17.2.4.1 --highMinDS=mc12_8TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1119_s1469_s1471/ --lowMinDS=mc12_8TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1119_s1469_s1471/ --nHighMin=4 --nLowMin=4 --excludeFile=\*RDO.pool.root\*,\*ESD.pool.root\* --inDS=user.{0}.{1}.{2}.merge.HITS.pool.{3}/ --outDS=user.{0}.{1}.{2}.AOD.pool.{3}/'''.format(self.userName, self.runNumber, self.runName, self.versionPostfix)
    return digiCommand

  def dpdCommand(self):
    # DPD - p1032
    dpdCommand = r'pathena --trf "Reco_trf.py autoConfiguration=everything inputAODFile=%IN outputNTUP_SUSYFile=%OUT.NTUP_SUSY.root" --nFilesPerJob=20 --athenaTag=17.2.4.1 --inDS=user.{0}.{1}.{2}.AOD.pool.{3}/ --outDS=user.{0}.{1}.{2}.NTUP_SUSY.{3}/'.format(self.userName, self.runNumber, self.runName, self.versionPostfix)
    return dpdCommand
