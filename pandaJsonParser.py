# Fields we want (can change) from the taskBuffer
_taskBufferFields = (
    'jobsetID',
    'pandaID',
    'outputFileBytes',
    'creationTime',
    'modificationTime',
    'transformation',
    'jobStatus',
    'destinationDBlock',
    'prodDBlock',
    'computingSite',
    'attemptNr',
    'parentID',
    )

# Pre-baked fields returned by LFN request
_lfnFields = ('pandaID', 'type', 'LFN', 'dataset', 'GUID')

def _datetimeFromString(datetimeString):
  dateString = datetimeString.split()[0]
  timeString = datetimeString.split()[1]
  tokD = [int(x) for x in dateString.split('-')]
  tokT = [int(x) for x in timeString.split(':')]
  from datetime import datetime
  return datetime(tokD[0], tokD[1], tokD[2], tokT[0], tokT[1])


class TaskBufferRow(object):
  def __init__(self, row):
    self.row = row

  def __getattr__(self, key):
    if key == 'buildJob':
      return self._isBuildJob()
    elif key == 'creationDatetime':
      return _datetimeFromString(self.creationTime)
    elif key == 'modificationDatetime':
      return _datetimeFromString(self.modificationTime)
    index = _taskBufferFields.index(key)
    return self.row[index]

  def _isBuildJob(self):
    return 'buildGen' in self.transformation

  def __repr__(self):
    return str(self.pandaID)

  def __str__(self):
    return 'Jobset {3} - pandaID: {0}, status: {1}, attemptNr: {2}, parentID: {4}'.format(self.pandaID, self.jobStatus, self.attemptNr, self.jobsetID, self.parentID)
    return 'Job - pandaID: {0}, outDS: {1}'.format(self.pandaID, self.destinationDBlock)


class LFNRow(object):
  def __init__(self, row):
    self.row = row

  def __getattr__(self, key):
    index = _lfnFields.index(key)
    return self.row[index]


class PandaJsonParser(object):
  def __init__(self):
    self.username = 'thomas gillam'
    self.days = 3
    self.verbose = True
    self._taskBufferRows = None

  def updateTaskBuffer(self, jobsetID=None):
    tables = ['atlas_panda.jobsArchived4','atlas_panda.jobsActive4','atlas_panda.jobsWaiting4','atlas_panda.jobsDefined4']
    if self.days > 3:
      tables.append('atlas_pandaarch.jobsArchived')
    tablesString = str(tables)

    selectedFields = ','.join(_taskBufferFields)
    query = '''taskBuffer?method=getJobIds(site=None,username='{0}',jobtype=None,status=None,table={1},days={2},select='{3}',jobsetid={4})'''.format(self.username, tablesString, self.days, selectedFields, jobsetID)
    if self.verbose:
      print query

    import urllib
    query = urllib.quote(query)
    rowGenerator = self._rowsForQuery(query)
    self._taskBufferRows = self._removeDuplicateRows(rowGenerator)

  def jobsetsInTaskBuffer(self):
    from pandaParser import Jobset, JobsetContainer
    jobsets = JobsetContainer()
    for row in self._taskBufferRows:
      #if row.jobsetID == 152828:
      #  print row
      if row.jobsetID not in jobsets:
        jobset = Jobset(row.jobsetID)
        jobset.user = self.username
        jobset.pandaIDs = []
        jobsets.append(jobset)
      jobset = jobsets[row.jobsetID]
      jobset.jobs += 1
      jobset.pandaIDs.append(row.pandaID)
      if row.buildJob:
        jobset.created = row.creationTime
        jobset.site = row.computingSite
      else:
        if jobset.latest == '' or row.modificationDatetime > _datetimeFromString(jobset.latest):
          jobset.latest = row.modificationTime
        jobset.inputDataset = row.prodDBlock
        jobset.outputDataset = row.destinationDBlock
      simplifiedStatus = self._simplifiedStatus(row.jobStatus)
      setattr(jobset, simplifiedStatus, getattr(jobset, simplifiedStatus) + 1)
    return jobsets

  def jobsInTaskBuffer(self):
    from pandaJobsetParser import Job, JobContainer
    jobs = JobContainer()
    pandaIDs = []
    for row in self._taskBufferRows:
      if row.buildJob: continue
      job = Job()
      job.jobsetID = row.jobsetID
      job.owner = self.username
      job.pandaID = row.pandaID
      job.status = row.jobStatus
      job.created = row.creationTime
      job.ended = row.modificationTime
      job.site = row.computingSite
      job.inputDataset = row.prodDBlock
      job.outputDataset = row.destinationDBlock
      job.outputSize = row.outputFileBytes
      jobs.append(job)
      pandaIDs.append(job.pandaID)

    for pandaID, outputFile, outputBlock, inputFiles in self._jobLFNInfo(pandaIDs):
      job = jobs[pandaID]
      job.outputFile = outputFile
      job.outputBlock = outputBlock
      job.inputFiles = inputFiles
    return jobs

  def _simplifiedStatus(self, jobStatus):
    if jobStatus in ['pending', 'defined', 'waiting', 'assigned', 'activated', 'sent', 'starting']:
      return 'prerun'
    elif jobStatus in ['holding', 'transferring']:
      return 'holding'
    else:
      return jobStatus

  def _jobLFNInfo(self, pandaIDs):
    jobsString = ','.join([str(x) for x in pandaIDs])
    table = 'new'
    if self.days > 3:
      table = 'deep'
    query = "joblfn?type=*&jobs={0}&table='{1}'&days={2}".format(jobsString, table, self.days)
    if self.verbose:
      print query
    import urllib
    query = urllib.quote(query)

    rows = list(self._rowsForQuery(query, LFNRow))
    if len(rows) >= 1000:
      return self._slowerJobLFNInfo(pandaIDs)
    else:
      return self._fasterJobLFNInfoFromRows(pandaIDs, rows)

  def _fasterJobLFNInfoFromRows(self, pandaIDs, rows):
    outputFileDict = {}
    outputBlockDict = {}
    inputFilesDict = {}

    for row in rows:
      if row.pandaID not in inputFilesDict:
        inputFilesDict[row.pandaID] = []
      if row.type == 'output':
        outputFileDict[row.pandaID] = row.LFN
        outputBlockDict[row.pandaID] = row.dataset
      elif row.type == 'input':
        # Skip build inputs
        if row.LFN.split('.')[-2] == 'lib' and row.dataset.split('.')[-2] == 'lib':
          continue
        inputFilesDict[row.pandaID].append(row.LFN)

    for pandaID in pandaIDs:
      yield pandaID, outputFileDict[pandaID], outputBlockDict[pandaID], inputFilesDict[pandaID]

  def _slowerJobLFNInfo(self, pandaIDs):
    for pandaID in pandaIDs:
      table = 'new'
      if self.days > 3:
        table = 'deep'
      query = "joblfn?type=*&jobs={0}&table='{1}'&days={2}".format(pandaID, table, self.days)
      if self.verbose:
        print query
      import urllib
      query = urllib.quote(query)
      
      outputFile = ''
      outputBlock = ''
      inputFiles = []
      for row in self._rowsForQuery(query, LFNRow):
        if row.type == 'output':
          outputFile = row.LFN
          outputBlock = row.dataset
        elif row.type == 'input':
          # Skip build inputs
          if row.LFN.split('.')[-2] == 'lib' and row.dataset.split('.')[-2] == 'lib':
            continue
          inputFiles.append(row.LFN)
      yield pandaID, outputFile, outputBlock, inputFiles


  def _removeDuplicateRows(self, rowGenerator):
    seenIDs = set()
    seenJobsetIDs = []
    idToModificationTime = {}
    initialResult = []
    for row in rowGenerator:
      if row.pandaID not in seenIDs:
        seenIDs.add(row.pandaID)
        seenJobsetIDs.append((row.jobsetID, row.pandaID))
        idToModificationTime[row.pandaID] = row.modificationDatetime
        initialResult.append(row)
      else:
        if idToModificationTime[row.pandaID] != row.modificationDatetime:
          raise RuntimeError('ERROR!! Duplicates for {0} in jobset {1} have different modification times'.format(row.pandaID, row.jobsetID))

    rowsToKill = []
    for row in initialResult:
      if (row.jobsetID, row.parentID) in seenJobsetIDs:
        #print 'AAAH', row.parentID, row.jobsetID, row.attemptNr
        for parent in initialResult:
          if (parent.jobsetID, parent.pandaID) == (row.jobsetID, row.parentID):
            break
            #print 'AAAH (parent)', parent.pandaID, parent.jobsetID, parent.attemptNr
        if row.attemptNr > parent.attemptNr: rowsToKill.append(parent)
        else: rowsToKill.append(row)

    result = []
    for row in initialResult:
      if row in rowsToKill: continue
      result.append(row)
    return result

  def _rowsForQuery(self, query, RowType=TaskBufferRow):
    jsonString = self._getJsonString(query)
    #if self.verbose:
    #  print 'BEGIN JSON'
    #  print jsonString
    #  print 'END JSON'
    import json
    parsed = json.loads(jsonString)
    for row in parsed['pm'][0]['json']['buffer']['data']['rows']:
      yield RowType(row)

  def _getJsonString(self, query):
    command = 'curl --silent --compressed "http://pandamon.cern.ch/{0}"'.format(query)
    #command = 'curl --silent --compressed "http://pandamon-eu.atlascloud.org/{0}"'.format(query)
    if self.verbose:
      command = command.replace('--silent ', '')
      #print command
    import subprocess
    jsonString = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).communicate()[0]
    if jsonString.startswith('error'):
      raise ValueError(jsonString)
    return jsonString


def getJobsetContainer(names, days, verbose=True, retryAdInfinitum=True):
  if isinstance(names, list):
    username = ' '.join(names)
  elif isinstance(names, str):
    username = names
  parser = PandaJsonParser()
  parser.username = username
  parser.days = days
  parser.verbose = verbose
  if retryAdInfinitum:
    _updateParserUntilSuccessful(parser)
  else:
    parser.updateTaskBuffer()

  return parser.jobsetsInTaskBuffer()

def getJobContainer(names, days, jobsetID):
  if isinstance(names, list):
    username = ' '.join(names)
  elif isinstance(names, str):
    username = names
  parser = PandaJsonParser()
  parser.username = username
  parser.days = days
  _updateParserUntilSuccessful(parser, jobsetID)
  return parser.jobsInTaskBuffer()

def _updateParserUntilSuccessful(parser, jobsetID=None):
  updated = False
  while not updated:
    try:
      if jobsetID is None:
        parser.updateTaskBuffer()
      else:
        parser.updateTaskBuffer(jobsetID=jobsetID)
      updated = True
    except Exception, e:
      #print '{0}: {1}'.format(str(type(e).__name__), e)
      import traceback
      print traceback.format_exc()
      print 'pandaJsonParser: failed to update, retrying in 30s'
      import time
      time.sleep(30)

def test():
  jobsets = getJobsetContainer(['thomas','gillam'], 7)
  for jobset in jobsets.all():
    #if 'user.tgillam.0130RPV_p1328-defaultslim.00200804.physics_Egamma' in jobset.outputDataset:
    #if 153026 == jobset.jobID:
    #if 152172 == jobset.jobID:
    if 152992 == jobset.jobID:
      print jobset
  #jobs = getJobContainer(['thomas','gillam'], 1, 61507)
  #print jobs

if __name__ == '__main__':
  test()
