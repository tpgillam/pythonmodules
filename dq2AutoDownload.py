# Library to handle auto-download of datasets from DQ2, with
# a decent amount of consistency checking. Ideally, this will
# retry until all datasets have been fetched correctly

class AutoDownloader(object):
  '''Auto download dataset (container)s from DQ2 until all have
  been downloaded successfully.'''

  def __init__(self):
    self._jobs = []
    self._pollDelay = 30
    self._timeout = 2 * 3600
    self._setupScript = '/usera/gillam/scripts/dq2_setup.sh'
    self._maxRetrials = 5

  def append(self, dataset, outputDir, pattern):
    '''Downloaded datset to outputDir. Only download files that match pattern'''
    job = _DownloadObject()
    job.dataset = dataset
    job.outputDir = outputDir
    job.pattern = pattern
    self._jobs.append(job)

  def setPollDelay(self, pollDelay):
    '''Set the delay (in s) between polling DQ2 processes'''
    self._pollDelay = pollDelay

  def setTimeout(self, timeout):
    '''Maximum amount of time (in s) to let a download job run before
    killing it and declaring it as failed'''
    self._timeout = timeout

  def setDQ2Setup(self, setupScript):
    '''Set the location of the DQ2 setup script'''
    self._setupScript = setupScript

  def setMaxRetrials(self, maxRetrials):
    '''Set the maximum number of retrials for any one dataset'''
    self._maxRetrials = maxRetrials

  def run(self):
    '''GO!!!'''
    print 'AutoDownloader: Max retrials per dataset:',self._maxRetrials
    while self._stillGoing():
      for job in self._jobs:
        if not job.completed and not job.numFails == self._maxRetrials:
          self._submitJob(job)
    
    # Finished...
    successful = True
    for job in self._jobs:
      if not job.completed:
        successful = False

    if successful:
      print 'AutoDownloader: All datasets downloaded successfully (hopefully...)!'
    else:
      print 'AutoDownloader: The following datasets failed to download'
      for job in self._jobs:
        if not job.completed:
          print job.dataset

  ##### INTERNAL FUNCTIONS #####
  
  def _stillGoing(self):
    '''Returns false if all jobs listed as completed. Take into account jobs that
    have been retried the maximum number of times'''
    for job in self._jobs:
      if not job.completed and job.numFails < self._maxRetrials:
        return True
    return False


  def _submitJob(self, job):
    '''Submit the job, setting up DQ2 in the process'''
    # Create directory if it doesn't already exit
    import os
    if not os.path.isdir(job.outputDir):
      os.makedirs(job.outputDir)

    command = '. {0}; dq2-get '.format(self._setupScript)
    if job.pattern != '':
      command += '-f {0} '.format(job.pattern)
    command += job.dataset 

    import subprocess
    print 'AutoDownloader: downloading', job.dataset
    print 'AutoDownloader: executing', command
    #subprocess.call(command, cwd=job.outputDir, shell=True)
    process = subprocess.Popen(command, cwd=job.outputDir, \
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    # Wait until job is finished, or job times out (and should be killed) 
    import time
    starttime = time.time()
    errorFlag = False
    while process.poll() != None:
      if time.time() - starttime > self._timeout:
        # Kill the job, and chalk up as error
        print 'AutoDownloader: download timed out ({0}s)'.format(self._timeout)
        print 'AutoDownloader: killing process...'
        process.kill()
        errorFlag = True
      time.sleep(self._pollDelay)

    # If we terminated the job or encountered another error, don't bother
    # parsing output, and count as a failure
    if errorFlag:
      job.numFails += 1
    else:
      output = process.communicate()[0]
      # print output
      if self._successfulOutput(output):
        print 'AutoDownloader: download succeeded!'
        job.completed = True
      else:
        job.numFails += 1
        print 'AutoDownlaoder: download failed ({0} failures for this dataset)'.format(job.numFails)
        #print output

  def _successfulOutput(self, output):
    '''Determine whether the output is indicative of a failure or not.
    Returns True if the output was unequivocally a success.'''
    # Test 1: Square brackets => it's failed... (not strictly true, but they're annoying)
    # Remember to get rid of dq2 initialisation, which contains square brackets...
    import re
    moddedoutput = re.sub(r'Setting up dq2[\s\S]+Your proxy is valid until','',output)
    if moddedoutput.count('[') > 0 or moddedoutput.count (']') > 0:
      #print moddedoutput
      print 'AutoDownloader: Failed Test #1'
      return False
    
    # Test 2: Look for the word 'FAILED'
    if output.count('FAILED') > 0:
      print 'AutoDownloader: Failed Test #2'
      return False

    # Let's hope it was all fine ;-)
    return True

class _DownloadObject(object):
  '''Internal class to summarise the state and properties
  of a download'''
  
  def __init__(self):
    self.outputDir = ''
    self.dataset = ''
    self.pattern = ''

    self.completed = False
    self.numFails = 0

  def __str__(self):
    return self.dataset

