def killJobsets(jobsetIDsToKill):
  if isinstance(jobsetIDsToKill, int):
    jobsetIDsToKill = [jobsetIDsToKill, ]
  from pandaJsonParser import getJobsetContainer
  print 'Updating database...'
  jobsets = getJobsetContainer(getUsername(), 3, retryAdInfinitum=False)
  print '... retrieved',len(jobsets),'jobs'
  for jobset in jobsets.running():
    if jobset.jobID not in jobsetIDsToKill:
      continue
    print 'Killing',jobset.jobID
    from pandatools import Client
    Client.killJobs(jobset.pandaIDs)

def getUsername():
    username = ''
    from pandatools import Client
    gridSrc = Client._getGridSrc()
    if gridSrc == False:
        return ''
    import commands
    output = commands.getoutput('%s grid-proxy-info -identity' % gridSrc)
    for token in output.split('/'):
      if token.startswith('CN='):
        username = token.strip()[3:]
        break
    import re
    # remove _
    username = re.sub('_$','',username)
    # remove ' & "
    username = re.sub('[\'\"]','',username)
    # check
    if username == '':
        print 'ERROR: could not get DistinguishedName from %s' % output
    return username

if __name__ == '__main__':
  killJobsets(72199)
