#!/usr/bin/env python

def main():
  tweeter = Tweeter()
  import time
  while 1:
    tweeter.printRandomTweet()
    print
    time.sleep(1)


class Tweeter(object):
  def __init__(self):
    self.defaultSearchTerms = ['olympics', 'ladygaga', 'higgs', 'boson', \
        'cern', 'berkeley', 'physics', 'merton', 'oxford', 'cambridge', \
        'dontmentionthewar', 'oboe', 'rowing', 'clawhammer']

    self._extraSearchTerms = self._getExtraSearchTerms()

  def _getExtraSearchTerms(self):
    sourceCode = self._downloadPage('http://trendsmap.com/')
    import re
    matches = re.findall('/topic/(\w+)\'', sourceCode)
    return matches

  def printRandomTweet(self):
    searchTerm,tweet = (None, None)
    try:
      searchTerm,tweet = self.getRandomSearchTermAndTweet()
    except:
      searchTerm,tweet = ('*error*', 'Sorry, failed to get tweet!')
    print 'Random tweet on the topic of {0}:\n  {1}'.format(searchTerm, tweet)

  def getRandomSearchTermAndTweet(self):
    from random import choice
    searchTerms = self._allSearchTerms()
    searchResult = None
    unicodeTweet = None
    foundTweet = False
    while not foundTweet:
      searchTerm = choice(searchTerms)
      searchResult = self._getTwitterSearchResults(searchTerm)
      if self._noResultsInSearch(searchResult):
        continue
      unicodeTweet = choice(searchResult['results'])[u'text']
      if not self._isValidTweet(unicodeTweet):
        continue
      else:
        foundTweet = True
        break
    return (searchTerm, unicodeTweet)

  def _allSearchTerms(self):
    return self.defaultSearchTerms+self._extraSearchTerms

  def _downloadPage(self, url):
    import urllib
    contents = None
    while contents == None:
      try:
        f = urllib.urlopen(url)
        contents = f.read()
        f.close()
      except:
        import time
        print 'Tweeter: timeout, retrying in 30s'
        time.sleep(30)
    return contents

  def _noResultsInSearch(self, searchResult):
    if searchResult == None:
      return True
    if not 'results' in searchResult:
      return True
    if len(searchResult['results']) == 0:
      return True
    return False

  def _getTwitterSearchResults(self, searchTerm):
    import twitter
    try:
      twitter_search = twitter.Twitter(domain="search.twitter.com")
      searchResult = twitter_search.search(q=searchTerm)
      return searchResult
    except twitter.api.TwitterHTTPError:
      return None

  def _isValidTweet(self, unicodeTweet):
    try:
      unicodeTweet.encode('ascii')
    except UnicodeEncodeError:
      return False
    return True

if __name__ == '__main__':
  main()
