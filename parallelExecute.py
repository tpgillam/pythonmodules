# Very light-weight parallel processing library. Give a set of commands, a
# directory to run in, and this will run all of them, running as many in
# parallel as possible.

import subprocess

class ParallelExecute(object):
  '''Run a list of commands, in parallel, but only running a maximum number together
  at once'''

  def __init__(self, startdir, commandlist):
    '''Run the list of commands, commandlist, starting in the directory startdir'''
    # Where we should execute the commands
    self._startdir = startdir

    # The list of commands we want to run, as a dictionary
    self._commands = {}
    index = 0
    for command in commandlist:
      self._commands[index] = command
      index += 1

    # Maximum number of processes that should run at any one time
    self._maxThreads = 4

    # Dictionary of active processes, keyed by the index of the command used to start.
    # 'None' indicates that the process has not yet been spawned
    self._processes = {}
    for index in self._commands:
      self._processes[index] = None

    # Set waiting time between job submission
    self._waitingTime = 60

    # Output for each command, as read from stdout
    self._output = [''] * len(commandlist)

    # Count of how many jobs we've submitted
    self._numSubmitted = 0
  
  def setMaxThreads(self, maxThreads):
    '''Set the maximum number of threads we should spawn'''
    self._maxThreads = maxThreads

  def setWaitingTime(self, waitingTime):
    '''Set how long we should wait for jobs to complete before submitting more'''
    self._waitingTime = waitingTime

  def runAndWait(self):
    '''Run all specified commands, and wait till they're all done. Return list containing
    output of each command separately (stdout & stderr combined)'''
    while(1):
      # Submit jobs if necessary
      if self._numWaitingProcs() > 0:
        self._submitUpToLimit()

      # Clean up finished jobs
      self._cleanUpFinished()

      # Are we all finished?
      if self._numWaitingProcs() == 0 and self._numActiveProcs() == 0 and self._numFinishedProcs() == 0:
        # Consistency check...
        if self._numCleanedProcs() != len(self._commands):
          raise Exception('ERROR: we don\'t have enough finished processes')
        break

      # Sleep a bit till resubmitting/repolling
      import time
      time.sleep(self._waitingTime)
    
    return self._output


  ##### INTERNAL FUNCTIONS. Don't call explicitly  #####

  def _singleAppendOutput(self, key):
    '''Append the output for a given key'''
    output = self._processes[key].communicate()[0]
    if output != None:
      self._output[key] += output

  def _cleanUpFinished(self):
    '''Clean up finished jobs and set process to "done"'''
    for key in self._processes:
      # If a job has finished, but not been cleaned up...
      if self._getProcState(key) == 2:
        # Flush output from stdin pipe
        self._singleAppendOutput(key)

        print 'ParallelExecute: Cleaning up '+str(self._processes[key].pid)
        # Delete the process object
        del self._processes[key]

        # Change the process label to 'done' string
        self._processes[key] = 'done'

  def _submitUpToLimit(self):
    '''Submit unsubmitted jobs from command until there are maxThreads jobs running'''
    for key in self._commands:
      # If the number of active processes is >= maxthreads, get out of here!
      if self._numActiveProcs() >= self._maxThreads:
        return

      # Submit a job if the process hasn't already been spawned.
      if self._getProcState(key) == 0:
        process = subprocess.Popen(self._commands[key], cwd=self._startdir, \
            stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT, shell=True)
        #process = subprocess.Popen(self._commands[key], cwd=self._startdir, \
        #    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        #process = subprocess.Popen(self._commands[key], cwd=self._startdir, shell=True)
        self._numSubmitted += 1
        print 'ParallelExecute: Spawned process '+str(process.pid)+', '+str(self._numSubmitted)+'/'+str(len(self._commands))
        self._processes[key] = process

  def _getProcState(self, key):
    '''0 = not started, 1 = running, 2 = finished/failed, 3 = cleaned up'''
    if self._processes[key] == None:
      return 0
    elif self._processes[key] == 'done':
      return 3

    returncode = self._processes[key].poll()
    if returncode == None:
      return 1
    return 2

  def _numWaitingProcs(self):
    '''Determine the number processes waiting to submit'''
    count = 0
    for key in self._processes:
      if self._getProcState(key) == 0:
        count += 1
    return count

  def _numActiveProcs(self):
    '''Determine the number of active processes'''
    count = 0
    for key in self._processes:
      if self._getProcState(key) == 1:
        count += 1
    return count

  def _numFinishedProcs(self):
    '''Determine the number of finished/failed processes'''
    count = 0
    for key in self._processes:
      if self._getProcState(key) == 2:
        count += 1
    return count

  def _numCleanedProcs(self):
    '''Determine the number of cleaned up processes'''
    count = 0
    for key in self._processes:
      if self._getProcState(key) == 3:
        count += 1
    return count

# Testing
def main():
  commands = ['ls']*20
  par = ParallelExecute('/usera/gillam/', commands)
  par.setMaxThreads(4)
  par.setWaitingTime(2)
  output = par.runAndWait()
  for x in output:
    print x

if __name__ == "__main__":
  main()
