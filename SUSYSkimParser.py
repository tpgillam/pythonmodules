# Library to skim information from SUSY D3PD Trigger skimmed
# summary pages. Only gotcha is that some of the information is
# (apparently) wrong...

from HTMLParser import HTMLParser
import urllib

class SusySkimParser(HTMLParser):
  def __init__(self, container):
    HTMLParser.__init__(self)

    # Container to place information
    self.container = container

    # State indicators
    self.inH2Title = False
    self.inH3Title = False
    self.inDataRow = False

    self.inAODName = False
    self.inD3PDName = False
    self.inNumAOD = False
    self.inNumD3PD = False

    self._resetRowInfo()

  def _resetRowInfo(self):
    self.doneAODName = False
    self.doneD3PDName = False
    self.doneNumAOD = False
    self.doneNumD3PD = False

    # Row information
    self.valAODName = ''
    self.valD3PDName = ''
    self.valNumAOD = 0
    self.valNumD3PD = 0

  def handle_starttag(self, tag, attrs):
    if tag == 'h2':
      self.inH2Title = True
    if tag == 'h3':
      self.inH3Title = True
    
    # Handle a data row
    if tag == 'tr' and ('bgcolor', '#ffffff') in attrs or ('bgcolor', '#edf4f9') in attrs:
      self.inDataRow = True

    # If we're in a data row, pick out the information we want
    if self.inDataRow:
      if tag == 'font' and ('size', '1') in attrs:
        if not self.doneAODName:
          self.inAODName = True
        elif not self.doneD3PDName:
          self.inD3PDName = True
      if tag == 'td' and ('align', 'right') in attrs:
        if not self.doneNumAOD:
          self.inNumAOD = True
        elif not self.doneNumD3PD:
          self.inNumD3PD = True

  def handle_endtag(self, tag):
    if self.inH2Title and tag == 'h2':
      self.inH2Title = False
    if self.inH3Title and tag == 'h3':
      self.inH3Title = False

    if self.inDataRow:
      # End of elements within data row
      if self.inAODName and tag == 'font':
        self.doneAODName = True
        self.inAODName = False
      if self.inD3PDName and tag == 'font':
        self.doneD3PDName = True
        self.inD3PDName = False
      if self.inNumAOD and tag == 'td':
        self.doneNumAOD = True
        self.inNumAOD = False
      if self.inNumD3PD and tag == 'td':
        self.doneNumD3PD = True
        self.inNumD3PD = False

      # End of data row
      if tag == 'tr':
        self.inDataRow = False
        # Write out row information here
        self.container.addRow(self.valD3PDName, self.valNumD3PD, self.valAODName, self.valNumAOD)
        #print '{0}: {1}'.format(self.valAODName, self.valNumAOD)
        #print '{0}: {1}'.format(self.valD3PDName, self.valNumD3PD)

        # Clear row state tags and values
        self._resetRowInfo()

  def handle_data(self, data):
    if self.inH2Title:
      self.container.setH2(data.strip())
      #print 'H2:', data
    elif self.inH3Title:
      self.container.setH3(data.strip())
      #print 'H3:', data

    elif self.inAODName:
      self.valAODName = data.strip()
    elif self.inD3PDName:
      self.valD3PDName = data.strip()
    elif self.inNumAOD:
      self.valNumAOD = int(data.strip())
    elif self.inNumD3PD:
      self.valNumD3PD = int(data.strip())

class SusySkimContainer(object):
  '''Container object for info on the susy skim D3PD summary page'''
  def __init__(self):
    self._allRows = []
    # Double dict of lists: h2 -> h3 -> [D3PD names]
    self._headerDict = {}

    # State info
    self._currH2 = ''
    self._currH3 = ''

  def __str__(self):
    '''Informal summary of information'''
    s = ''
    for h2key in self._headerDict:
      for h3key in self._headerDict[h2key]:
        for d3pdname in self._headerDict[h2key][h3key]:
          s += d3pdname+'\n'
    return s

  def setH2(self, headername):
    '''Set the h2 level header'''
    self._currH2 = headername

  def setH3(self, headername):
    '''Set the h3 level header'''
    self._currH3 = headername

  def addRow(self, named3pd, numd3pd, nameaod, numaod):
    '''Append a data row to the container. Use current header info. D3PD name should be unique'''
    self._allRows.append((named3pd, numd3pd, nameaod, numaod))
    self._initDictLevel()
    self._headerDict[self._currH2][self._currH3].append(named3pd)

  def numFromD3PDName(self, named3pd):
    '''Return the number of events in a particular d3pd'''
    for row in self._allRows:
      if row[0] == named3pd:
        return row[1]

  def sumFromD3PDNameList(self, named3pdlist):
    '''Return the sum of the events in the given list of names'''
    total = 0
    for row in self._allRows:
      if row[0] in named3pdlist:
        total += row[1]
    return total

  def D3PDNameListFromHeaders(self, h2name, h3name, family = ''):
    '''Get the list of d3pd objects under headers h2name and h3name. When
    second header level isn't present in the page, use an empty string.
    Use, say, family = 'NTUP_SUSYSKIM' for data if desired'''
    temp = []
    for name in self._headerDict[h2name][h3name]:
      if family == '' or name.count('.'+family+'.') > 0:
        temp.append(name)
    return temp

  #### INTERNAL FUNCTIONS ####

  def _initDictLevel(self):
    if not self._currH2 in self._headerDict:
      self._headerDict[self._currH2] = {}
    if not self._currH3 in self._headerDict[self._currH2]:
      self._headerDict[self._currH2][self._currH3] = []

def downloadPage(url):
  f = urllib.urlopen(url)
  s = f.read()
  f.close()
  return s

def getContainer(url):
  s = downloadPage(url)

  # Fix seemingly stupid syntax
  s = s.replace('td +', 'td ')
  s = s.replace('td >', 'td>')
  s = s.replace('0<eff<1', '0&lt eff &lt 1')

  container = SusySkimContainer()
  parser = SusySkimParser(container)
  parser.feed(s)
  return container

# Example
def main():
  container = getContainer('http://atlsusy.home.cern.ch/atlsusy/html_data11_p832.html')
  #for x in container._headerDict:
  #  print x
  #  for y in container._headerDict[x]:
  #    print y
  names = container.D3PDNameListFromHeaders('Egamma', 'Egamma periodH', 'NTUP_SUSYSKIM')
  print 'Datasets in Egamma periodH:\n'
  for name in names:
    print '{0}:\t{1}'.format(name, container.numFromD3PDName(name))
  print '\nSum of events:\t', container.sumFromD3PDNameList(names)
  
  container2 = getContainer('http://atlsusy.home.cern.ch/atlsusy/html_mc11c_p832.html')
  names2 = container2.D3PDNameListFromHeaders('QCD', 'Pythia Jx', 'NTUP_SUSY')
  print 'Datasets in QCD, Pythia Jx:\n'
  for name in names2:
    print '{0}:\t{1}'.format(name, container2.numFromD3PDName(name))
  print '\nSum of events:\t', container2.sumFromD3PDNameList(names2)

if __name__ == '__main__':
  main()

